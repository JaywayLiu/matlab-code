%This is the orignal rate generation func


function f = getRate(x, i, j, MaxRate)
%global N;
global isOpt;
global phaseId;
global deployRatio;
global nonParInfo;

if j==1
    bb=sum(x ,1);
    if phaseId==1 && deployRatio < 1 && isOpt
        bb = bb + nonParInfo{1};
    end
    if bb(j) >0
        f = MaxRate(i, j) / bb(j);
    else
        %if no one connects, rate is 0?  may have inf?
        f=0;
    end
    
else
    sumR = 0;
    ii = find(x(:,j));
    %if no UE connected
    if(isempty(ii))
        f = 0;
        return;
    end
    for k = 1:length(ii)
        rateI = MaxRate(ii(k), j);
        if  x(ii(k), j) >0
            if rateI>0 
                sumR = sumR + 1/(rateI* x(ii(k),j));
            else
                f = 0;
                return;               
            end
        end
    end
    if phaseId==1 && deployRatio < 1 && isOpt
        sumR = sumR + nonParInfo{2}(j);
    end
    if sumR<0.00000000000000001
        f = 0;
    else
        f = (1/sumR);
    end
end
end