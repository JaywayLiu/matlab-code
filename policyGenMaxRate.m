function xa = policyGenMaxRate(MaxRate)
global M;
global N;
%global lteMCSIndex;
%global wifiMCSIndex;

%only wifi's distance, so -1
xa = zeros(N, M);
for i=1: N
%     th = zeros(1, M-1);
%     for j=2:M
%         th(j-1) = wifiMCSIndex(i, j-1);
%     end
    %th = ;
    
    %index is the first occurence
    [maxRate, ~] = max(MaxRate(i, 2:M));
    idx = find(MaxRate(i, 2:M)== maxRate);
    
    %connect to the cloest wifi
    if maxRate >0
        randIndex = randi(length(idx), 1, 1);
        %xa(i, index+1)  =1;
        xa(i, idx(randIndex)+1)  =1;
    else
        %connect to lte
        xa(i, 1) = 1;
    end
end
