function [tempV, currentR] = incrementalEval(currentV, currentR, maxRateTable, i, j)
    %global onOffMap;
    %use of logical index is more efficient than find
    % ii = currentR(:,j) >0;
    bi = find(currentR(:, j) > 0);
    nUEOld = length(bi);
    nActiveUE = nUEOld + 1;

    if j == 1

        newValue = maxRateTable(i, j) / nActiveUE;
        currentR(i, j) = newValue;
        newLog = safe_log(newValue);

        if ~isempty(bi)

            oldValues = currentR(bi, j);
            newValues = maxRateTable(bi, j) / nActiveUE;

            sumDelta = sum(log(newValues) - log(oldValues));
            currentR(bi, j) = newValues;

            sumDelta = sumDelta + newLog;
            %when old values are all 0s
        else
            sumDelta = newLog;
        end

    else

        %directly calc sumRInverse from the maxRateTable
        if ~isempty(bi)

            newRate = maxRateTable(i, j)
            oldValue = currentR(bi(1), j);

            if (newRate > 0)
                newArray = maxRateTable(bi, j);
                newArray(length(newArray) + 1, 1) = newRate
                %newArray is row vector, return should be a scalar
                newInverse = calcSumRInverse(newArray)

                newValue = 1 / newInverse
                sumDelta = nActiveUE * log(newValue) - nUEOld * log(oldValue);

                currentR(bi, j) = newValue;
                currentR(i, j) = newValue;
            else
                currentR(:, j) = 0;
                sumDelta = -nUEOld * log(oldValue);
            end

            currentR
        else
            newValue = maxRateTable(i, j);
            currentR(i, j) = newValue;
            sumDelta = safe_log(newValue);
        end

    end

    tempV = currentV + sumDelta;
end
