function pos=generateRandomPlacementInCircleV1(nSteps, nPoints, center, rangeRadius, randType)
isVis = 0;
pos = zeros(2, nPoints, nSteps);
ori{1} = center(1) .* ones(nPoints, nSteps);
ori{2} = center(2) .* ones(nPoints, nSteps);
if strcmp(randType, 'uniform')
    randArray = rand(nPoints, nSteps);
    randForDist = rand(nPoints, nSteps);
%elseif strcmp(randType ,'poisson')
% need a for loop of nSteps here??
 % randArray = poissrnd(nPoints);
end

randAngle = randArray * 2* pi;
%randDist = rangeRadius .* sqrt(randArray) ;
randDist = rangeRadius .* sqrt(randForDist) ;
%polar coodination
pos(1, :, :) =  randDist  .* cos(randAngle) + ori{1};
pos(2, :, :) = randDist .* sin(randAngle) + ori{2};

if (isVis)
outputFolder = 'image-output';
%outputFolder = strcat(outputFolder, 'mcsDataOutput/new-400ns-16k-new-nobf-partial/');
checkDir(outputFolder);
%ax = gca;
viscircles([0,0],  1, 'Color', 'k', 'LineWidth', 3);
hold on
scatter(pos(1, :, :), pos(2, :, :));
axis equal
end

end

