function [nv, xtemp, roundTimeJTemp] = moveEval(i, j, MaxRate, newV, newX, nLte, roundTimeJ)

    global isWiFiPF;

    xtemp = newX;
    xtemp(i, 1) = 0;
    xtemp(i, j) = 1;

    if nLte > 1
        lteDelta = (nLte - 1) * log((nLte) / (nLte - 1)) - log(MaxRate(i, 1) / nLte);
    elseif nLte == 1
        lteDelta = -log(MaxRate(i, 1));
    end

    ins = find(newX(:, j) > 0);
    K = length(ins);
    roundTimeJTemp = (roundTimeJ + 1 / MaxRate(i, j));

    if ~isWiFiPF
        if roundTimeJ ~= 0
            wifiDelta = (K + 1) * log(1 / roundTimeJTemp) - K * log(1 / roundTimeJ);
        else
            wifiDelta = log(MaxRate(i, j));
        end

    else
        if K > 0
            wifiDelta = K * log((K) / (K + 1)) + log(MaxRate(i, j) / (K + 1));
        else
            wifiDelta = log(MaxRate(i, j));
        end

    end

    nv = newV + lteDelta + wifiDelta;

    % debugNv =  -myfunInsideS(xtemp)
    % if abs(debugNv) < 100
    %     xtemp
    %   assert(abs(nv - debugNv)<0.001);
    % end

end
