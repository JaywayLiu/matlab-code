function x = gRandom()
global M;
global N;
%global lteMCSIndex;
%global wifiMCSIndex;
%global MaxRate;

%only wifi's distance, so -1
x = randi(M, N, M);
end