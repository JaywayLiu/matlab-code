function [tempV, currentR] = incrementalEvalNoZero(currentV, currentR, maxRateTable, i, j)
    %need to make sure the input maxRateTable(i, j) >0
    %global onOffMap;
    %use of logical index is more efficient than find
    % ii = currentR(:,j) >0;
    global isWiFiPF;
    bi = find(currentR(:, j) > 0);
    nUEOld = length(bi);
    nActiveUE = nUEOld + 1;
    newValue = maxRateTable(i, j);
    %otherDelta = 0;

    if (nActiveUE > 1)

        if (j == 1)
            newValue = maxRateTable(i, j) / nActiveUE;
            oldValues = currentR(bi, j);
            newValues = maxRateTable(bi, j) / nActiveUE;

            otherDelta = sum(log(newValues) - log(oldValues));
            currentR(bi, j) = newValues;
            currentR(i, j) = newValue;
            tempV = currentV + log(newValue) + otherDelta;
            %when old values are all 0s
        else

            if ~isWiFiPF
                oldValue = currentR(bi(1), j);
                %if (newRate > 0)
                newArray = maxRateTable(bi, j);
                newArray(length(newArray) + 1, 1) = newValue;
                %newArray is row vector, return should be a scalar
                newInverse = calcSumRInverse(newArray);

                newValue = 1 / newInverse;
                otherDelta = nActiveUE * log(newValue) - nUEOld * log(oldValue);
                currentR(bi, j) = newValue;
                currentR(i, j) = newValue;
                tempV = currentV + otherDelta;
            else
                newValue = maxRateTable(i, j) / nActiveUE;
                oldValues = currentR(bi, j);
                newValues = maxRateTable(bi, j) / nActiveUE;

                otherDelta = sum(log(newValues) - log(oldValues));
                currentR(bi, j) = newValues;
                currentR(i, j) = newValue;
                tempV = currentV + log(newValue) + otherDelta;

            end

            % else
            %     currentR(:, j) = 0;
            %     sumDelta = -nUEOld * log(oldValue);
            % end
        end

    else
        tempV = currentV + log(newValue);
        currentR(i, j) = newValue;
    end

end
