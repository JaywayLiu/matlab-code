function genOneSceneForMulti(genType, nSteps, nUE, nAP, globalP, subName, angleIn)

    global N;
    N = nUE;
    global M;

    %wifiPos = 0.375;
    %wifiPos = 0.5;
    wifiPos = 2/3;
    M = nAP + 1;

    mkdir(subName);
    cd(subName);

    lteAPCenter = [0, 0];
    csvwrite('lte_centers.txt', lteAPCenter);

    %wifiRadius = 0.625;
    wifiRadius = 0.775;

    rangeLimit = 1;

    fileID = fopen('scene-config.txt', 'w');
    fprintf(fileID, '%d\n', nSteps);
    fprintf(fileID, '%d %d\n', nAP, nUE);
    fprintf(fileID, '%.4f\n', wifiRadius);
    fprintf(fileID, '%.4f\n', wifiPos);
    fprintf(fileID, '%.4f\n', rangeLimit);
    fprintf(fileID, '%.4f %.4f\n', lteAPCenter(1), lteAPCenter(2));
    fclose(fileID);

    %this is for calc of Wifi locations

    if strcmp(genType, 'normal')
        pos = generateRandomPlacementInCircleV1(nSteps, nUE, lteAPCenter, rangeLimit, 'uniform');
    elseif strcmp(genType, "cluster-circ")

        clusterPos = 2/3;
        rangeRadius = 0.25;

        fileID = fopen('circ-cluster.txt', 'w');

        fprintf(fileID, 'globalP: %.3f\n', globalP);
        fprintf(fileID, 'clusterPos: %.3f\n', clusterPos);
        fprintf(fileID, 'rangeRadius: %.3f\n', rangeRadius);
        fprintf(fileID, 'angleIn: %.3f\n', angleIn);
        fclose(fileID);

        %[pos, clusterCenters] = genCircClusters(3, nUE, globalP, nSteps, clusterPos, rangeRadius, angleIn, lteAPCenter);
        
        [pos, clusterCenters] = genCircClustersRandom(3, nUE, globalP, nSteps, clusterPos, rangeRadius, angleIn, lteAPCenter);

    elseif strcmp(genType, "cluster-tshape")
        nC = 2;
        clusterCenters(:, 1) = [0; 0];
        clusterCenters(:, 2) = [0; 0.5];

        clusterWh(:, 1) = [2; 0.1];
        clusterWh(:, 2) = [0.1; 1];

          fileID = fopen('tshape-cluster.txt', 'w');

        fprintf(fileID, 'globalP: %.3f\n', globalP);
        fprintf(fileID, 'clusterCenters 1: %.3f %.3f\n', clusterCenters(1, 1), clusterCenters(2, 1));
        fprintf(fileID, 'clusterCenters 2: %.3f %.3f\n', clusterCenters(1, 2), clusterCenters(2, 2));

        fprintf(fileID, 'clusterWh 1: %.3f %.3f\n', clusterWh(1, 1), clusterWh(2, 1));
        fprintf(fileID, 'clusterWh 2: %.3f %.3f\n', clusterWh(1, 2), clusterWh(2, 2));
        fclose(fileID);

        pos = genTshapeClustersRandom(nC,nUE, clusterCenters,   globalP, clusterWh, nSteps, lteAPCenter);

    else
        disp("type not suported, exit")
        exit
    end

    for i = 1:nSteps
        csvwrite(sprintf('mobility_%06d.txt', i), pos(:, :, i));
    end


    drawOneSpotNorm(wifiRadius, wifiPos, false, nAP, rangeLimit);

    cd('..');
end
