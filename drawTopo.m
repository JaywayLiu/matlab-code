global wifiRadius;
global v_edge;
global h_edge;


wifiRadius = 125;
v_edge = 10;
h_edge = 15;

angle = 100/180*pi;



wifi_rows = 2;
wifi_col = 3;

nwifi= wifi_rows * wifi_col;

global wifi_centers;
wifi_centers = cell(wifi_rows, wifi_col);

m_centers = zeros(wifi_rows*wifi_col, 2);


y= v_edge + wifiRadius;

sqrt3 = cos(angle/2.0) * 2;

k = 1;
for i=1:wifi_rows
    x = h_edge + wifiRadius;
    for j=1:wifi_col       
         wifi_centers{i, j} = [x, y];
         m_centers(k, :)= [x, y];
         x = x + sqrt3* wifiRadius;
         k = k +1;
    end
    y = y + sqrt3 * wifiRadius;
end

k =1;
for i=1:wifi_rows
    for j=1:wifi_col
        disp(wifi_centers{i, j});
        disp(m_centers(k, :));
        k = k+1;

    end
end

xlim = 2*h_edge + wifiRadius * (2+(wifi_col-1)*sqrt3);
ylim = 2*v_edge + wifiRadius * (2+(wifi_rows-1)*sqrt3);

clf
axis([0 650 0 450]);
xt = get(gca, 'XTick');
set(gca, 'FontSize', 14)

ax = gca;

outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];


viscircles(ax, m_centers, ones(nwifi, 1) * wifiRadius, 'LineWidth', 4);
viscircles(ax, m_centers, ones(nwifi, 1) * 3, 'Color','r', 'LineWidth', 4);
rectangle('Position',[0 0 xlim ylim]);



print('mp_topo','-dpng')


