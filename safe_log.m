function re = safe_log(input)
    assert(input >=0);
    LARGE_NUMBER = 1e100;
    if (input ~=0)
        re =  log(input);
       else
        re = -LARGE_NUMBER;
       end
end