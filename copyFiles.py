import os
level1 = ["opt=0", "opt=1"]
level2 = ["0.0000", "0.2500", "0.5000", "0.7500", "1.0000"]

targets = ["minList", "maxList"]
targets = ["describe.txt"]
#targets = ["all.txt", "Figs"]

input = "mcsDataOutput"
output = "Figs"

folderName = "dist-partial-32-4-16384-667"

def checkFolder(dest):
    if not os.path.exists(dest):
        os.makedirs(dest)

checkFolder(output)

input =  input +"/" + folderName
output = output+"/"+folderName
checkFolder(output)

for ii in level1:
    temp = output+"/"+ii
    checkFolder(temp)
    for jj in level2:
        tempIn = temp + "/"+jj
        checkFolder(tempIn)
        p = "/" +ii +"/" +jj +"/"
        for i in targets:
            print("cp -ra "+input+p+i+" " + output+p+i)
            os.system("cp -ra "+input+p+i+" " + output+p+i)
