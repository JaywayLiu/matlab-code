classdef SystemParameters < handle

    properties
        nAP = 0;
        nUE = 0;

        %accumulated metrics
        %it is better to store the metrics as a column vector
        %just document the meaning of the fields here

        %the metrics variables are two dim matrix
        %col for methods
        %row for metrics
        % 1 t
        % 2 v
        % 3 tfi
        %allMethods = ('gg', 'atom', 'lg');
        cur_metrics;
        accu_metrics;

        %last dimention methods
        % row UE, col AP
        cur_solutions;
        cur_rates;

        %it it possible to do UE level rate here, but given the large number of
        %simulation runs, we will only do it for static ones

        %the number of schedules has been run
        nSched;
    end

    methods

        function obj = SystemParameters(N, M)
            global nMetrics;
            global nMethods;

            obj.cur_metrics = zeros(nMetrics, nMethods);
            obj.accu_metrics = zeros(nMetrics, nMethods);
            obj.nSched = 0;
            obj.nUE = N;
            obj.nAP = M;
            obj.cur_solutions = zeros(N, M, nMethods);
            obj.cur_rates = zeros(N, nMethods);
        end

        function updateAccu(obj, duration)
            % obj.cur_metrics
            % obj.accu_metrics
            % duration
            %[instances, names] = enumeration(obj.accu_metrics);
            obj.accu_metrics = obj.accu_metrics + (obj.cur_metrics .* duration);
        end

        % function updateCur(obj, new_metrics)
        %     cur_metrics = new_metrics;
        % end

        %this is mainly for off right now, but if use periodally schedule,
        %will work for on also
        % use when on or off but not schedule
        function updateForOnOff(obj, flowID)
            global MaxRate;
            global nMetrics;
            global nMehtods;
            global N;

            %onOffMap is of nUE, one dim  onOffMap(1, i)
            global onOffMap;

            prev_onOffMap = onOffMap;

            %will we get the global one here?
            onOffMap(flowID, 1) = ~onOffMap(flowID, 1);
           
            % disp('cur_sol');
            % disp(length(obj.cur_solutions));
            % disp(obj.cur_solutions)
            %exit
            dims = size(obj.cur_solutions);

            if (sum(onOffMap) == 0)
                obj.cur_metrics(:, :) = 0;
                return;
            end

            %is it possible onOffMap not all zero, but no connection to any of
            %the APs? I think not possible

            % for every method
            for i = 1:dims(3)
                sol = obj.cur_solutions(:, :, i);

                %may store another array storing the ap index
                %then no need to do an O(M) search here, since M is really small
                %it does not matter for us
                apIndex = find(sol(flowID, :) == 1);

                %old_sol is the solution of apIndex
                %these are all row vectors
                old_sol = sol(:, apIndex);
                old_esol = old_sol & prev_onOffMap;

                esol = old_sol & onOffMap;

                ratesOneAP = MaxRate(:, apIndex);
                ratesOneAP = ratesOneAP .* old_esol;
                ratesOneAPNew = ratesOneAP .* esol;

                nConnected = sum(old_esol);
                nConnectedNew = sum(esol);

                oldRates = zeros(N, 1);
                updatedRates = zeros(N, 1);

                if apIndex == 1
                    %if LTE
                    %or need to find one non-zeor one in the old if real rates
                    %for each UE recorded
                    if nConnected ~= 0
                        oldRates = ratesOneAP ./ nConnected;

                    end

                    if nConnectedNew ~= 0
                        updatedRates = ratesOneAPNew ./ nConnectedNew;
                    end

                else
                    %only consider the off now
                    %if WiFi
                    revRates = 1 ./ ratesOneAP;
                    %some  of them may be 0
                    zero_indices = isinf(revRates);
                    revRates(zero_indices) = 0;
                    oldSumR = sum(revRates);

                    if oldSumR ~= 0
                        oldOneR = 1 / oldSumR;
                        oldRates = ones(N, 1) .* oldOneR;
                        oldRates(zero_indices) = 0;
                    end

                    revRates = 1 ./ ratesOneAPNew;
                    %some  of them may be 0
                    zero_indices = isinf(revRates);
                    revRates(zero_indices) = 0;
                    SumR = sum(revRates);

                    if SumR ~= 0
                        OneR = 1 / SumR;
                        updatedRates = ones(N, 1) .* OneR;
                        updatedRates(zero_indices) = 0;
                    end
                end

                delta = updatedRates - oldRates;
                delta_t = sum(delta);

                oldLog = log(oldRates);
                newLog = log(updatedRates);
                oldLog(isinf(oldLog)) = 0;
                newLog(isinf(newLog))= 0;


                %div(isinf(div)) = 0;
                %div = div(div > 0);
                %delta_v = log(prod(div));
                delta_v = sum(newLog - oldLog);

                obj.cur_metrics(1, i) = obj.cur_metrics(1, i) + delta_t;
                obj.cur_metrics(2, i) = obj.cur_metrics(2, i) - delta_v;

                %otherVInOff = myfunInsideS(obj.cur_solutions(:, :, i))
                %disp(obj.cur_metrics(2, i))
                %assert(abs(otherVInOff -obj.cur_metrics(2, i)) < 0.00001);
                
                index = find(old_esol > 0);
                obj.cur_rates(index, i) = updatedRates(index);
                obj.cur_rates(flowID, i) = updatedRates(flowID);
                [T, tfi] = sumTFromTArray(obj.cur_rates(:, i));
                obj.cur_metrics(3, i) = tfi;
                %assert(abs(T- obj.cur_metrics(1, i)) < 0.00001);

                %oldRates_sq = oldRates .* oldRates;
            end

        end

    end

end
