
function [sol, finalV,  exitcode, policyV,...
    outS, intV, intx, xa,  bfx, bfV, startArray, startxV,...
    policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
    atomx, atomV, atomT, atomJ, snrMatrix] =mainljwRateNs3RandomSNR()

%global disToBs;
global NodeApSNR;
global isBoth;
global underwifiPer;

global N;
global M;




%rng(seed)

assert(N>M);
%N >M assert

%    r used in myfunInside, that's why we make it global
%global r
%r = rand(N, M);
%randM = r;

lb = zeros(N, M);
ub = ones(N, M);

Aeq_m = eye(N, N);
Aeq = Aeq_m;

for i=1:M-1
    Aeq = [Aeq, Aeq_m];
end

%     A_m = eye(M, M);
%     A = A_m;
%
%     for i=1:N-1
%         A = [A, A_m];
%     end
%     A = -A;
%
%     disp(A)


A = zeros(M, M*N);

for i= 1:M
    A_m = zeros(M, N);
    A_m(i, :) = ones(1, N);
    A(:, N*(i-1)+1:N*(i)) = A_m;
end
A = -A;

%disp(A)

b =-ones(M,1);


beq=ones(N, 1);

%beq = beq* M*0.8;
%disp(beq);

%GetRandomNodeApSNRUpdate();
snrMatrix = NodeApSNR;
%distGenRandom();
%initWThrough();
%try to use multiple staring point
TRY_DIFF_START_POINT = 5;
values = zeros(1, TRY_DIFF_START_POINT);
solutions = cell(1, TRY_DIFF_START_POINT);
outputs = cell(1, TRY_DIFF_START_POINT);
exitfs = zeros(1, TRY_DIFF_START_POINT);
diffs = zeros(1, TRY_DIFF_START_POINT);

%only wifi's distance, so -1
xa = zeros(N, M); %%%modify with SNR max value of each row
for i=1: N
    %dis = zeros(1,M);
    %for j=1:M
%     disp(M);
%     disp(N);
%     disp(NodeApSNR);
    dis = NodeApSNR(i,:);
    %end
    [maxD, index] = max(dis);
    
    %connect to the cloest wifi
    
        xa(i, index)  =1;     
   
end

policyV = myfunInsideS(xa);
[policyT, policyJ] = sumT(xa);

[bfV, bfx] = bfMore();
[bfT, bfJ] = sumT(bfx);

[atomV, atomx] = atom_ljw();
[atomT, atomJ] = sumT(atomx);

startArray = zeros(TRY_DIFF_START_POINT, 3);
startxV = cell(1, TRY_DIFF_START_POINT);

for k=1:TRY_DIFF_START_POINT
    x0 = zeros(N,M);
    taken = zeros(1, M);
    
    %this gurantee at least one user is connected to every ap
    for i=1:M
        ii = randi(N);
        x0(ii, i) = 1;
        taken(i) = ii;
    end
    
    for i=1: N
        %j = mod(i, M) +1;
        %randomly select an ap to associate to
        j = randi(M);
        
        %if it is not the assigned node in the last init step
        lia = ismember(i, taken);
        if lia==0
            x0(i, j) = 1;
        end
    end
    
    startV = myfunInsideS(x0);
    [startT, startJ] = sumT(x0);
    
    startArray(k, 1) = startV;
    startArray(k, 2) = startT;
    startArray(k, 3) = startJ;
    
    startxV{1, k} = x0;
    
    
    
    
    %set the iterations we want to run
    opts = optimoptions(@fmincon, 'MaxFunctionEvaluations', 5000*N);
    %opts = optimoptions(@fmincon,'Algorithm','sqp', 'MaxFunctionEvaluations', 5000);
    
    [x,fval,exitflag,output] = fmincon(@myfunInsideS,x0,A,b,Aeq,beq,lb,ub,null_nonlin, opts);
    
    values(1, k) = fval;
    solutions{1, k} = x;
    diff =  startV - fval;
    exitfs(1, k) = exitflag;
    outputs{1, k} = output;
    diffs(1, k) = diff;
    
end


[finalV, b] = max(values);
sol = solutions{1, b};
exitcode = exitfs(1, b);
outS = outputs{1, b};

[solT, solJ] = sumT(sol);

intx = round(sol);

%finalRealV = myfunInsideS(sol);

intV = myfunInsideS(intx);
[intT, intJ] = sumT(intx);




end

function [c,ceq] = null_nonlin(x)
c = [];
ceq = [];
end

