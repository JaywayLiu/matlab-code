
rng('default');
rng(1);
TestN = 50;
sols = cell(1, TestN);

int_sols = cell(1, TestN);

outputs = cell(1, TestN);
startVs = zeros(1, TestN);
finalVs = zeros(1, TestN);
exs = zeros(1, TestN);
intVs = zeros(1, TestN);

nFracs = zeros(1, TestN);
fracPers = zeros(1, TestN);

randomM = cell(1, TestN);

%intDiff= zeros(1, TestN);

for i=1:TestN 
    [sol, finalV, exitcode, startV, output, intV, rM] = mainljw();
    sols{1, i} = sol;
    randomM{1,i}  = rM;
    startVs(1, i) = startV;
    finalVs(1, i) = finalV;
    exs(1, i) = exitcode;
    outputs{1, i} = output;
    intVs(1, i) = intV;
end

for i=1:TestN 
    [aa, bb] =max(sols{1}, [], 2);
   % int_sols{1, i}  = round(sols{i});
   sizeSol = size(sols{1});
        int_sols{1, i}  = genIntSol(bb, sizeSol(2));
    [nFracs(i), fracPers(i)] = countFrac(sols{i});
    %intDiff(i) = 

end



intDiff = finalVs - intVs;
meanIntDiff = mean(intDiff)
stdIntDiff = std(intDiff)

meanInt = mean(intVs);

normDiff = intDiff ./meanInt  *100

impP = (startVs- finalVs) ./ (-startVs)



