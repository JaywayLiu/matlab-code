function [sol, finalV,  exitcode, policyV,...
    outS, intV, intx, xa, D, bfx, bfV, startArray, startxV,...
    policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
    atomx, atomV, atomT, atomJ,...
    ratediff_bf, ratediff_int, ratediff_atom, ratediff_sol,...
    rateVs...
    ]=mainljwRateNs3(isAddErr0)
%res_atom_raw, res_int_raw, res_bf_raw, res_policy_raw

global N;
global M;
global disToBs;
global isDist;
global isBF;

isDist = 1;

if nargin<1
	isAddErr = 0;
else 
	isAddErr = isAddErr0;
end

%rng(seed)
assert(N>M);
%N >M assert

%    r used in myfunInside, that's why we make it global
%global r
%r = rand(N, M);
%randM = r;

lb = zeros(N, M);
ub = ones(N, M);

Aeq_m = eye(N, N);
Aeq = Aeq_m;

for i=1:M-1
    Aeq = [Aeq, Aeq_m];
end



A = zeros(M, M*N);

for i= 1:M
    A_m = zeros(M, N);
    A_m(i, :) = ones(1, N);
    A(:, N*(i-1)+1:N*(i)) = A_m;
end
A = -A;

%disp(A)

b =-ones(M,1);


beq=ones(N, 1);

%beq = beq* M*0.8;
%disp(beq);


distGenRandom();
initWThrough();
%try to use multiple staring point
TRY_DIFF_START_POINT = 5;
values = zeros(1, TRY_DIFF_START_POINT);
solutions = cell(1, TRY_DIFF_START_POINT);
outputs = cell(1, TRY_DIFF_START_POINT);
exitfs = zeros(1, TRY_DIFF_START_POINT);
diffs = zeros(1, TRY_DIFF_START_POINT);


xa = policyGen();
%disp(xa);

assert(isequal(sum(xa, 2), ones(N, 1)));

[atomV, atomx] = atom_ljw();

if isBF
	[bfV, bfx] = bfMore();
else 
      bfV = atomV;
      bfx = atomx; 
end



startArray = zeros(TRY_DIFF_START_POINT, 3);
startxV = cell(1, TRY_DIFF_START_POINT);

maxRandomV = -1e10;
maxRandomI = 0;
randomx = zeros(N,M);

for k=1:TRY_DIFF_START_POINT
    x0 = zeros(N,M);
    taken = zeros(1, M);
    
    %this gurantee at least one user is connected to every ap
    for i=1:M
        ii = randi(N);
        x0(ii, i) = 1;
        taken(i) = ii;
    end
    
    for i=1: N
        %j = mod(i, M) +1;
        %randomly select an ap to associate to
        j = randi(M);
        
        %if it is not the assigned node in the last init step
        lia = ismember(i, taken);
        if lia==0
            x0(i, j) = 1;
        end
    end
    
    startxV{1, k} = x0;
     
   startV = myfunInsideS(x0);
    startArray(k, 1) = startV;
    if startV > maxRandomV
       maxRandomV = startV;
      % randomx = x0;
      % maxRandomI = k;
   end 
 
        
    
    %set the iterations we want to run
    opts = optimoptions(@fmincon, 'MaxFunctionEvaluations', 5000*N);
    %opts = optimoptions(@fmincon,'Algorithm','sqp', 'MaxFunctionEvaluations', 5000);
    
    [x,fval,exitflag,output] = fmincon(@myfunInsideS,x0,A,b,Aeq,beq,lb,ub,null_nonlin, opts);
    
    values(1, k) = fval;
    solutions{1, k} = x;
    diff =  startV - fval;
    exitfs(1, k) = exitflag;
    outputs{1, k} = output;
    diffs(1, k) = diff;
    
end


[finalV, b] = max(values);
sol = solutions{1, b};
exitcode = exitfs(1, b);
outS = outputs{1, b};


intx = round(sol);
%/////////////////////////////
%add this if because when running the adding error senario, we only need
%thesolution and then use the real/correct throughts for the
%utility/performance metrics calculations
if ~isAddErr

[res_bf_raw, rate_bf_raw] = calcRates(bfx);
rateV_bf = sum(rate_bf_raw, 2);
[bfT, bfJ] = sumTNorm(rateV_bf, rateV_bf, bfx);

policyV = myfunInsideS(xa);
[res_policy_raw, rate_policy_raw] = calcRates(xa);
rateV_policy = sum(rate_policy_raw, 2);
[policyT, policyJ] = sumTNorm(rateV_bf, rateV_policy, xa);

[res_sol_raw, rate_sol_raw] = calcRates(sol);
rateV_sol = sum(rate_sol_raw, 2);
[solT, solJ] = sumTNorm(rateV_bf, rateV_sol, sol);


intV = myfunInsideS(intx);
[res_int_raw, rate_int_raw] = calcRates(intx);
rateV_int = sum(rate_int_raw, 2);
[intT, intJ] = sumTNorm(rateV_bf, rateV_int, intx);

for ii = 1:length(startxV) 
    [res_random_raw, rate_random_raw] = calcRates(startxV{ii});
    rate_random = sum(rate_random_raw, 2);
    [ranT, ranJ] = sumTNorm(rateV_bf, rate_random, startxV{ii});
    startArray(ii, 2) = ranT;
    startArray(ii, 3) = ranJ;
end;


   


[res_atom_raw, rate_atom_raw] = calcRates(atomx);
rateV_atom = sum(rate_atom_raw, 2);
[atomT, atomJ] = sumTNorm(rateV_bf, rateV_atom, atomx);

ratesV = zeros(M, 5);

    rateVs(:,1) =rateV_policy;
    rateVs(:,2) =rateV_bf ;
    rateVs(:,3) = rateV_int;
    rateVs(:,4) = rateV_atom;
    rateVs(:,5) = rateV_sol;
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ratediff_bf = ((rateV_bf - rateV_policy) ./ rateV_policy)';
ratediff_atom = ((rateV_atom - rateV_policy) ./ rateV_policy)';
ratediff_int = ((rateV_int- rateV_policy) ./ rateV_policy)';
ratediff_sol = ((rateV_sol - rateV_policy) ./ rateV_policy)';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
[intV , intJ ,intT , atomJ , atomT , atomV ] = deal(-1); 
[solT , solJ ]= deal(-1);
[ policyT , policyV , policyJ ] = deal(-1);
[bfT , bfJ , bfV ] = deal(-1);


end

D = disToBs;

end

function [c,ceq] = null_nonlin(x)
c = [];
ceq = [];
end




%write another function to make rateGen follow certain distribution














