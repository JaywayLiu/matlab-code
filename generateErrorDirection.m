function generateErrorDirection()
global errorDirection;
global N;
global M;
for i = 1:N
    for j = 1:M
        randplus = rand();
        if randplus <0.5
            errorDirection(i, j) = -1;
        else
            errorDirection(i, j) = 1;
            
        end
    end
end
end