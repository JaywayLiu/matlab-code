function f = myfun(x)
    N = 5;
    M = 3;
    %global M
    %global N
    %disp('M=')
    %disp(M)
    f=0;
    r = rand(N, M) * 10000000;
    for i=1:N
        for j=1:M
            b=sum(x,1);
            if x(i,j)~=0
              f = f+ log(r(i, j) * x(i,j) * (1/ b(j)));
            end
        end
    end
    f =-f;
end

