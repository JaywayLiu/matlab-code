function [newV, newX, newNLte, newRoundTimeJ, assignedToWifi] = calcWiFiAPIncrePartial(j, x, ci, currentV, MaxRate, N, M, nLte, roundTimeJ)
    %global isDist;
    %ci are the indicies of UEs that has MaxRate >0 and not set
    %make sure if ci is empty, this won't be called
    global isOnOff;
    global onOffMap;

    global nonParInfo;

    %only get the ones that not in the LTE yet
    ciNotLte = ci(x(ci, 1) == 0);

    newNLte = nLte+ length(ciNotLte);
    nLteAdd = nLte + nonParInfo{1}(1, 1); 
    newNLteAdd = nLteAdd + length(ciNotLte);

    if nLte ~= 0
        %still use nLte * here, cause we only care about the participants
        oldDelta = nLte * log(nLteAdd / newNLteAdd);
    else
        oldDelta = 0;
    end

    % currentV
    % oldDelta
    % ciNotLte
    % newNLte
    % MaxRate(ciNotLte, 1)



    newV = currentV + oldDelta + sum(log(MaxRate(ciNotLte, 1) / newNLteAdd));
    %assert(~isinf(newV))
    newX = x;

    %this is the round time added the hidden traffic
    newRoundTimeJ = roundTimeJ;
    %delta  =1;
    newX(ciNotLte, 1) = 1;
    assignedToWifi = [];

    %newV is the current V value
    kk = 1;

    while 1
        %maxV = -1e10000;
        maxV = newV;
        maxx = zeros(N, M);
        maxI = 0;

        for k = 1:length(ci)
            i = ci(k);
            %nv = myfunInsid,eS(xtemp);
            %try to move i from LTE to AP j
            [nv, xtemp, roundTimeJTemp] = moveEvalPartial(i, j, MaxRate, newV, newX, newNLte, newNLteAdd, newRoundTimeJ);

            %assert(~isinf(nv))

            if nv > maxV
                maxV = nv;
                maxx = xtemp;
                maxI = k;
                maxRT = roundTimeJTemp;
            end

        end

        % if the min is smaller than  the current value, we can continue
        % if equal will stop because of the top while
        %delta  = newV - maxV;
        if maxV > newV
            newV = maxV;
            newX = maxx;
            newRoundTimeJ = maxRT;
            assignedToWifi(kk) = ci(maxI);
            kk = kk +1;
            %if maxI ~=0
            ci(maxI) = [];
            %end
            newNLte = newNLte - 1;
        else
            break;
        end

    end

    %newV = -newV;

end
