function [outputFolder, fullDataFolder]= mainPrintAndCallerFixedMCS(timeStamp, deployRatioIn, isOptIn)
tic
isAddErr = 0;
global wifiPos;
global lteMCSIndex;
global wifiMCSIndex;
global MaxRate;

global isWiFiPF;

%[re, compName] = getenv('computername');
[re, compName] = system('hostname'); 
disp('compName=');
disp(compName);
%dataFolder = 'mcs_20181113T111754';
if isequal( compName , 'usignite')
    isPa = 0;
elseif isequal( compName(1:6), 'apollo')
	isPa = 2;
else
    isPa =1;
end

isPa = 2;

disp('isPa=');
disp(isPa);
%this is the one for the figures

%no / allowd in datafolder, need to concat later!!!
if isPa ==0
    dataFolder = 'mcs_12-4-256_20190310T113336';
else
    %dataFolder = 'mcs_12-4-256_20190310T113336';
%	dataFolder =  'mcs_32-4-256_20190317T164724';
%	dataFolder = 'mcs_32-4-32768_20190317T164806';
  %dataFolder = 'mcs_12-4-256_20190609T114611';
%	dataFolder = 'mcs_12-4-16384_20190612T114750';
	if (wifiPos == 0.5)
	dataFolder = 'mcs_12-4-16384-500_20190614T230829';
        elseif ( wifiPos == 2/3)
%u	dataFolder = 'mcs_12-4-16384-667_20190614T231201';
	
%dataFolder = 'mcs_32-4-16384-667_20190617T231357';
	%dataFolder = 'mcs_5-3-256-667_20190622T143757';
	%dataFolder = 'mcs_12-4-16384-667_20190625T223011';
	dataFolder = 'mcs_12-4-16384-667_20190702T001509';
  dataFolder = 'mcs_12-4-16384-667_20191010T100942';
dataFolder = 'mcs_32-4-16384-667_20191102T172358_cluster-circ'
%dataFolder = 'mcs_32-4-16384-667_20191017T222005'
  %dataFolder = 'mcs_32-4-16384-667_20191031T154538_cluster-circ';
%  dataFolder = 'mcs_12-4-10-667-20250825T113122'
%dataFolder  = 'mcs_12-4-16384-667_20251010T100943'
  %dataFolder = 'mcs_6-4-256-667_20191013T111109'
	%dataFolder = 'mcs_32-4-16384-667_20190702T183414';
%	dataFolder = 'mcs_5-3-256-667_20190811T115246';
	end
end

pwdString = pwd;
fullDataFolder = strcat(pwdString,  '/' , dataFolder);
disp(fullDataFolder);

sizes = sscanf(dataFolder, 'mcs_%d-%d-%d-%d_%dT%d');
%dataFolder = 'mcs_20-4-300_20190111T075246';
%dataFolder ='mcs_12-4-1000_20190124T205110';
%dataFodler = 'mcs_12-4-1000_20190224T111001-both11n';
global isOpt;
global isSaveMat;
global isDebug;
global isWiFiPre;
global isDist;
global isMexBf;
global isBF;

isDist = 0; 
isSaveMat = 0;
isDebug = 0;
isWiFiPre = 0;
isMexBf = 1;


%///add new global from DisRunner.m
    global isNotConvex;
    global isRunRandom;
    isNotConvex = 1;
%%%%%%%%%%%%%%%%%%%

  %isBF = 1;

%folderString= 'dist-sym-partial-%d-%d-%d-%d';
folderString= dataFolder;

if isBF
  folderString =strcat(folderString, '-bf-new');
else
  folderString =strcat(folderString, '-nobf');
end 

if isWiFiPF
  folderString =strcat(folderString, '-wifipf');
else
  folderString =strcat(folderString, '-nowifipf');
end 


isOpt = isOptIn;


if ~isPa
    outputFolder='';
elseif isPa ==2
    outputFolder = '/users/jianwel/';
elseif isPa == 1
    outputFolder='/scratch2/jianwel/';
elseif isPa == 3
    outputFolder='/scratch/jianwel/';
end

%outputFolder = strcat(outputFolder, 'mcsDataOutput/32k-nobf-partial/');
%outputFolder = strcat(outputFolder, 'mcsDataOutput/800ns-16k-new-nobf-partial/');
outputString= sprintf(folderString, sizes(1), sizes(2), sizes(3), sizes(4));
outputFolder = strcat(outputFolder, 'mcsDataOutput/', outputString, '/');
%outputFolder = strcat(outputFolder, 'mcsDataOutput/new-400ns-16k-new-nobf-partial/');
checkDir(outputFolder);
outputFolder = strcat(outputFolder, sprintf('opt=%d', isOpt));
checkDir(outputFolder);

outputFolder = strcat(outputFolder, '/');
%outputFolder='mcsDataOutput/westall/';
%outputFolder='mcsDataOutput/256TestRealLTE/';
%outputFolder='mcsDataOutput/256TestPartialNoOpt/';
%outputFolder='maxRateOutput/';
%outputFolder='mcsDataOutput/256Test/';
%outputFolder='mcsDataOutput/testbf';
outputFolder = strcat(outputFolder, sprintf('%.4f', deployRatioIn), '/');
checkDir(outputFolder);
disp('output=')
disp(outputFolder)


global N;
global M;
global isBoth;
global isAtomOnly;

global deployRatio;
deployRatio = deployRatioIn;
global isThroughputScaling;

global iSMcsIndexFromFile;
global isMcsIndexFromDistFiles;
global isFixMaxRate;
isFixMaxRate = 0;

isMcsIndexFromDistFiles = 1;
iSMcsIndexFromFile = 0;
isThroughputScaling = 0;
isPartialOpt = 1;

disp('timeStamp=');
disp(timeStamp);

if (ischar(timeStamp))
  disp('in timestamp conversion');
  timeStamp = str2num(timeStamp);
end

if (ischar(deployRatioIn))

  disp('in deploy ratio conversion');
  timeStamp = str2num(deployRatioIn);
end

deployRatio = deployRatioIn;

disp('deploy Ratio=');
disp(deployRatio);

%N = 20;
N = sizes(1);
%N =5;

M = sizes(2);
nn = N;
mm = M;


isBoth =1;
isAtomOnly =1;
nCompare = 8;

%%%%%%%%%%%%%%%files
fileName = sprintf('%d-%d-%06d', nn, mm, timeStamp);

if isBoth
    fileName = strcat(fileName,'-both');
else
    fileName = strcat(fileName,'-pfonly');
   % fileNametime = strcat(fileNametime,'-pfonly');
end

fileNametime = strcat(fileName, '-time');
fileNameBase = fileName;

fileName = strcat(fileName,'.txt');
fileNametime = strcat(fileNametime, '.txt');

fileID = fopen(strcat(outputFolder,fileName), 'w');
fileTime = fopen(strcat(outputFolder, fileNametime), 'w');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xArray = cell(1, nCompare);

%ratediffMethods = {'bf', 'pe', 'atom', 'gr'};
%ratesMethods = {'policy', 'bf', 'int', 'atom', 'sol'};
%ratesMethods = {'policy', 'bf',  'atom', 'gg', 'pe', 'gr'};

ratesMethods = {'lgw', 'opt',  'atom', 'gg', 'lge', 'rand'};
%ratediffM = zeros( N, length(ratediffMethods));

fileHandles=[];

for kk=1:length(ratesMethods)
    ratesName = strcat(fileNameBase, '_', char(ratesMethods(kk)), '_rates.txt');
    fileHandles(kk) = fopen(strcat(outputFolder,ratesName), 'w');
end

initAPLocations(fullDataFolder);
initUELocations(fullDataFolder, timeStamp);


    
%    [exitcode, outS, D,...
%    policyx, policyV, policyT, policyJ, policyNJ, policyMM, policyTFI, ...
%      intx, intV, intT, intJ, intNJ, intMM, intTFI, ...
%      bfx, bfV, bfT, bfJ, bfNJ, bfMM, bfTFI, ...
%      startArray, startxV,...
%     solx, solV, solT, solJ, solNJ, solMM, solTFI, ...
%     atomx, atomV, atomT, atomJ, atomNJ, atomMM, atomTFI, ...
%     ggx, ggV, ggT, ggJ, ggNJ, ggMM, ggTFI, ...
%     grx, grV, grT, grJ, grNJ, grMM, grTFI, ...
%     pex, peV, peT, peJ, peNJ, peMM, peTFI, ...
%      ratediff_atom,...
%     rateVs,...
%     MaxRate...
%     ]  = mainljwRateNs3FromFile(isAddErr, dataFolder, timeStamp);
prev_pex = zeros(N, M);
prev_policyx = prev_pex; 


[exitcode, outS, D, ...
        policyx, policyV, policyT, policyJ, policyNJ, policyMM, ...
        intx, intV, intT, intJ, intNJ, intMM, ...
        bfx, bfV, bfT, bfJ, bfNJ, bfMM, ...
         startxV, ...
        solx, solV, solT, solJ, solNJ, solMM, ...
        atomx, atomV, atomT, atomJ, atomNJ, atomMM, ...
        ggx, ggV, ggT, ggJ, ggNJ, ggMM, ...
        grx, grV, grT, grJ, grNJ, grMM, ...
        pex, peV, peT, peJ, peNJ, peMM, ...
        rateVs, resFs] = mainljwForDiscrete(1, prev_pex, prev_policyx);
 
    if (isDebug)
        disp(MaxRate);
    end
%     ratediffM(:, 1) = ratediff_bf;
%     ratediffM(:, 2) = ratediff_int;
%     ratediffM(:, 3) = ratediff_atom;
%     ratediffM(:, 4) = ratediff_sol;
   if (deployRatio==1 && isOpt ==0)  
    maxRatesName = strcat(fileNameBase,  '_MaxRate.txt');
    csvwrite(strcat(outputFolder, maxRatesName), MaxRate);

    lteMcsName = strcat(fileNameBase,  '_lteMcs.txt');
    csvwrite(strcat(outputFolder, lteMcsName), lteMCSIndex);

    wifiMcsName = strcat(fileNameBase,  '_wifiMcs.txt');
    csvwrite(strcat(outputFolder, wifiMcsName), wifiMCSIndex)
  end

    %xArray{1, 1} = startxV;
    
%fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);

        xArray{1, 1} = policyx;
        xArray{1, 2} = bfx;
         % xArray{1, 4} = intx;
          %xArray{1, 5} = solx;
          xArray{1, 3} = atomx;
          xArray{1, 4} = ggx;
          xArray{1, 5} = pex;
          xArray{1, 6} = grx;
    
    for kk=1:length(ratesMethods)
        writeVectorToFile(rateVs(:,kk), fileHandles(kk));
        solutionName = strcat(fileNameBase,  '_', char(ratesMethods(kk)), '_x.txt');
        csvwrite(strcat(outputFolder, solutionName), xArray{1, kk});
    end
    
    
    
          
   
    fprintf(fileID, ['%06d %4f %4f %4f %4f %4f %4f '...
         '%4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f '...
	'%4f %4f %4f %4f %4f %4f '...
	'%4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f %4f %4f\n'],...
        timeStamp,...
      -0, -0, -0, ...
      0, 0, 0, ...
      -policyV, policyT, policyJ, ...
      -bfV, bfT, bfJ, ...
      -intV, intT, intJ, ...
      -solV, solT, solJ,...
      -atomV, atomT, atomJ, ...
      -ggV, ggT, ggJ, ...
      0, 0, 0,... 
      -peV, peT, peJ,...
      -grV, grT, grJ,...
      policyNJ, bfNJ, intNJ, solNJ, atomNJ, ggNJ, peNJ, grNJ,...
      resFs(1, 1), resFs(1, 2), 0, 0, resFs(1, 3), resFs(1, 4), resFs(1, 5), resFs(1, 6),...  
     policyMM, bfMM, intMM, solMM, atomMM, ggMM,  peMM, grMM);
  
  %3x10 before the NJ part  8 NJs

  
fclose(fileID);

%fprintf(fileDiff, '%.4f\n', ratediff_atom);
%fclose(fileDiff);


for kk=1:length(ratesMethods)
    fclose(fileHandles(kk));
end


if isSaveMat
    save(strcat(outputFolder, fileNameBase, '.mat'));
end



toc
fprintf(fileTime, '%f\n', toc);
fclose(fileTime);

end
