function f = calcSumRInverse(rateArray)
%  ir = ones(size(rateArray))./rateArray;
ir = 1 ./ rateArray;
%some  of them may be 0
zero_indices = isinf(ir);
ir(zero_indices) = 0;
  f = sum(ir);
end