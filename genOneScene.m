function genOneScene()

datestring = datestr(datetime('now'), 'yyyymmddTHHMMSS');
folderName = strcat('mobility_',datestring);
mkdir(folderName);
cd(folderName);

nSteps  = 100;
nUE = 20;

lteAPDist = [5000, 10000];
csvwrite('lte_ap.txt', lteAPDist);

wifiRadius = 125;
nAP = 5;
spotRange = 80;
rangeLimit = [400, 400];
alpha = 1.2;

fileID = fopen('scene-config.txt', 'w');
fprintf(fileID, '%d\n', nSteps);
fprintf(fileID, '%d %d\n', nAP, nUE);
fprintf(fileID, '%.4f\n', wifiRadius);
fprintf(fileID, '%.4f\n', spotRange);
fprintf(fileID, '%.4f %.4f\n', rangeLimit(1), rangeLimit(2));
fprintf(fileID, '%.4f\n', alpha);
fclose(fileID);


drawOneSpotFunc(wifiRadius, true, nAP, spotRange, rangeLimit);

generateRandomPlacement(nSteps, nUE, rangeLimit, alpha);



cd('..');
end