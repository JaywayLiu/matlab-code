function mainPrintAndCallerFixedMCS(timeStamp, deployRatioIn, isOptIn)
tic
isAddErr = 0;

%[re, compName] = getenv('computername');
[re, compName] = system('hostname'); 
disp('compName=');
disp(compName);
%dataFolder = 'mcs_20181113T111754';
if isequal( compName , 'usignite')
    isPa = 0;
elseif isequal( compName(1:6), 'apollo')
	isPa = 2;
else
    isPa =1;
end

isPa = 2;

disp('isPa=');
disp(isPa);
%this is the one for the figures

if isPa ==0
    dataFolder = 'mcs_12-4-256_20190310T113336';
else
    %dataFolder = 'mcs_12-4-256_20190310T113336';
%	dataFolder =  'mcs_32-4-256_20190317T164724';
	dataFolder = 'mcs_32-4-32768_20190317T164806';
end
disp(dataFolder);
sizes = sscanf(dataFolder, 'mcs_%d-%d-%d_%dT%d');
%dataFolder = 'mcs_20-4-300_20190111T075246';
%dataFolder ='mcs_12-4-1000_20190124T205110';
%dataFodler = 'mcs_12-4-1000_20190224T111001-both11n';
global isOpt;
global isSaveMat;
global isDebug;
global isWiFiPre;


isSaveMat = 0;
isDebug = 0;
isWiFiPre = 0;


isOpt = isOptIn;


if ~isPa
    outputFolder='';
elseif isPa ==2
    outputFolder = '/users/jianwel/';
elseif isPa == 1
    outputFolder='/scratch2/jianwel/';
end

%outputFolder = strcat(outputFolder, 'mcsDataOutput/32k-nobf-partial/');
%outputFolder = strcat(outputFolder, 'mcsDataOutput/800ns-16k-new-nobf-partial/');
outputFolder = strcat(outputFolder, 'mcsDataOutput/new-400ns-16k-new-nobf-partial/');
checkDir(outputFolder);
outputFolder = strcat(outputFolder, sprintf('opt=%d', isOpt));
checkDir(outputFolder);

outputFolder = strcat(outputFolder, '/');
%outputFolder='mcsDataOutput/westall/';
%outputFolder='mcsDataOutput/256TestRealLTE/';
%outputFolder='mcsDataOutput/256TestPartialNoOpt/';
%outputFolder='maxRateOutput/';
%outputFolder='mcsDataOutput/256Test/';
%outputFolder='mcsDataOutput/testbf';
outputFolder = strcat(outputFolder, sprintf('%.4f', deployRatioIn), '/');

outputFolder = 'lg-test';
checkDir(outputFolder);
disp('output=')
disp(outputFolder)


global N;
global M;
global isBoth;
global isAtomOnly;
global isBF;

global deployRatio;
deployRatio = deployRatioIn;
global isThroughputScaling;

global iSMcsIndexFromFile;
global isFixMaxRate;
isFixMaxRate = 1;

iSMcsIndexFromFile = 1;
isThroughputScaling = 0;
isPartialOpt = 1;

disp('timeStamp=');
disp(timeStamp);

if (ischar(timeStamp))
  disp('in timestamp conversion');
  timeStamp = str2num(timeStamp);
end

if (ischar(deployRatioIn))

  disp('in deploy ratio conversion');
  timeStamp = str2num(deployRatioIn);
end

deployRatio = deployRatioIn;

disp('deploy Ratio=');
disp(deployRatio);

%N = 20;
N = sizes(1);
%N =5;

M = sizes(2);
nn = N;
mm = M;

isBF =0;

isBoth =1;
isAtomOnly =1;
nCompare = 8;

%%%%%%%%%%%%%%%files
fileName = sprintf('%d-%d-test%d', nn, mm, timeStamp);

if isBoth
    fileName = strcat(fileName,'-both');
else
    fileName = strcat(fileName,'-pfonly');
   % fileNametime = strcat(fileNametime,'-pfonly');
end

fileNametime = strcat(fileName, '-time');
fileNameBase = fileName;

fileName = strcat(fileName,'.txt');
fileNametime = strcat(fileNametime, '.txt');

fileID = fopen(strcat(outputFolder,fileName), 'w');
fileTime = fopen(strcat(outputFolder, fileNametime), 'w');

rateDiffFileName = strcat(fileNameBase, '-diff.txt');
fileDiff = fopen(strcat(outputFolder, rateDiffFileName), 'w');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xArray = cell(1, nCompare);

%ratediffMethods = {'bf', 'int', 'atom', 'sol'};
%ratesMethods = {'policy', 'bf', 'int', 'atom', 'sol'};
ratesMethods = {'policy', 'bf',  'atom', 'gg', 'pe', 'gr'};
%ratediffM = zeros( N, length(ratediffMethods));

fileHandles=[];

for kk=1:length(ratesMethods)
    ratesName = strcat(fileNameBase, sprintf('_%05d', timeStamp), '_', char(ratesMethods(kk)), '_rates.txt');
    fileHandles(kk) = fopen(strcat(outputFolder,ratesName), 'w');
end

% fileHandlesX=[];
% for kk=1:length(ratesMethods)
%     solutionName = strcat(fileNameBase, sprintf('_%05d', timeStamp), '_', char(ratesMethods(kk)), '_x.txt');
%     fileHandlesX(kk) = fopen(strcat(outputFolder,ratesName), 'w');
% end

maxRatesName = strcat(fileNameBase, sprintf('_%05d', timeStamp),  '_MaxRate.txt');
%fileMaxRate = fopen(strcat(outputFolder, maxRatesName), 'w');

[ exitcode, outS, D,...
   policyx, policyV, policyT, policyJ, policyNJ, policyMM,...
     intx, intV, intT, intJ, intNJ, intMM, ...
     bfx, bfV, bfT, bfJ, bfNJ, bfMM,...
     startArray, startxV,...
    solx, solV, solT, solJ, solNJ, solMM, ...
    atomx, atomV, atomT, atomJ, atomNJ, atomMM,...
    ggx, ggV, ggT, ggJ, ggNJ, ggMM, ...
     grx, grV, grT, grJ, grNJ, grMM, ...
    pex, peV, peT, peJ, peNJ, peMM,...
     ratediff_atom,...
    rateVs,...
    MaxRate...
    ]=mainljwRateNs3FromFile(isAddErr, dataFolder, timeStamp);
 
    if (isDebug)
        disp(MaxRate);
    end
%     ratediffM(:, 1) = ratediff_bf;
%     ratediffM(:, 2) = ratediff_int;
%     ratediffM(:, 3) = ratediff_atom;
%     ratediffM(:, 4) = ratediff_sol;
    
    csvwrite(strcat(outputFolder, maxRatesName), MaxRate);
    
    %xArray{1, 1} = startxV;
    
%fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);

        xArray{1, 1} = policyx;
        xArray{1, 2} = bfx;
         % xArray{1, 4} = intx;
          %xArray{1, 5} = solx;
          xArray{1, 3} = atomx;
          xArray{1, 4} = ggx;
          xArray{1, 5} = pex;
          xArray{1, 6} = grx;
    
    for kk=1:length(ratesMethods)
        writeVectorToFile(rateVs(:,kk), fileHandles(kk));
        solutionName = strcat(fileNameBase, sprintf('_%05d', timeStamp), '_', char(ratesMethods(kk)), '_x.txt');
        %disp('solutionName=');
        %disp(strcat(outputFolder, solutionName));
        csvwrite(strcat(outputFolder, solutionName), xArray{1, kk});
    end
    
    
    
          
    startva = mean(startArray, 1);
    startstd = std(startArray, 1);
    [randomV, randomI] = max(startArray(:, 1));
    randomT  = startArray(randomI, 2);
    randomJ  = startArray(randomI, 3);
    %randomNJ  = startArray(randomI, 4);
    
    fprintf(fileID, ['%4f %4f %4f %4f %4f %4f '...
         '%4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f '...
	'%4f %4f %4f %4f %4f %4f '...
	'%4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f %4f %4f\n'],...
      -startva(1), -startva(2), -startva(3), ...
      startstd(1), startstd(2), startstd(3), ...
      -policyV, policyT, policyJ, ...
      -bfV, bfT, bfJ, ...
      -intV, intT, intJ, ...
      -solV, solT, solJ,...
      -atomV, atomT, atomJ, ...
      -ggV, ggT, ggJ, ...
      -randomV, randomT, randomJ,... 
      -peV, peT, peJ,...
      -grV, grT, grJ,...
      policyNJ, bfNJ, intNJ, solNJ, atomNJ, ggNJ, peNJ, grNJ,...
     policyMM, bfMM, intMM, solMM, atomMM, ggMM,  peMM, grMM);
  
  %3x10 before the NJ part  8 NJs

  
fclose(fileID);

fprintf(fileDiff, '%.4f\n', ratediff_atom);
fclose(fileDiff);


for kk=1:length(ratesMethods)
    fclose(fileHandles(kk));
end


if isSaveMat
    save(strcat(outputFolder, fileNameBase, '.mat'));
end



toc
fprintf(fileTime, '%f\n', toc);
fclose(fileTime);

end
