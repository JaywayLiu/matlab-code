function []=GetRandomSNRInitial()
global NodeApSNR;
global N;
global M;
 xmin=2000;
 xmax=21000;
 for i=1:N
     for j=1:M
         distForUE=1000+rand*(xmax-xmin);
         if(distForUE <= 2000)
            NodeApSNR(i,j)=70.3522;
         elseif (distForUE>2000 && distForUE<=3000)
            weigh=(distForUE-2000)/1000;
            NodeApSNR(i,j)=70.3522*(1-weigh) + 31.2676*(weigh); 
         elseif (distForUE>3000 && distForUE<=4000)
            weigh=(distForUE-3000)/1000;
            NodeApSNR(i,j)=31.2676*(1-weigh) + 17.588*(weigh);
         elseif (distForUE>4000 && distForUE<=5000)
            weigh=(distForUE-4000)/1000;
            NodeApSNR(i,j)=17.588*(1-weigh) + 11.2563*(weigh);
         elseif (distForUE>5000 && distForUE<=6000)
            weigh=(distForUE-5000)/1000;
            NodeApSNR(i,j)=11.2563*(1-weigh) + 7.81691*(weigh);  
         elseif (distForUE>6000 && distForUE<=7000)
            weigh=(distForUE-6000)/1000;
            NodeApSNR(i,j)=7.81691*(1-weigh) + 5.74303*(weigh);
         elseif (distForUE>7000 && distForUE<=8000)
            weigh=(distForUE-7000)/1000;
            NodeApSNR(i,j)=5.74303*(1-weigh) + 4.39701*(weigh); 
         elseif (distForUE>8000 && distForUE<=9000)
            weigh=(distForUE-8000)/1000;
            NodeApSNR(i,j)=4.39701*(1-weigh) + 3.47418*(weigh);
         elseif (distForUE>9000 && distForUE<=10000)
            weigh=(distForUE-9000)/1000;
            NodeApSNR(i,j)=3.47418*(1-weigh) + 2.81409*(weigh); 
         elseif (distForUE>10000 && distForUE<=11000)
            weigh=(distForUE-10000)/1000;
            NodeApSNR(i,j)=2.81409*(1-weigh) + 2.32569*(weigh);
         elseif (distForUE>11000 && distForUE<=12000)
            weigh=(distForUE-11000)/1000;
            NodeApSNR(i,j)=2.32569*(1-weigh) + 1.95423*(weigh);
         elseif (distForUE>12000 && distForUE<=14000)
            weigh=(distForUE-12000)/2000;
            NodeApSNR(i,j)=1.95423*(1-weigh) + 1.43576*(weigh); 
         elseif (distForUE>14000 && distForUE<=15000)
            weigh=(distForUE-14000)/1000;
            NodeApSNR(i,j)=1.43576*(1-weigh) + 1.25071*(weigh); 
         elseif (distForUE>15000 && distForUE<=16000)
            weigh=(distForUE-15000)/1000;
            NodeApSNR(i,j)=1.25071*(1-weigh) + 1.09925*(weigh);
         elseif (distForUE>16000 && distForUE<=17000)
            weigh=(distForUE-16000)/1000;
            NodeApSNR(i,j)=1.09925*(1-weigh) + 0.973732*(weigh);
         elseif (distForUE>17000 && distForUE<=18000)
            weigh=(distForUE-17000)/1000;
            NodeApSNR(i,j)=0.973732*(1-weigh) + 0.868545*(weigh);
         elseif (distForUE>18000 && distForUE<=19000)
            weigh=(distForUE-18000)/1000;
            NodeApSNR(i,j)=0.868545*(1-weigh) + 0.779525*(weigh);
         elseif (distForUE>19000 && distForUE<=20000)
            weigh=(distForUE-19000)/1000;
            NodeApSNR(i,j)=0.779525*(1-weigh) + 0.703522*(weigh);  
         elseif (distForUE>20000 && distForUE<=21000)
            weigh=(distForUE-20000)/1000;
            NodeApSNR(i,j)=0.703522*(1-weigh) + 0.638115*(weigh);
         else
            NodeApSNR(i,j)=0; 
         end
             
     end
  end
end