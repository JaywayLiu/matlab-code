function drawOneSpotNorm(wifiRadius, wifiPos, isVis, nWiFiAP, RangeLimit)

global v_edge;
global h_edge;

v_edge = 15;
h_edge = 15;
%wifiRadius = 0.625;

angleStep = 2 * pi /nWiFiAP;
startAngle = pi/2;
randAngle = (startAngle: angleStep: 2*pi+startAngle - angleStep);
randDist = wifiPos;

m_centers = zeros(nWiFiAP, 2);
m_centers(:,1) =  randDist .* sin(randAngle);
m_centers(:,2) =  randDist .* cos(randAngle);
%m_centers  = m_centers + ones(nAP, 2) * RangeLimit/2;


if (isVis)
    
clf
%saveas(gcf,'wifi-pos.pdf')
axis equal
axis([-RangeLimit RangeLimit -RangeLimit RangeLimit]);
xt = get(gca, 'XTick');
set(gca, 'FontSize', 14)


ax = gca;

% outerpos = ax.OuterPosition;
% ti = ax.TightInset; 
% left = outerpos(1) + ti(1);
% bottom = outerpos(2) + ti(2);
% ax_width = outerpos(3) - ti(1) - ti(3);
% ax_height = outerpos(4) - ti(2) - ti(4);
% ax.Position = [left bottom ax_width ax_height];

colors = {'b','r','g','y','k'};

viscircles(ax, [0,0], RangeLimit , 'Color', 'k' , 'LineWidth', 2, 'LineStyle', ':');
viscircles(ax, [0,0], 0.01 , 'Color', 'k', 'LineWidth', 3);

for k=1:nWiFiAP
viscircles(ax, m_centers(k, :),  wifiRadius, 'Color', colors{k}, 'LineWidth', 4);
viscircles(ax, m_centers(k, :), 0.01, 'Color', colors{k}, 'LineWidth', 3);

%rectangle('Position',[0 0 xlim ylim]);
pause(1);
end


end %end if(isVis)
csvwrite('wifi_centers.txt', m_centers);
%print('oneSpot_topo_norm','-dpng')

end


