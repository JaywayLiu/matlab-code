nUE = 50;
nSeconds = 5000;
activeTime = 120;
miu = 80;  %50 seconds
sleepInter = exprnd(miu, nUE, nSeconds/2);


activePercentage = 0.8;
onM = zeros(nUE, nSeconds);

for i = 1:nUE
    isOn = 0;
    isStart = 1;
    milestone =0;
    currentStep = 1;
    idleCount =1;
    
    %include the boundary
    %this means the seconds has passed, will start to count from randStart+1 second
    randStart = randi([0, floor(activeTime + sleepInter(i,idleCount))]);
    left  = activeTime - randStart;
    if (left >0)
        isOn = 1;
        onM(i,1:left) = 1;
        currentStep = left;
    else
        isOn = 0;
        leftIdle = ceil(sleepInter(i,idleCount)) + left;
        onM(i, 1:leftIdle)=0;
        currentStep = leftIdle;
        idleCount = idleCount +1;
    end
    
    %this will make it larger than 5000, but it is OK
    %we can just use those <=5000
    while(currentStep <=5000)
        if (isOn)
            %use one of the idle time
            sleeps = round(sleepInter(i,idleCount));
            onM(i, currentStep:currentStep + sleeps) = 0;
            currentStep = currentStep + sleeps;
            idleCount = idleCount +1;
        else
            onM(i, currentStep: currentStep + activeTime) = 1;
            currentStep = currentStep + activeTime;
        end
        isOn = ~isOn;
    end

end

activeUE = sum(onM(:, 1:5000), 1);
maxUE = max(activeUE)
minUE = min(activeUE)

csvwrite('activeMatrix.csv', onM(:, 1:5000));
csvwrite('activeMatrixSum.csv', activeUE);
save atomPara.mat
