rng('default');
seed = 1;
rng(seed);
TestN = 100;
nCompare =6;
global N;
global M;
global NodeApSNR;
global cof;
global CTLFREQU;
global Band;

global isBoth;
global underwifiPer;

global isDist;
isDist  =0;

%XIN_isboth=[0, 1];

XIN_isboth=[0, 1];
XIN_M=[3];
%XIN_N=[5,10,20];
XIN_N=[5];
XIN_wifi=[0.8,0.4,0.6,0.2];
XIN_fre = [1, 3, 7, 15, 31];

cof=0.3;%the gloabl variable for cof we can change.
Band=10;

snrCell = cell(1, TestN);

isFirstTime = 1;

for a=1:length(XIN_N)
    for b=1:length(XIN_M)
        for c=1:length(XIN_isboth)
            for d=1:length(XIN_wifi)
                for e=1:length(XIN_fre)
                    
                    
                    
                    %%%%%%%%%%%%%%%%parameters we want to change
                    N = XIN_N(a);
                    M = XIN_M(b);
                    CTLFREQU = XIN_fre(e);
                    
                    NodeApSNR=zeros(N,M);
                    isBoth = XIN_isboth(c);
                    underwifiPer = XIN_wifi(d);
                    
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%
                    
                    fileName = sprintf('%d-%d-test%d-%f-control%d', N, M, TestN, underwifiPer, CTLFREQU);
                    
                    if isBoth
                        fileName = strcat(fileName,'-both');
                    else
                        fileName = strcat(fileName,'-pfonly');
                    end
                    
                    fileName = strcat(fileName,'.txt');
                    
                    fileID = fopen(fileName, 'w');
                    fprintf(fileID, '%s %s %s %s %s %s\n',...
                        'start.va start.ta start.ja start.vstd start.tstd start.jstd',...
                        'policy.v policy.t policy.j',...
                        'bf.v bf.t bf.j',...
                        'int.v int.t int.j',...
                        'frac.v frac.t frac.j',...
                        'atom.v atom.t atom.j');
                    
                    tic
                    %store D, x
                    
                    xA = cell(1, nCompare);
                    % snrArray = cell(TestN,1);
                    xArray = cell(TestN, nCompare);
                    if isFirstTime
                        GetRandomSNRInitial();
                    end
                    
                    for i=1:TestN
                        
                        % GetRandomNodeApSNRUpdate() .....
                        if isFirstTime
                            if i~=1
                                GetRandomNodeApSNRUpdate();
                            end
                            snrCell{1, i} = NodeApSNR;
                        else
                            NodeApSNR = snrCell{1, i};
                        end
                        
                        
                        
                        if CTLFREQU==1 || mod(i, CTLFREQU)==1
                            
                            
                            [sol, finalV,  exitcode, policyV,...
                                outS, intV, intx, xa,  bfx, bfV, startArray, startxV,...
                                policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
                                atomx, atomV, atomT, atomJ] =mainljwRateNs3RandomSNR();
                            
                            xArray{i, 1} = startxV;
                            xArray{i, 2} = xa;
                            xArray{i, 3} = bfx;
                            xArray{i, 4} = intx;
                            xArray{i, 5} = sol;
                            xArray{i, 6} = atomx;
                            
                            xA{1, 1} = startxV;
                            xA{1, 2} = xa;
                            xA{1, 3} = bfx;
                            xA{1, 4} = intx;
                            xA{1, 5} = sol;
                            xA{1, 6} = atomx;
                            
                            
                        else
                            [startArray, policyV, policyT, policyJ, ...
                                bfV, bfT, bfJ, ...
                                intV, intT, intJ, ...
                                finalV, solT, solJ,...
                                atomV, atomT, atomJ] = updatedResultsFromOldAssociation(xA);
                        end
                        
                        %snrArray{i, 1} = NodeApSNR;
                        
                        
                        startva = mean(startArray, 1);
                        startstd = std(startArray, 1);
                        
                        fprintf(fileID, '%4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f\n',...
                            startva(1), startva(2), startva(3), ...
                            startstd(1), startstd(2), startstd(3), ...
                            policyV, policyT, policyJ, ...
                            bfV, bfT, bfJ, ...
                            intV, intT, intJ, ...
                            finalV, solT, solJ,...
                            atomV, atomT, atomJ);
                        
                        
                    end
                    
                    isFirstTime  = 0;
                    fclose(fileID);
                    save(strcat(fileName, '.mat'));
                    toc
                    
                end
            end
        end
    end
end