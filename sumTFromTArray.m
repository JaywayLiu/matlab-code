function [sumT, jain] = sumTFromTArray(tarray)
global N;
global isOnOff;

global onOffMap;
%global onOffMap;
nOff = 0;
if isOnOff
    nOff = length(find(onOffMap(:, 1)==0));
end
sumT = sum(tarray);
%rateV

%use negative value to diff the reason of nan

% if (sum(onOffMap) ==0)
%     jain = -2;
% elseif (sumT ==0)
%     jain = -1;

if (sumT ==0)
    jain = 0;
else
    rateVSq = tarray .^2;
    %N - nOff should not be 0, make sure when passing in, or need to add code here
    jain = (sum(tarray) ^2) / ((N - nOff)*  sum(rateVSq));
end