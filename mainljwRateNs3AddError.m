
function [sol, finalV,  exitcode, policyV,...
    outS, intV, intx, xa, D, bfx, bfV, startArray, ...
    policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
    atomx, atomV, atomT, atomJ, xArrayR] =mainljwRateNs3AddError(errorRateInit, xArray, disToBsInit, startx)
global N;
global M;
global disToBs;

global errorRate;

%this should stay in the main level, not the more level
global isAddError;

TRY_DIFF_START_POINT = 5;
isAddError = 1;


disToBs = disToBsInit;

errorRate = errorRateInit;



%rng(seed)

assert(N>M);

generateErrorDirection();



[bfV, bfx] = bfMore();
rate_bf_raw = calcRates(bfx);
rateV_bf = sum(rate_bf_raw, 2);
[bfT, bfJ] = sumTNorm(rateV_bf, rateV_bf, bfx);
%N >M assert

startArray = zeros(TRY_DIFF_START_POINT, 3);
for ii = 1:length(startx) 
    startArray(ii, 1) = myfunInsideS(startx{1,ii});
    rate_random_raw = calcRates(startx{1,ii});
    rate_random = sum(rate_random_raw, 2);
    [ranT, ranJ] = sumTNorm(rateV_bf, rate_random, startx{1,ii});
    startArray(ii, 2) = ranT;
    startArray(ii, 3) = ranJ;
end;


%    r used in myfunInside, that's why we make it global
%global r
%r = rand(N, M);
%randM = r;

lb = zeros(N, M);
ub = ones(N, M);

Aeq_m = eye(N, N);
Aeq = Aeq_m;

for i=1:M-1
    Aeq = [Aeq, Aeq_m];
end

%     A_m = eye(M, M);
%     A = A_m;
%
%     for i=1:N-1
%         A = [A, A_m];
%     end
%     A = -A;
%
%     disp(A)


A = zeros(M, M*N);

for i= 1:M
    A_m = zeros(M, N);
    A_m(i, :) = ones(1, N);
    A(:, N*(i-1)+1:N*(i)) = A_m;
end
A = -A;

%disp(A)

b =-ones(M,1);


beq=ones(N, 1);

%beq = beq* M*0.8;
%disp(beq);


%distGenRandom();
initWThrough();

xArrayR = zeros(length(xArray), 3);
for i = 1:length(xArray)
    xArrayR(i, 1) = myfunInsideS(xArray{i});
    rateTmp = calcRates(xArray{i});
    rateTmpS = sum(rateTmp, 2);
    [t, j] = sumTNorm(rateV_bf, rateTmpS, xArray{i});
    xArrayR(i, 2) = t;
     xArrayR(i, 3) = j;
end

%try to use multiple staring point
values = zeros(1, TRY_DIFF_START_POINT);
solutions = cell(1, TRY_DIFF_START_POINT);
outputs = cell(1, TRY_DIFF_START_POINT);
exitfs = zeros(1, TRY_DIFF_START_POINT);
%diffs = zeros(1, TRY_DIFF_START_POINT);
xa = policyGen();

[atomV, atomx] = atom_ljw();

%startx = cell(1, TRY_DIFF_START_POINT);

for k=1:TRY_DIFF_START_POINT
    x0 = startx{1, k};

   
    
    %set the iterations we want to run
    opts = optimoptions(@fmincon, 'MaxFunctionEvaluations', 5000*N);
    %opts = optimoptions(@fmincon,'Algorithm','sqp', 'MaxFunctionEvaluations', 5000);
    
    [x,fval,exitflag,output] = fmincon(@myfunInsideS,x0,A,b,Aeq,beq,lb,ub,null_nonlin, opts);
    
    values(1, k) = fval;
    solutions{1, k} = x;
   % diff =  startV - fval;
    exitfs(1, k) = exitflag;
    outputs{1, k} = output;
   % diffs(1, k) = diff;
    
end


[finalV, b] = max(values);
sol = solutions{1, b};
exitcode = exitfs(1, b);
outS = outputs{1, b};

[solT, solJ] = sumT(sol);

intx = round(sol);

%/////////////////////////////////

policyV = myfunInsideS(xa);
rate_policy_raw = calcRates(xa);
rateV_policy = sum(rate_policy_raw, 2);
[policyT, policyJ] = sumTNorm(rateV_bf, rateV_policy, xa);

rateV = calcRates(sol);
rateVSum = sum(rateV, 2);
[solT, solJ] = sumTNorm(rateV_bf, rateVSum, sol);


intx = round(sol);
intV = myfunInsideS(intx);
rate_int_raw = calcRates(intx);
rateV_int = sum(rate_int_raw, 2);
[intT, intJ] = sumTNorm(rateV_bf, rateV_int, intx);


 
%for ii = 1:length(startx)
%    startArray(k, 1) = myfunInsideS(x0);
%    rate_random_raw = calcRates(startx{ii});
%    rate_random = sum(rate_random_raw, 2);
%    [ranT, ranJ] = sumTNorm(rateV_bf, rate_random, startx{ii});
%    startArray(ii, 2) = ranT;
%    startArray(ii, 3) = ranJ;
%end;



rate_atom_raw = calcRates(atomx);
rateV_atom = sum(rate_atom_raw, 2);
[atomT, atomJ] = sumTNorm(rateV_bf, rateV_atom, atomx);


D = disToBs;

end

function [c,ceq] = null_nonlin(x)
c = [];
ceq = [];
end

