function index = searchCloestLower(array, value)
possibles = array <= value;
%will always have some value smaller or euqal to than value
[posmax, posind] = max(array(possibles));
%disp(posmax);
inddatapos = find(possibles); % possible indices
inddata = inddatapos(posind); % find the index we care about

if isempty(inddata)
    index = 0;
else
    index = inddata;
end

end