#!/bin/python
import os,sys

jobID=int(sys.argv[1])
cores = 1 
maxStep = 300;

print "jobID = " +str(jobID)

#MY_MATLAB_FOLDER="/scratch2/jianwel/matlab-code"
MY_MATLAB_FOLDER="/home/jianwel/matlab-code"
LOG_FOLDER="/scratch2/jianwel/logs"
isLog=0

#if not os.path.exists(MY_MATLAB_FOLDER):
#	os.system("cp -ra /home/jianwel/matlab-code /scratch2/jianwel/")

os.chdir(MY_MATLAB_FOLDER);
#wifiC=[0.8, 0.6]
partialRate=[0, 0.25, 0.5, 0.75, 1]

#rateIndex = jobID 
for rate in partialRate:
#rate = partialRate[jobID-1]

    if isLog:	
        print("taskset -c %d matlab -nodisplay -nosplash -r 'mainPrintAndCallerFixedMCS(%d, %f,  1), exit' > %s/test_%d_%f.log"%(cores, jobID, rate, LOG_FOLDER, jobID, rate))
        os.system("taskset -c %d matlab -nodisplay -nosplash -r 'mainPrintAndCallerFixedMCS(%d, %f,  1), exit' > %s/test_%d_%f.log"%(cores, jobID, rate, LOG_FOLDER, jobID, rate))
    else:
        print("taskset -c %d matlab -nodisplay -nosplash -r 'mainPrintAndCallerFixedMCS(%d, %f,  1), exit'"%(cores, jobID, rate))
        os.system("taskset -c %d matlab -nodisplay -nosplash -r 'mainPrintAndCallerFixedMCS(%d, %f,  1), exit'"%(cores, jobID, rate))

