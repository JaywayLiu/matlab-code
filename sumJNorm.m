function [jain] = sumJNorm(rateBase, rateVSum)
global N;

global isOnOff;

global onOffMap;
%global onOffMap;
nOff = 0;
if isOnOff
    nOff = length(find(onOffMap(:, 1)==0));
end

rateVNorm = rateVSum ./ rateBase;
%rateVNorm
rateVSq = rateVNorm .^ 2;
sumRateSq = sum(rateVSq);

if sumRateSq ~=0
    jain = (sum(rateVNorm) ^2) / (N * sumRateSq );
else
    jain = 1;
end

end
