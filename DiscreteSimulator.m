classdef DiscreteSimulator < handle

    properties
        curTime;
        pq;
        N_SCHED_MAX;
        sys;
        %listLen;
        %listIndex;
        EXP_MEAN;
        dataFolder;
        outputBase;
        outputFolder;
        eList;
        isStop;
        N;
        M;
    end

    methods

        function obj = DiscreteSimulator(inputFolderName, outputFolderBase, sched_max)
            global N;
            global M;
            global nMethods;

            global isMoving;
            global movingP;

            %obj.N_SCHED_MAX = 16384;
            obj.N_SCHED_MAX = sched_max;
            obj.EXP_MEAN = 1;

            obj.dataFolder = inputFolderName;
            obj.outputBase = strcat(outputFolderBase, 'mcsDataOutput');
            checkDir(obj.outputBase);
            if isMoving
                obj.outputFolder = strcat(obj.outputBase, '/',...
                 obj.dataFolder, '-atom', '-', sprintf('%f', movingP), '/');
            else
                obj.outputFolder = strcat(obj.outputBase, '/',...
                 obj.dataFolder, '-atom', '-', sprintf('%f', 0), '/');

            end
            checkDir(obj.outputFolder);

            re = loadSceneConfig(inputFolderName);
            N = re(3, 1);
            M = re(2, 1) + 1;

            obj.N = N;
            obj.M = M;
            %obj.nOn = 0;

        end

        function printSysToFile(obj, nRun)
            outName = sprintf('%d-%d-%07d', obj.sys.nUE, obj.sys.nAP, nRun);
            outName = strcat(outName, '_accu.txt');
            average = obj.sys.accu_metrics ./ obj.curTime;
            csvwrite(strcat(obj.outputFolder, outName), average);
        end

        function init(obj, nRun)
            %TODO: every run do not need to init the AP location, but can ignore for
            %now
            global onOffMap;
            global nMethods;
            global isEventFromFile;
            obj.curTime = 0;
            obj.isStop = 0;
            obj.pq = PriorityQueue();

            % new the system parameters for every run
            obj.sys = SystemParameters(obj.N, obj.M);
            obj.sys.nSched = 0;

            %need to init the nUE first from the input folder
            %obj.eList{obj.listLen} = Event('pre', 0, obj.pq, 0);
            obj.eList = cell(1, obj.N);
            onOffMap = ones(obj.N, 1);

            initAPLocations(obj.dataFolder);
            initUELocations(obj.dataFolder, nRun);

            if (isEventFromFile)
                obj.initEventsFromArray();
            else
                obj.initEvents();

            end

            doOneSchedule(obj.sys, 1);
            init_ev(obj);

            % obj.sys.cur_metrics(:, nMethods) = obj.sys.cur_metrics(:, 3);
            % obj.sys.cur_rates(:, nMethods) = obj.sys.cur_rates(:, 3);
            % obj.sys.cur_solutions(:, :, nMethods) = obj.sys.cur_solutions(:, :, 3);

        end

        function initEvents(obj)
            %for N UES, do rand, if >0.5 on if <=0.5 off
            global onOffMap;

            for i = 1:obj.sys.nUE
                randV = rand();

                if randV > 0.5
                    onTime = exprnd(obj.EXP_MEAN);
                    onOffData = OnOffEventData();
                    onOffData.flowID = i;
                    onOffData.onOffType = 'off';
                    newEvent = Event('onoff', onTime, obj, onOffData);
                elseif randV <= 0.5
                    offTime = exprnd(obj.EXP_MEAN);
                    onOffData = OnOffEventData();
                    onOffData.flowID = i;
                    onOffData.onOffType = 'on';
                    newEvent = Event('onoff', offTime, obj, onOffData);
                    onOffMap(i, 1) = 0;
                end

                obj.addEvent(newEvent, i);
            end

        end

        function initEventsFromArray(obj)
            %for N UES, do rand, if >0.5 on if <=0.5 off
            global onOffMap;
            %onArray = [1, 2, 4, 5];
            onOffMap = [1; 1; 0; 1; 1; 0];

        end

        function addEvent(obj, event, indexToUse)
            obj.eList{indexToUse} = event;
            obj.pq.push(indexToUse, event.time);
            %obj.listIndex = obj.listIndex + 1;
            %        if obj.listIndex > obj.listLen
            %            obj.listIndex = 1;
            %        end
        end

        function event = popEvent(obj)
            index = obj.pq.top();
            obj.pq.pop();
            event = obj.eList{index};
            %obj.sys.popIndex = index;
        end

        function clean(obj)
            obj.pq.delete();
        end

        function run(obj, nRun)

            global isEventFromFile;
            global onOffMap;
            global nMethods;
            global allMethods;
            global isOutputEventDetails;

            if isOutputEventDetails

                logFiles = cell(1, nMethods);
                runS = sprintf('%04d', nRun);
                outputFolder = strcat(obj.outputFolder, '/', runS);
                checkDir(outputFolder);

                for ii = 1:nMethods
                    strcat(outputFolder, '/', 'cur_', allMethods{1, ii}, '_', runS, '.log')
                    logFiles{1, ii} = fopen(strcat(outputFolder, '/', 'cur_', allMethods{1, ii}, '_', runS, '.log'), 'w');
                    logFiles{2, ii} = fopen(strcat(outputFolder, '/', 'accu_', allMethods{1, ii}, '_', runS, '.log'), 'w');
                end

            end

            if isEventFromFile
                eventFile = fopen('event.txt', 'r');
            end

            %should init the locaiton here
            obj.init(nRun);
            sizeq = obj.pq.size();

            if isOutputEventDetails

                for ii = 1:nMethods
                    fprintf(logFiles{1, ii}, '%4f %4f %4f\n', obj.sys.cur_metrics(1, ii), obj.sys.cur_metrics(2, ii), obj.sys.cur_metrics(3, ii));
                    fprintf(logFiles{2, ii}, '%4f %4f %4f %4f %4f %4f %4f\n', obj.sys.accu_metrics(1, ii), obj.sys.accu_metrics(2, ii), obj.sys.accu_metrics(3, ii), obj.sys.accu_metrics(1, ii) / obj.curTime, obj.sys.accu_metrics(2, ii) / obj.curTime, obj.sys.accu_metrics(3, ii) / obj.curTime, obj.curTime);
                end

            end

            if (isEventFromFile)
                [eventPara, count] = fscanf(eventFile, "%f  %d    %d\n", [3, Inf]);
                %assert(count ==0 || count ==3);
                sizea = size(eventPara)

                for i = 1:sizea(2) - 1
                    timeNext = eventPara(1, i);
                    onOffData = OnOffEventData();
                    onOffData.flowID = eventPara(3, i) + 1;

                    if onOffMap(onOffData.flowID, 1) == 1
                        onOffData.onOffType = 'off';
                    else
                        onOffData.onOffType = 'on';
                    end

                    event = Event('onoff', timeNext, obj, onOffData);

                    event.handle_event(obj.sys);

                    if isOutputEventDetails

                        for ii = 1:nMethods
                            fprintf(logFiles{1, ii}, '%4f %4f %4f\n', obj.sys.cur_metrics(1, ii), obj.sys.cur_metrics(2, ii), obj.sys.cur_metrics(3, ii));
                            fprintf(logFiles{2, ii}, '%4f %4f %4f %4f %4f %4f %4f\n', obj.sys.accu_metrics(1, ii), obj.sys.accu_metrics(2, ii), obj.sys.accu_metrics(3, ii), obj.sys.accu_metrics(1, ii) / obj.curTime, obj.sys.accu_metrics(2, ii) / obj.curTime, obj.sys.accu_metrics(3, ii) / obj.curTime, obj.curTime);
                        end

                    end

                end

            else

                while (~obj.isStop && sizeq)
                    sizeq = obj.pq.size();
                    event = obj.popEvent();
                    disp('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
                    disp(obj.sys.nSched);

                    %handle the event
                    event.handle_event(obj.sys);

                    if isOutputEventDetails

                        for ii = 1:nMethods
                            fprintf(logFiles{1, ii}, '%4f %4f %4f\n', obj.sys.cur_metrics(1, ii), obj.sys.cur_metrics(2, ii), obj.sys.cur_metrics(3, ii));
                            fprintf(logFiles{2, ii}, '%4f %4f %4f %4f %4f %4f %4f\n', obj.sys.accu_metrics(1, ii), obj.sys.accu_metrics(2, ii), obj.sys.accu_metrics(3, ii), obj.sys.accu_metrics(1, ii) / obj.curTime, obj.sys.accu_metrics(2, ii) / obj.curTime, obj.sys.accu_metrics(3, ii) / obj.curTime, obj.curTime);
                        end

                    end

                end

            end

            obj.clean();

            if isOutputEventDetails

                for ii = 1:nMethods
                    fclose(logFiles{1, ii});
                    fclose(logFiles{2, ii});
                end

            end

        end

        %     function delete(obj)
        %             end
    end

end
