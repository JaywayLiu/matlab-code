function [pos, clusterCenters] = genCircClusters(nCluster, nUE, globalP, nSteps, clusterPos,  rangeRadius, angleIn, baseClusterCenter)

nCluster = 3;
%globalP = 0;
%nSteps = 3;
%clusterPos = 2/3;
%rangeRadius = 0.25;
%angleIn = 60;

randType = 'uniform';

outputName = strcat("multi_cluster", "circ", int2str(angleIn))
%checkDir(outputName)


%nCluster does not count the global one
p = (1 - globalP) / nCluster;
pV = ones(1, nCluster + 1) .* p;
pV(1, 1) = globalP;

sump = 0;

for i = 1:length(pV)
    sump = sump + pV(i);
    pV(i) = sump;
end

nUEArray = simuPlacement(pV, nUE);
%nUEArray = simuPlacement(pV, nUE)
% if globalP == 0
%     nUEArray = [0, 10, 11, 11]
% else
%     nUEArray = [3, 9, 10, 10]
% end

clusterCenters = getClusterCenters(clusterPos, angleIn)

nClusters = length(nUEArray)

pos = generateRandomPlacementInCircleV1(nSteps, nUEArray(nClusters), clusterCenters(nClusters-1, :), rangeRadius, randType);

for i = nClusters-1: -1: 1
    if i ~=1
        cCenter = clusterCenters(i-1, :);
    else
        cCenter = baseClusterCenter;
    end
    subpos = generateRandomPlacementInCircleV1(nSteps, nUEArray(i), cCenter,  rangeRadius, randType);
    pos = cat(2, pos, subpos);

end

%end
