import os
import time
import glob
import sys

outputFolder = sys.argv[1]
print("output="+outputFolder)


outputName = "all.txt"


#outputFile.write('start.va start.ta start.TFIa start.vstd start.tstd start.jstd '+
#'lgw.v lgw.t lgw.j '+
#'opt.v opt.t opt.j '+
#'int.v int.t int.j '+
#'frac.v frac.t frac.j '+
#'atom.v atom.t atom.j'+'\n');

header = ('frameN start.va start.ta start.ja start.vstd start.tstd start.jstd '+
'lgw.v lgw.t lgw.TFI '+
'opt.v opt.t opt.TFI '+
'int.v int.t int.TFI '+
'frac.v frac.t frac.TFI '+
'atom.v atom.t atom.TFI '+
'gg.v gg.t gg.TFI ' +
      'random.v random.t random.TFI '+
      'lge.v lge.t lge.TFI '+
      'rand.v rand.t rand.TFI '+
      'lgw.nj opt.nj int.nj sol.nj atom.nj gg.nj lge.nj rand.nj '+
      'lgw.tFI opt.tFI int.tFI sol.tFI atom.tFI gg.tFI lge.tFI rand.tFI '+
      'lgw.mm opt.mm int.mm sol.mm atom.mm gg.mm lge.mm rand.mm'+
'\n')


l1 = ["opt=0"]
l2 = ["1.0000"]

#for ii in l1:
#  for jj in l2:
#fullFolder = outputFolder + ii + '/' + jj + "/"
fullFolder = outputFolder 
outputFile = open(fullFolder+"/"+outputName,"w")
outputFile.write(header)
outputFile.close()
allFiles = glob.glob(fullFolder+"/"+"*-both.txt")

print("concat output...")
for fileName in sorted(allFiles):
  os.system('cat %s >> %s'%(fileName, fullFolder+"/"+outputName))
#  print('cat %s >> %s'%(fileName, fullFolder+outputName))

#  time.sleep(0.005);

