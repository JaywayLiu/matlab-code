function mcsIndexFromFiles(dataFolder, timeStamp)
%read the csv
global lteMCSIndex;
global wifiMCSIndex;

lteMCSIndex = csvread(strcat(dataFolder, sprintf('/lte-%04d.txt', timeStamp)));
wifiMCSIndex = csvread(strcat(dataFolder, sprintf('/wifi-%04d.txt', timeStamp)));


end