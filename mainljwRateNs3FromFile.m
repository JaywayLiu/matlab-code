function [ exitcode, outS, D,...
   policyx, policyV, policyT, policyJ, policyNJ, policyMM, policyTFI, ...
     intx, intV, intT, intJ, intNJ, intMM, intTFI, ...
     bfx, bfV, bfT, bfJ, bfNJ, bfMM, bfTFI, ...
     startArray, startxV,...
    solx, solV, solT, solJ, solNJ, solMM, solTFI, ...
    atomx, atomV, atomT, atomJ, atomNJ, atomMM, atomTFI, ...
    ggx, ggV, ggT, ggJ, ggNJ, ggMM, ggTFI, ...
    grx, grV, grT, grJ, grNJ, grMM, grTFI, ...
    pex, peV, peT, peJ, peNJ, peMM, peTFI, ...
     ratediffpex = pex & onOffM;
     grx = grx & onOffM;_atom,...
    rateVs,...
    MaxRate...
    ]=mainljwRateNs3FromFile(isAddErr0, dataFolder, timeStampPassIn)
%res_atom_raw, res_int_raw, res_bf_raw, res_policy_raw
   % ratediff_bf, ratediff_int, ratediff_atom, ratediff_sol,...
   %rate diff is without doing the percentage
   
   %startArray contains cells for V T J
global N;
global M;
global disToBs;
global isDist;
global isBF;
global isMexBf;
global isAtomOnly;

global isOpt;

% a cell array; 0 is for lte and 1 for wifi aps
% 1 is an array
global nonParInfo;

%can move this to the called later
global isOnOff;
global isOnOffFromFile;

global timeStamp;
timeStamp = timeStampPassIn;
global onOffMap;
global rateSource;
global realN;
global deployRatio;
global MaxRate;
global isWiFiPre;
global phaseId;

phaseId = 1;

global iSMcsIndexFromFile;
global isMcsIndexFromDistFiles;
global isFixMaxRate;

%%%configurations !!
isOnOff =0;

%rateSource = RandRateMethod.Rand_SINR;
rateSource = RandRateMethod.Rand_MCS;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if nargin<1
	isAddErr = 0;
else 
	isAddErr = isAddErr0;
end

%rng(seed)


%    r used in myfunInside, tha is why we make it global
%global r
%r = rand(N, M);
%randM = r;
TRY_DIFF_START_POINT = 5;

startArray = zeros(TRY_DIFF_START_POINT, 4);
startxV = cell(1, TRY_DIFF_START_POINT);



%distGenRandom();


    

    if ~isFixMaxRate
        %TODO: should use an enum for these
        if iSMcsIndexFromFile
            mcsIndexFromFiles(dataFolder, timeStampPassIn);
            initMCSTables();
        elseif isMcsIndexFromDistFiles
            initWThroughNew();
            mcsIndexFromDistFiles(dataFolder, timeStampPassIn);         
        else
            setMCSIndex();
            initMCSTables();
        end

           MaxRate = calcMaxRateTable();
    else
        fixMaxRate();
    end



assert(N>M);
%N >M assert
%read the on-off traffic map
if isOnOff
    if isOnOffFromFile
        initOnOffMap(dataFolder);
    end
end



%%%%%%%%%%%%%%%%%Need to change the N from here for deplyment ratio
%Need a realN for the throughput estimation part







if isDist
    policyx = policyGen();
else
    policyx = policyGenMaxRate();
end
  
pex = policyGenMCS_equal(MaxRate);

grx = gRandomAvail();


%For all the methods does not depends myFuncInside, need to fix them here for on
%-off
if isOnOff
onOffM = repmat(onOffMap(: , timeStampPassIn), 1, M);
policyx = policyx & onOffM;
pex = pex & onOffM;
grx = grx & onOffM;
end


if deployRatio < 1
    realN = N;
    %use ceil here to make it at least one UE
    N = ceil(N * deployRatio);
end;


if (deployRatio <1) && (isOpt ==1)
    disp('in opt');
    %nonParInfo(0) = sum(MaxRate(N+1, realN, :) >0, 2); 
    nonParInfo{1} = realN -N; 
    %only divide and sum those that are non-zero
    [row, col]= find(MaxRate(N+1: realN, :) >0);
    %uc = unique(col);
    nonParInfo{2} = zeros(1, M);
    for kk = 2:M     
        rates = MaxRate(row(col==kk)+N,kk);
        if(~isempty(rates))
           nonParInfo{2}(1, kk)= sum(1./rates);
        end        
    end
   
end
%disp(xa);
%assert(sum(xa, 2) <= ones(N, 1));

[atomV, atomx] = atom_ljw();


if isBF
      if isMexBf
	[bfx, bfV] = bfMex(MaxRate);
	bfx = double(bfx);
    bfV = -bfV;
	%disp(bfx);
     else
	[bfV, bfx] = bfMore();
	end
else 
      bfV = atomV;
      bfx = atomx; 
end

[ggV, ggx, rate_gg_raw] = global_greedy(MaxRate);


if ~isAtomOnly
    


lb = zeros(N, M);
ub = ones(N, M);

Aeq_m = eye(N, N);
Aeq = Aeq_m;

for i=1:M-1
    Aeq = [Aeq, Aeq_m];
end



A = zeros(M, M*N);

for i= 1:M
    A_m = zeros(M, N);
    A_m(i, :) = ones(1, N);
    A(:, N*(i-1)+1:N*(i)) = A_m;
end
A = -A;

%disp(A)

b =-ones(M,1);


beq=ones(N, 1);

%beq = beq* M*0.8;
%disp(beq);

%try to use multiple staring point

values = zeros(1, TRY_DIFF_START_POINT);
solutions = cell(1, TRY_DIFF_START_POINT);
outputs = cell(1, TRY_DIFF_START_POINT);
exitfs = zeros(1, TRY_DIFF_START_POINT);
diffs = zeros(1, TRY_DIFF_START_POINT);


maxRandomV = -1e10;
maxRandomI = 0;
randomx = zeros(N,M);

for k=1:TRY_DIFF_START_POINT
    x0 = zeros(N,M);
    taken = zeros(1, M);
    
    %this gurantee at least one user is connected to every ap
    for i=1:M
        ii = randi(N);
        x0(ii, i) = 1;
        taken(i) = ii;
    end
    
    for i=1: N
        %j = mod(i, M) +1;
        %randomly select an ap to associate to
        j = randi(M);
        
        %if it is not the assigned node in the last init step
        lia = ismember(i, taken);
        if lia==0
            x0(i, j) = 1;
        end
    end
    
    startxV{1, k} = x0;
     
   startV = myfunInsideS(x0);
    startArray(k, 1) = startV;
    if startV > maxRandomV
       maxRandomV = startV;
      % randomx = x0;
      % maxRandomI = k;
   end 
 
        
    
    %set the iterations we want to run
    opts = optimoptions(@fmincon, 'MaxFunctionEvaluations', 5000*N);
    %opts = optimoptions(@fmincon,'Algorithm','sqp', 'MaxFunctionEvaluations', 5000);
    
    [x,fval,exitflag,output] = fmincon(@myfunInsideS,x0,A,b,Aeq,beq,lb,ub,null_nonlin, opts);
    
    values(1, k) = fval;
    solutions{1, k} = x;
    diff =  startV - fval;
    exitfs(1, k) = exitflag;
    outputs{1, k} = output;
    diffs(1, k) = diff;
    
end


[solV, b] = max(values);
solx = solutions{1, b};
exitcode = exitfs(1, b);
outS = outputs{1, b};


intx = round(solx);

else
    solx = policyx;
    solV = 0;
    exitcode = 0;
    outS = 'fake outS';
    
    disp('isBF = ');
    disp(isBF);
    if ~isBF
        bfx = policyx;
        bfV = 1;
    end
    
    intx = policyx;
    intV = 1;

end
%/////////////////////////////
%add this if because when running the adding error senario, we only need
%thesolution and then use the real/correct throughts for the
%utility/performance metrics calculations
if ~isAddErr
    
   %first connect the solution with xa. 
   %tempX = xa;
   if deployRatio < 1
     if isWiFiPre ==0
         atomx = conSolution(pex, atomx, N, realN); 
         ggx = conSolution(pex, ggx, N, realN);
         %grx = conSolution(pex, grx, N, realN); 
    
     else
        atomx = conSolution(policyx, atomx, N, realN); 
        ggx = conSolution(policyx, ggx, N, realN);
        %grx = conSolution(policyx, grx, N, realN); 
     end
    bfx= ggx;
    %disp(size(bfx));
    N = realN;
   end;
   %and then change the N back for utility calculation

   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %from here we enter phase 2, which will be the calculation of metrics
   phaseId = 2;
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
 [res_bf_raw, rate_bf_raw] = calcRates(bfx);
rateV_bf = sum(rate_bf_raw, 2);
%[bfT, bfJ] = sumTNorm(rateV_bf, rateV_bf, bfx);
%[bfT, bfJ] = sumT(bfx);
[bfT, bfJ] = sumTFromTArray(rateV_bf);
bfNJ = sumJNorm(rateV_bf, rateV_bf);

rateV_bf_mean = mean(rateV_bf);
bfTFI = sumJNorm(rateV_bf_mean, rateV_bf);


%update the rate_gg_raw if deployRation <1
if (deployRatio < 1)
    [res_gg_raw, rate_gg_raw] = calcRates(ggx);
     ggV = myfunInsideS(ggx);
end
    
rateV_gg = sum(rate_gg_raw, 2);
[ggT, ggJ] = sumTFromTArray(rateV_gg);
ggNJ = sumJNorm(rateV_bf, rateV_gg);

rateV_gg_mean = mean(rateV_gg);
ggTFI = sumJNorm(rateV_gg_mean, rateV_gg);

 policyV = myfunInsideS(policyx);
[res_policy_raw, rate_policy_raw] = calcRates(policyx);
rateV_policy = sum(rate_policy_raw, 2);
[policyT, policyJ] = sumTFromTArray(rateV_policy);
policyNJ = sumJNorm(rateV_bf, rateV_policy);

rateV_policy_mean = mean(rateV_policy);
policyTFI = sumJNorm(rateV_policy_mean, rateV_policy);

 peV = myfunInsideS(pex);
[res_pe_raw, rate_pe_raw] = calcRates(pex);
rateV_pe = sum(rate_pe_raw, 2);

[peT, peJ] = sumTFromTArray(rateV_pe);
peNJ = sumJNorm(rateV_bf, rateV_pe);

rateV_pe_mean = mean(rateV_pe);
peTFI = sumJNorm(rateV_pe_mean, rateV_pe);

 grV = myfunInsideS(grx);
[res_gr_raw, rate_gr_raw] = calcRates(grx);
rateV_gr = sum(rate_gr_raw, 2);
[grT, grJ] = sumTFromTArray(rateV_gr);
grNJ = sumJNorm(rateV_bf, rateV_gr);

rateV_gr_mean = mean(rateV_gr);
grTFI = sumJNorm(rateV_gr_mean, rateV_gr);

if (deployRatio <1)
    atomV = myfunInsideS(atomx);
end
[res_atom_raw, rate_atom_raw] = calcRates(atomx);
rateV_atom = sum(rate_atom_raw, 2);
%[atomT, atomJ] = sumT(atomx);
[atomT, atomJ] = sumTFromTArray(rateV_atom);
atomNJ = sumJNorm(rateV_bf, rateV_atom);

rateV_atom_mean = mean(rateV_atom);
atomTFI = sumJNorm(rateV_atom_mean, rateV_atom);

if ~isAtomOnly
    
[res_sol_raw, rate_sol_raw] = calcRates(solx);
rateV_sol = sum(rate_sol_raw, 2);
[solT, solJ] = sumTFromTArray(rateV_sol);
solNJ = sumJNorm(rateV_bf, rateV_sol);

rateV_sol_mean = mean(rateV_sol);
solTFI = sumJNorm(rateV_sol_mean, rateV_sol);
%[solJNorm] = sumTNorm(rateV_bf, rateV_sol);


intV = myfunInsideS(intx);
[res_int_raw, rate_int_raw] = calcRates(intx);
rateV_int = sum(rate_int_raw, 2);
[intT, intJ] = sumTFromTArray(rateV_int);
intNJ = sumJNorm(rateV_bf, rateV_int);

rateV_int_mean = mean(rateV_int);
intTFI = sumJNorm(rateV_int_mean, rateV_int);

for ii = 1:length(startxV) 
    [res_random_raw, rate_random_raw] = calcRates(startxV{ii});
    rateV_random = sum(rate_random_raw, 2);
    [ranT, ranJ] = sumTFromTArray(rateV_random);
    ranNJ = sumJNorm(rateV_bf, rateV_random);
    %[ranT, ranJ] = sumTNorm(rateV_bf, rate_random, startxV{ii});
    startArray(ii, 2) = ranT;
    startArray(ii, 3) = ranJ;
    startArray(ii, 4) = ranNJ;
end;
else
    [intV , intJ ,intT, intNJ, intTFI] = deal(-1); 
[solT , solJ, solTFI, solNJ, intTFI]= deal(-1);
if ~isBF
    [bfT , bfJ , bfV, bfNJ ] = deal(-1);
end

end
   
    rateVs = zeros(N, 6);

    rateVs(:,1) =rateV_policy;
    rateVs(:,2) =rateV_bf ;
    rateVs(:,3) = rateV_atom;
    rateVs(:,4) = rateV_gg;
    rateVs(:,5) =rateV_pe;
    rateVs(:,6) =rateV_gr;
    
    policyMM = min(rateV_policy) / max(rateV_policy);
    bfMM = min(rateV_bf) / max(rateV_bf);
    atomMM = min(rateV_atom) / max(rateV_atom);
    ggMM = min(rateV_gg) / max(rateV_gg);
    peMM = min(rateV_pe) / max(rateV_pe);
    grMM = min(rateV_gr) / max(rateV_gr);
    
    intMM = policyMM;
    solMM = policyMM;
    
    
%     rateVs(:,2) =rateV_bf ;
%     rateVs(:,3) = rateV_int;
%     rateVs(:,5) = rateV_sol;
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ratediff_bf = ((rateV_bf - rateV_policy) ./ rateV_policy)';
ratediff_atom = ((rateV_atom - rateV_policy) ./ rateV_policy)';
% ratediff_int = ((rateV_int- rateV_policy) ./ rateV_policy)';
% ratediff_sol = ((rateV_sol - rateV_policy) ./ rateV_policy)';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
[intV , intJ ,intT , intNJ, atomJ , atomT , atomV, atomNJ ] = deal(-1); 
[solT , solJ, solNJ ]= deal(-1);
[ policyT , policyV , policyJ, policyNJ ] = deal(-1);
[bfT , bfJ , bfV, bfNJ ] = deal(-1);


end

D = disToBs;


end

function [c,ceq] = null_nonlin(x)
c = [];
ceq = [];
end




%write another function to make rateGen follow certain distribution














