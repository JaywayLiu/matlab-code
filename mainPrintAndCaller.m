function mainPrintAndCaller(timeStamp, deployRatioIn, isOptIn)
tic
isAddErr = 0;

%dataFolder = 'mcs_20181113T111754';
dataFolder = 'mcs_12-4-1000_20190201T141635';
%outputFolder='/scratch2/jianwel/mcsDataOutput/';
%outputFolder='mcsDataOutput/westall';
outputFolder='mcsDataOutput/westall';
outputFolder = strcat(outputFolder, sprintf('%.4f', deployRatioIn), '/');

global N;
global M;
global isBoth;
global isAtomOnly;
global isBF;
global deployRatio;
global isOpt;

isOpt = isOptIn;

deployRatio = deployRatioIn;
disp(timeStamp);

if (ischar(timeStamp))
  timeStamp = str2num(timeStamp);
end

N = 12;
M = 4;
nn = N;
mm = M;

isBF =0;

isBoth =1;
isAtomOnly =1;
nCompare = 6;

%%%%%%%%%%%%%%%files
fileName = sprintf('%d-%d-test%d', nn, mm, timeStamp);
fileNametime = strcat(fileName, '-time');
if isBoth
    fileName = strcat(fileName,'-both');
    fileNametime = strcat(fileNametime,'-both');
else
    fileName = strcat(fileName,'-pfonly');
    fileNametime = strcat(fileNametime,'-pfonly');
end

fileNameBase = fileName;

fileName = strcat(fileName,'.txt');
fileNametime = strcat(fileNametime, '.txt');

fileID = fopen(strcat(outputFolder,fileName), 'w');
fileTime = fopen(strcat(outputFolder, fileNametime), 'w');

rateDiffFileName = strcat(fileNameBase, '-diff.txt');
fileDiff = fopen(strcat(outputFolder, rateDiffFileName), 'w');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xArray = cell(1, nCompare);

%ratediffMethods = {'bf', 'int', 'atom', 'sol'};
%ratesMethods = {'policy', 'bf', 'int', 'atom', 'sol'};
ratesMethods = {'policy', 'atom'};
%ratediffM = zeros( N, length(ratediffMethods));

fileHandles=[];

for kk=1:length(ratesMethods)
    ratesName = strcat(fileNameBase, sprintf('_%05d', timeStamp), '_', char(ratesMethods(kk)), '_rates.txt');
    fileHandles(kk) = fopen(ratesName, 'w');
end


[sol, finalV,  exitcode, policyV,...
    outS, intV, intx, xa, D, bfx, bfV, startArray, startxV,...
    policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
    atomx, atomV, atomT, atomJ,...
     ratediff_atom,...
    rateVs...
    ]=mainljwRateNs3FromFile(isAddErr, dataFolder, timeStamp);
 
%     ratediffM(:, 1) = ratediff_bf;
%     ratediffM(:, 2) = ratediff_int;
%     ratediffM(:, 3) = ratediff_atom;
%     ratediffM(:, 4) = ratediff_sol;
    
    for kk=1:length(ratesMethods)
        writeVectorToFile(rateVs(:,kk), fileHandles(kk));
    end
    
    
    xArray{1, 1} = startxV;
    
%fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);

        xArray{1, 2} = xa;
        xArray{1, 3} = bfx;
          xArray{1, 4} = intx;
          xArray{1, 5} = sol;
          xArray{1, 6} = atomx;
          
    startva = mean(startArray, 1);
    startstd = std(startArray, 1);
    [randomV, randomI] = max(startArray(:, 1));
    randomT  = startArray(randomI, 2);
    randomJ  = startArray(randomI, 3);
    
    fprintf(fileID, '%4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f\n',...
      startva(1), startva(2), startva(3), ...
      startstd(1), startstd(2), startstd(3), ...
      policyV, policyT, policyJ, ...
      bfV, bfT, bfJ, ...
      intV, intT, intJ, ...
      finalV, solT, solJ,...
      atomV, atomT, atomJ, ...
      randomV, randomT, randomJ);

  
fclose(fileID);

fprintf(fileDiff, '%.4f\n', ratediff_atom);
fclose(fileDiff);


for kk=1:length(ratesMethods)
    fclose(fileHandles(kk));
end


save(strcat(outputFolder, fileNameBase, '.mat'));



toc
fprintf(fileTime, '%f\n', toc);
fclose(fileTime);

end
