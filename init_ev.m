function init_ev(simu)
    global onOffMap;
    global nMethods;
    in = find(onOffMap(:, 1)>0);
    for k = 1: length(in)
        i = in(k);
        [sol, v] = event_local_greedy(i, simu);
        simu.sys.cur_solutions(:, :, nMethods) = sol;
        simu.sys.cur_metrics(2, nMethods) = v; 

        [res_egr_raw, rate_egr_raw] = calcRates(sol);
                    rateV_egr = sum(rate_egr_raw, 2);
                    [egrT, egrTFI] = sumTFromTArray(rateV_egr);

                    simu.sys.cur_metrics(1, nMethods) = egrT;
                    simu.sys.cur_metrics(3, nMethods) = egrTFI;
                    simu.sys.cur_rates(:, nMethods) = rateV_egr;

                    % resV_egr = sum(rate_egr_raw, 2);
                    % [~, resF_egr] = sumTFromTArray(resV_egr);
                    % simu.sys.cur_metrics(4, nMethods) = resF_egr;
    end

end