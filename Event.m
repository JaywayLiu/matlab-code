classdef Event < handle

    properties
        eventType;
        time;
        %the iterator to the Priority Queue
        data;
        %can access the pq by simu.pq
        simu;
    end

    methods

        function obj = Event(et, time, simu, data)

            if nargin > 0
                obj.eventType = et;

                if nargin > 1
                    obj.time = time;
                    obj.simu = simu;
                    obj.data = data;
                end

            end

        end

        %TODO: this function can be removed out, and set in the constructor
        function handle_event(obj, sys)
            global onOffMap;
            global nMethods;
            global isPrintDebug;
            global isMoving;
            disp('obj time at front');
            disp(obj.time);

            if sys.nSched > obj.simu.N_SCHED_MAX || strcmp(obj.eventType, 'stop')
                disp('will set the stop flag');
                %obj.simu.pq.clear();
                obj.simu.isStop = 1;

            elseif strcmp(obj.eventType, 'onoff')

                duration = obj.time - obj.simu.curTime;

                %move the time first, because the new event need to use the
                %updated the curTime
                obj.simu.curTime = obj.time;

                %use the current system metrics to update the accumulated
                %metrics
                obj.simu.sys.updateAccu(duration);

                %generate the off time
                timeLen = exprnd(obj.simu.EXP_MEAN)

                if isPrintDebug
                    disp('onoffMap');
                    onOffMap
                end

                if strcmp(obj.data.onOffType, 'on')
                    disp('on event');
                    disp(obj.data.flowID);
                    disp("nSched")
                    obj.simu.sys.nSched
                    %compute the duration since the last event

                    %prepare the data for the new event
                    onOffData = OnOffEventData();
                    onOffData.flowID = obj.data.flowID;
                    onOffData.onOffType = 'off';
                    %add the off event
                    obj.simu.addEvent(Event('onoff', obj.simu.curTime + timeLen, obj.simu, onOffData), obj.data.flowID);

                    onOffMap(obj.data.flowID, 1) = ~onOffMap(obj.data.flowID, 1);

                    %add the movement here
                    isMoved = 0;
                    if isMoving
                        global movingP;
                        moveRand = rand();

                        if moveRand < movingP
                            global genType;
                            newPos = genOnePos(genType)
                            locationInfo.ueLocs(:, obj.data.flowID) = newPos;
                            isMoved  = 1;
                        end

                    end

                    %call schedule function
                    %will update both metrics and solutions inside
                    doOneSchedule(obj.simu.sys, isMoved);

                    %right now do the event seperately here, could move it to
                    %doOne?
                    %better not, can have a seperate func for that though
                    [sol, v] = event_local_greedy(obj.data.flowID, obj.simu);
                    obj.simu.sys.cur_solutions(:, :, nMethods) = sol;

                    %otherV = myfunInsideS(sol)
                    %assert(abs(otherV -  v) < 0.00001);
                    %                    obj.simu.sys.cur_metrics(2, nMethods) = otherV;
                    obj.simu.sys.cur_metrics(2, nMethods) = v;
                    %                    assert(otherV == v);

                    [res_egr_raw, rate_egr_raw] = calcRates(sol);

                    rateV_egr = sum(rate_egr_raw, 2);
                    [egrT, egrTFI] = sumTFromTArray(rateV_egr);

                    obj.simu.sys.cur_metrics(1, nMethods) = egrT;
                    obj.simu.sys.cur_metrics(3, nMethods) = egrTFI;

                    % resV_egr = sum(res_egr_raw, 2);
                    % [~, resF_egr] = sumTFromTArray(resV_egr);
                    % obj.simu.sys.cur_metrics(4, nMethods) = resF_egr;

                    obj.simu.sys.cur_rates(:, nMethods) = rateV_egr;

                else
                    %compute the duration since the last event
                    disp('off event');
                    disp(obj.data.flowID);

                    onOffData = OnOffEventData();
                    onOffData.flowID = obj.data.flowID;
                    onOffData.onOffType = 'on';

                    %add the on event
                    obj.simu.addEvent(Event('onoff', obj.simu.curTime + timeLen, obj.simu, onOffData), obj.data.flowID);

                    %update sys based on off flow
                    obj.simu.sys.updateForOnOff(obj.data.flowID);

                end

                %               disp(duration);
            end

            %obj.pq.Add()

        end

    end

end
