function [n, percent] = countFrac(A)
  dim = size(A);
  n =0;
  for i= 1:dim(1)
      isFracRow = 0;
      for j=1:dim(2)
          if A(i, j)>0.1 && A(i, j) <0.9
              isFracRow =1;
          end
      end
      n = n + isFracRow;
  end
  percent = n/dim(1);
end
