function [exitcode, outS, D, ...
        policyx, policyV, policyT, policyJ, policyNJ, policyMM, ...
        intx, intV, intT, intJ, intNJ, intMM, ...
        bfx, bfV, bfT, bfJ, bfNJ, bfMM, ...
         startxV, ...
        solx, solV, solT, solJ, solNJ, solMM, ...
        atomx, atomV, atomT, atomJ, atomNJ, atomMM, ...
        ggx, ggV, ggT, ggJ, ggNJ, ggMM, ...
        grx, grV, grT, grJ, grNJ, grMM, ...
        pex, peV, peT, peJ, peNJ, peMM, ...
        rateVs, resFs] = mainljwForDiscrete(isMoved, prev_pex, prev_policyx)
%res_atom_raw, res_int_raw, res_bf_raw, res_policy_raw
% ratediff_bf, ratediff_int, ratediff_atom, ratediff_sol,...
%rate diff is without doing the percentage

%dataFolder now is the folder to initialize the locations, only need to
%generate for one timestamp? or 256?

%startArray contains cells for V T J
global N;
global M;
global disToBs;
global isBF;
global isMexBf;
global isNotConvex;

global isOpt;
global deployRatio;
% a cell array; 0 is for lte and 1 for wifi aps
% 1 is an array
global nonParInfo;

%can move this to the called later
global isOnOff;
global isOnOffFromFile;

global locationInfo;
global onOffMap;
global rateSource;
global realN;
global MaxRate;
global isWiFiPre;
global phaseId;
global isRunRandom;

%global isBaseWiFiPre;

phaseId = 1;


%%%configurations !!

%rateSource = RandRateMethod.Rand_SINR;
rateSource = RandRateMethod.Rand_MCS;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if nargin<1
% 	isAddErr = 0;
% else
% 	isAddErr = isAddErr0;
% end

%rng(seed)
%    r used in myfunInside, tha is why we make it global
%global r
%r = rand(N, M);
%randM = r;
TRY_DIFF_START_POINT = 5;

startArray = zeros(TRY_DIFF_START_POINT, 4);
startxV = cell(1, TRY_DIFF_START_POINT);

%distGenRandom();

%if ~isFixMaxRate
if isMoved
    mcsIndexFromDist();
    %mcsIndexFromDistFiles(dataFolder, timeStampPassIn);         
    MaxRate = calcMaxRateTable();
end

% else
%     fixMaxRate();
% end

assert(N > M);
%N >M assert
%read the on-off traffic map
if isOnOff

    if isOnOffFromFile
        initOnOffMap(dataFolder);
    end

    onOffM = repmat(onOffMap, 1, M);
end

%%%%%%%%%%%%%%%%%Need to change the N from here for deplyment ratio
%Need a realN for the throughput estimation part
if isMoved
%    policyx = policyGenMaxRate(MaxRate);
    %policyx = policyGenMCS_stick();
    if ~all(prev_policyx)
     policyx = policyGenMaxRate(MaxRate);
    else
     policyx = policyGenMCS_stick_new(prev_policyx, MaxRate);
    end
    %policyx = policyGen();
    pex = policyGenMCS_equal(MaxRate);
else
    policyx = prev_policyx;
    pex = prev_pex;
end


if isRunRandom
    grx = gRandomAvail();
else
    grx = policyx;
end

if deployRatio < 1
    realN = N;
    %use ceil here to make it at least one UE
    N = ceil(N * deployRatio);

    if isWiFiPre
        basex = policyx;
    else
        basex = pex;
    end
end

if deployRatio < 1 && isOpt 
    disp('in opt');
    basex
    %nonParInfo(0) = sum(MaxRate(N+1, realN, :) >0, 2);
    baseNCon = sum(basex, 1)

    nonParInfo{1} = baseNCon;
    %only divide and sum those that are non-zero
    eff_max = MaxRate .* basex
    rev_eff_max  = 1 ./ eff_max;
    rev_eff_max(isinf(rev_eff_max)) = 0;

    nonParInfo{2} = sum(rev_eff_max(N+1: realN, :), 1);


    %[row, col] = find(MaxRate(N + 1:realN, :) > 0);
    %uc = unique(col);
    %nonParInfo{2} = zeros(1, M);

    % for kk = 2:M
    %     rates = MaxRate(row(col == kk) + N, kk);

    %     if (~isempty(rates))
    %         nonParInfo{2}(1, kk) = sum(1 ./ rates);
    %     end

    % end

end

%disp(xa);
%assert(sum(xa, 2) <= ones(N, 1));
%[atomV, atomx] = atom_ljw();
[atomV, atomx] = atom_ljw_init(MaxRate);
%assert(isequal(pex, atomx));
% if ~isequal(pex, atomx)
%     disp('#######################not equal');
% end

if isBF

    if isMexBf
        [bfx, bfV] = bfMex(MaxRate);
        bfx = double(bfx);
        bfV = -bfV;
        %disp(bfx);
    else
        [bfV, bfx] = bfMore();
    end

else
    bfV = atomV;
    bfx = atomx;
end

[ggV, ggx, rate_gg_raw] = global_greedy(MaxRate);
%otherV = myfunInsideS(ggx)

if ~isNotConvex
    disp('##########################running convex solver')

    lb = zeros(N, M);
    ub = ones(N, M);

    Aeq_m = eye(N, N);
    Aeq = Aeq_m;

    for i = 1:M - 1
        Aeq = [Aeq, Aeq_m];
    end

    A = zeros(M, M * N);

    for i = 1:M
        A_m = zeros(M, N);
        A_m(i, :) = ones(1, N);
        A(:, N * (i - 1) + 1:N * (i)) = A_m;
    end

    A = -A;

    %disp(A)

    b = -ones(M, 1);

    beq = ones(N, 1);

    %beq = beq* M*0.8;
    %disp(beq);

    %try to use multiple staring point

    values = zeros(1, TRY_DIFF_START_POINT);
    solutions = cell(1, TRY_DIFF_START_POINT);
    outputs = cell(1, TRY_DIFF_START_POINT);
    exitfs = zeros(1, TRY_DIFF_START_POINT);
    diffs = zeros(1, TRY_DIFF_START_POINT);

    maxRandomV = -1e10;
    maxRandomI = 0;
    randomx = zeros(N, M);

    for k = 1:TRY_DIFF_START_POINT
        x0 = zeros(N, M);
        taken = zeros(1, M);

        %this gurantee at least one user is connected to every ap
        for i = 1:M
            ii = randi(N);
            x0(ii, i) = 1;
            taken(i) = ii;
        end

        for i = 1:N
            %j = mod(i, M) +1;
            %randomly select an ap to associate to
            j = randi(M);

            %if it is not the assigned node in the last init step
            lia = ismember(i, taken);

            if lia == 0
                x0(i, j) = 1;
            end

        end

        startxV{1, k} = x0;

        startV = myfunInsideS(x0);
        startArray(k, 1) = startV;

        if startV > maxRandomV
            maxRandomV = startV;
            % randomx = x0;
            % maxRandomI = k;
        end

        %set the iterations we want to run
        opts = optimoptions(@fmincon, 'MaxFunctionEvaluations', 5000 * N);
        %opts = optimoptions(@fmincon,'Algorithm','sqp', 'MaxFunctionEvaluations', 5000);

        [x, fval, exitflag, output] = fmincon(@myfunInsideS, x0, A, b, Aeq, beq, lb, ub, null_nonlin, opts);

        values(1, k) = fval;
        solutions{1, k} = x;
        diff = startV - fval;
        exitfs(1, k) = exitflag;
        outputs{1, k} = output;
        diffs(1, k) = diff;

    end

    [solV, b] = max(values);
    solx = solutions{1, b};
    exitcode = exitfs(1, b);
    outS = outputs{1, b};

    intx = round(solx);

else
    solx = policyx;
    solV = 0;
    exitcode = 0;
    outS = 'fake outS';

    if ~isBF
        bfx = policyx;
        bfV = 1;
    end

    intx = policyx;
    intV = 1;

end

%/////////////////////////////
%add this if because when running the adding error senario, we only need
%thesolution and then use the real/correct throughts for the
%utility/performance metrics calculations

%first connect the solution with xa.
%tempX = xa;
if deployRatio < 1
    disp("########################concat solutions!!");
      basex

        atomx = conSolution(basex, atomx, N, realN)
        ggx = conSolution(basex, ggx, N, realN)
        %grx = conSolution(pex, grx, N, realN);

    bfx = ggx;
    N = realN;
end

%and then change the N back for utility calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%from here we enter phase 2, which will be the calculation of metrics
phaseId = 2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    if isOnOff
%       bfx = bfx & onOffM;
%       ggx = ggx & onOffM;
%       pex = pex & onOffM;
%       policyx = policyx & onOffM;
%       atomx = atomx & onOffM;
%       grx = grx & onOffM;
%    else
%       bfx = bfx;
%       ggx = ggx;
%       pex = pex;
%       policyx = policyx;
%       atomx = atomx;
%       grx = grx
%    end

[res_bf_raw, rate_bf_raw] = calcRates(bfx);
rateV_bf = sum(rate_bf_raw, 2);
resV_bf = sum(res_bf_raw, 2);
[~, resF_bf] = sumTFromTArray(resV_bf);
%[bfT, bfJ] = sumTNorm(rateV_bf, rateV_bf, bfx);
%[bfT, bfJ] = sumT(bfx);
[bfT, bfJ] = sumTFromTArray(rateV_bf);
bfNJ = sumJNorm(rateV_bf, rateV_bf);

%update the rate_gg_raw if deployRation <1


[res_gg_raw, rate_gg_raw] = calcRates(ggx);
rateV_gg = sum(rate_gg_raw, 2);
resV_gg = sum(res_gg_raw, 2);
[~, resF_gg] = sumTFromTArray(resV_gg);
[ggT, ggJ] = sumTFromTArray(rateV_gg);
ggNJ = sumJNorm(rateV_bf, rateV_gg);

policyV = myfunInsideS(policyx);
[res_policy_raw, rate_policy_raw] = calcRates(policyx);
rateV_policy = sum(rate_policy_raw, 2);
resV_policy = sum(res_policy_raw, 2);
[~, resF_policy] = sumTFromTArray(resV_policy);
[policyT, policyJ] = sumTFromTArray(rateV_policy);
policyNJ = sumJNorm(rateV_bf, rateV_policy);

peV = myfunInsideS(pex);
[res_pe_raw, rate_pe_raw] = calcRates(pex);
rateV_pe = sum(rate_pe_raw, 2);
resV_pe = sum(res_pe_raw, 2);
[~, resF_pe] = sumTFromTArray(resV_pe);

[peT, peJ] = sumTFromTArray(rateV_pe);
peNJ = sumJNorm(rateV_bf, rateV_pe);

if (isRunRandom)
    grV = myfunInsideS(grx);
    [res_gr_raw, rate_gr_raw] = calcRates(grx);
    rateV_gr = sum(rate_gr_raw, 2);
    resV_gr = sum(res_gr_raw, 2);
    [grT, grJ] = sumTFromTArray(rateV_gr);
    [~, resF_gr] = sumTFromTArray(resV_gr);
    grNJ = sumJNorm(rateV_bf, rateV_gr);

else
    grV = policyV;
    rateV_gr = rateV_policy;
    grT = policyT;
    grJ = policyJ;
    grNJ = policyNJ;
    resF_gr = resF_policy;
end

if deployRatio < 1
    ggV = myfunInsideS(ggx);
    atomV = myfunInsideS(atomx);
end

%if deployRatio == 1
    %ggVNew = myfunInsideS(ggx)
    %ggV
    %assert(abs(ggV- ggVNew) < 0.0001)
    %atomVNew = myfunInsideS(atomx)
    %atomVNew
    %assert(abs(atomV- atomVNew) < 0.0001)
%end
%

%     atomVNew = myfunInsideS(atomx)
%     atomV
%    atomx 
%     assert(abs(atomV -atomVNew)<0.0001);

[res_atom_raw, rate_atom_raw] = calcRates(atomx);
rateV_atom = sum(rate_atom_raw, 2);
resV_atom = sum(res_atom_raw, 2);

[~, resF_atom] = sumTFromTArray(resV_atom);
%[atomT, atomJ] = sumT(atomx);
[atomT, atomJ] = sumTFromTArray(rateV_atom);
atomNJ = sumJNorm(rateV_bf, rateV_atom);

[res_sol_raw, rate_sol_raw] = calcRates(solx);
rateV_sol = sum(rate_sol_raw, 2);

resV_sol = sum(res_sol_raw, 2);
[~, resF_sol] = sumTFromTArray(resV_sol);

[solT, solJ] = sumTFromTArray(rateV_sol);
solNJ = sumJNorm(rateV_bf, rateV_sol);

intV = myfunInsideS(intx);
[res_int_raw, rate_int_raw] = calcRates(intx);
rateV_int = sum(rate_int_raw, 2);
resV_int = sum(res_int_raw, 2);
[~, resF_int] = sumTFromTArray(resV_int);
[intT, intJ] = sumTFromTArray(rateV_int);
intNJ = sumJNorm(rateV_bf, rateV_int);


rateVs = zeros(N, 6);
resFs = zeros(1, 6);

rateVs(:, 1) = rateV_policy;
rateVs(:, 2) = rateV_bf;
rateVs(:, 3) = rateV_atom;
rateVs(:, 4) = rateV_gg;
rateVs(:, 5) = rateV_pe;
rateVs(:, 6) = rateV_gr;

resFs(1, 1) = resF_policy;
resFs(1, 2) = resF_bf;
resFs(1, 3) = resF_atom;
resFs(1, 4) = resF_gg;
resFs(1, 5) = resF_pe;
resFs(1, 6) = resF_gr;

policyMM = min(rateV_policy) / max(rateV_policy);
bfMM = min(rateV_bf) / max(rateV_bf);
atomMM = min(rateV_atom) / max(rateV_atom);
ggMM = min(rateV_gg) / max(rateV_gg);
peMM = min(rateV_pe) / max(rateV_pe);
grMM = min(rateV_gr) / max(rateV_gr);

intMM = policyMM;
solMM = policyMM;

%     rateVs(:,2) =rateV_bf ;
%     rateVs(:,3) = rateV_int;
%     rateVs(:,5) = rateV_sol;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ratediff_bf = ((rateV_bf - rateV_policy) ./ rateV_policy)';
%ratediff_atom = ((rateV_atom - rateV_policy) ./ rateV_policy)';
% ratediff_int = ((rateV_int- rateV_policy) ./ rateV_policy)';
% ratediff_sol = ((rateV_sol - rateV_policy) ./ rateV_policy)';

D = disToBs;

function [c, ceq] = null_nonlin(x)
    c = [];
    ceq = [];
end

end

%write another function to make rateGen follow certain distribution
