function pos = genTshapeClustersRandom(nCluster, nUE,  clusterCenters, globalP,  Wh, nSteps, lteAPCenter)

    pos = zeros(2, nUE, nSteps);

    randType = 'uniform';

    % outputName = strcat("multi_cluster", "circ", int2str(angleIn))
    % %checkDir(outputName)

    %nCluster does not count the global one
    p = (1 - globalP) / 3;
    pV(1, 2) =  p * 2;
    pV(1, 3) =  p;

    pV(1, 1) = globalP;

    sump = 0;

    for i = 1:length(pV)
        sump = sump + pV(i);
        pV(i) = sump;
    end
    for j = 1:nSteps

        nUEArray = simuPlacement(pV, nUE);
        posOneRun =  generateRandomPlacementInCircleRandom(nUEArray(1), [0,0], 1, randType);
        for k = 1: nCluster

%           disp("subpos")
            subpos = genNRectPoints(Wh(:, k), clusterCenters(:, k), nUEArray(k+1));
            posOneRun = cat(2, posOneRun, subpos);
        end
        
        pos(:, :, j) = posOneRun;
    end


 
end
