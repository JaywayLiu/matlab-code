%this is based on log normal SINR generation
function f= getRateFromDistDB(i, j)
global disToBsM;

if j==1
    snrInit = 200;
    SNR_db =  128.1 + 37.6 * log10(disToBsM(i, j) / 1000);
    snr = 10 ^ ((snrInit-SNR_db) / 10);
    f =  20 * log2(1+snr);
else
   snrInit = 100;
   SNR_db =  38 + 30 * log10(disToBsM(i, j));
    snr = 10 ^ ((snrInit-SNR_db) / 10);
    f =  40 * log2(1+snr);
end
    
end