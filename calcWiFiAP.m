function [newV, newX] = calcWiFiAP(j, x)
global N;
global M;
global MaxRate;
%global isDist;
newV = myfunInsideS(x);
newX = x;
%delta  =1;

%newV is the current V value
while 1
    minV = 1e10000;
    minx = zeros(N, M);
    for i = 1:N
        xtemp = newX;
        rateTmp = MaxRate(i, j);
        
        %if the rate is larger than 0 and the user has not been determined
        %to a certain wifi ap yet
        if  rateTmp >0 && isSet(newX(i, 2:end))==0
            xtemp(i, 1) = 0;
            xtemp(i, j) = 1;
        else
            continue;
        end
        nv = myfunInsideS(xtemp);
        if nv < minV
            minV = nv;
            minx = xtemp;
        end 
    end

    % if the min is smaller than  the current value, we can continue
    % if equal will stop because of the top while
    %delta  = newV - minV;
    if newV > minV
        newV = minV;
        newX = minx;
    else
        break;
    end

end
    
end

function re = isSet(array)
   re =0;
  for i = array
      re = re | i;
  end
end
