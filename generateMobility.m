datestring = datestr(datetime('now'), 'yyyymmddTHHMMSS');
folderName = strcat('mobility_',datestring);
mkdir(folderName);
cd(folderName);

nSteps = 10;
ti = 30; %in seconds

speed = 1;

%rangeLimit = [400, 400];

rangeRadius  = 180;

nUE = 20;
ori = [200, 200]; %x, y

    randAngle = rand(nUE, 1) * 2* pi;
    pos = zeros(nUE, 2);
    %polar coodination
    pos(:,1) = rangeRadius * rand(nUE, 1) .* cos(randAngle) + ori(1);
    pos(:,2) = rangeRadius * rand(nUE, 1) .* sin(randAngle) + ori(2);
    
for i = 1:nSteps
    radomDirection = rand(nUE, 2);
    pos = pos + radomDirection * ti;
    csvwrite(sprintf('%04d', i), pos);
end
cd('..');

