function f= getMaxRate(i, j)
global isDist;
global rateSource;

        if isDist
            f =  getRateFromDist(i, j); 
        else
            if rateSource == RandRateMethod.Rand_SINR
                f = getRateFromRandomSNR(i, j);
            else
                f = getRateFromMCS(i, j);
            end
        end
end