import os
level1 = ["opt=0", "opt=1"]
level2 = ["0.0000", "0.2500", "0.5000", "0.7500", "1.0000"]

#targets = ["Figs"]
targets = ["minList", "maxList"]

input = "mcsDataOutput"
input= "Figs"

folderName = "dist-partial-32-4-16384-667"
input += "/" + folderName

def checkFolder(dest):
    if not os.path.exists(dest):
        os.makedirs(dest)

for ii in level1:
    for jj in level2:
        p = "/" +ii +"/" +jj +"/"
        for i in targets:
            print("rm -rf "+input+p+i)
            os.system("rm -rf "+input+p+i)
