function maxRateTable = calcMaxRateTable()
global M;
global N;
maxRateTable = zeros(N, M);

for i=1:N
    for j=1:M
        maxRateTable(i, j) = getMaxRate(i, j);
    end
end

end