function f = getRateOnlyPF(x, i, j, MaxRate)
bb=sum(x ,1);
    if bb(j) >0
        f = MaxRate(i, j) / bb(j);
    else
        %if no one connects, rate is 0?  may have inf?
        f=0;
    end
   
end