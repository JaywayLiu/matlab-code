function x = conSolution(policyx, x_in, smallerN, realn)
% x_in is the one calculated from partial information by atom or bf, etc.
% xa is the policy based 
 x = policyx;
 sizep = size(policyx);
% assert(sizep(1) == realn);
 x(1:smallerN, :) = x_in(1:smallerN, :);
 %x(smallerN+1:realn, :) = policyx(smallerN+1:realn, :);
 
end
