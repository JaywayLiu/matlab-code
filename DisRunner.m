function DisRunner(varargin)
    tic
    global M;
    global N;
    global isBF;
    global isBoth;

    global isRunRandom;
    %[nMethods]
    global nConnectedLte;
    global nSumRWifi;
    global allMethods;

    global nMetrics;
    global nMethods;

    global isOnOff;
    global isOnOffFromFile;
    global timeStamp;

    global isOpt;
    global deployRatio;

    global isWiFiPre;
    global isDist;

    global isEventFromFile;
    isEventFromFile = 0;
    isDist = 0;

    global isOutputEventDetails;
    isOutputEventDetails = 0;

    global isNotConvex;
    isNotConvex = 1;

    global lteRadius;
    lteRadius = 1;

    global isPrintDebug;
    isPrintDebug = 0;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% related to moving
    global genType;
    genType = 'uniform';
    %genType = 'cCluster';

    global isMoving;
    isMoving = 0;

    global movingP;
    movingP = 0.25;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    isOpt = 0;
    deployRatio = 1;
    isWiFiPre = 0;

    isRunRandom = 0;
    isOnOff = 1;
    isOnOffFromFile = 0;

    nMetrics = 3;
    allMethods = {"gg", "at", "lg", "wp", "ev"};
    nMethods = length(allMethods);

    nRuns = 512;
    nRuns = 256;
    nRuns = 1024;
    %    nRuns = 1;

    isBF = 0;
    isBoth = 1;
    SCHED_MAX = 100;
    %SCHED_MAX = 160000;
    %SCHED_MAX = 80000;
    %    SCHED_MAX = 40000;
    %SCHED_MAX = 58515
    SCHED_MAX = 16384
    %SCHED_MAX = 65536;

    % will load the table once here
    initWThroughNew();

    %init connection status of UEs before the first on?
    %should run once to init the connections based on various method
    %inputFolderName = 'mcs_5-3-256-667_20190811T115246/'; inputFolderName =
    %'mcs_64-4-256-667_20190818T124213/'   inputFolderName =
    %'mcs_6-4-256-667_20190825T113122/'
    %inputFolderName = 'mcs_64-4-256-667_20190910T212109'
    %inputFolderName = 'mcs_64-4-1024-667_20190916T215215'
    inputFolderName = 'mcs_64-4-1300-667_20191112T223507'
    %   inputFolderName = 'mcs_64-4-1300-667_20191113T085915_cluster-circ'
    %    inputFolderName = 'mcs_6-4-256-667_test_location/'
    outputFolderBase = '/users/jianwel/';
    outputFolderBase = '/scratch2/jianwel/';
    %inputFolderName = 'mcs_5-3-256-667_20190815T135919/';
    %sys = SystemParameters();
    simu = DiscreteSimulator(inputFolderName, outputFolderBase, SCHED_MAX);
    %all_me = zeros(nMetrics, nMethods, nRuns);
    if nargin == 0

        for i = 1:nRuns
            %every random run will have different placment, which will update by
            %calling initStaticLocations
            simu.run(i);
            simu.printSysToFile(i);
            all_me(:, :, i) = simu.sys.accu_metrics;
        end

    else
        i = varargin{1}
        toTestFile = sprintf('%d-%d-%07d_accu.txt', N, M, i)
        ofilename = strcat(outputFolderBase, '/mcsDataOutput/', simu.outputFolder, '/', toTestFile)

        if exist(ofilename) == 2
            disp("file exit");
            exit;
        end

        simu.run(i);
        simu.printSysToFile(i);
    end

    % min(all_me(:, 1, :))
    % max(all_me(:, 1, :))
    % mean(all_me(:, 1, :))
    % min(all_me(:, 2, :))
    % max(all_me(:, 2, :))
    % mean(all_me(:, 2, :))
    toc
    runNum = sprintf('%06d', i);
    checkDir(strcat(outputFolderBase, '/time'));
    timeFile = fopen(strcat(outputFolderBase, '/time', '/time-', runNum, '.txt'), 'w');
    fprintf(timeFile, '%.3f', toc);
    fclose(timeFile);

end
