%% Gray Code Generation
% 
% # Er.Abbas Manthiri S
% # abbasmanthiribe@gmail.com
% # Matlab 2014a
% # 04-04-2017
% # <https://en.wikipedia.org/wiki/Gray_code Reference>
%
%% Initialize
clc
clear all
warning off all
%% demo1 for 2 Bit Gray code sequence Base 2
base=2;
nbit=2;
Seq=grayCodes(base,nbit);
disp('2 bit sequence for Base 2 Gray code')
disp(Seq)
%% demo2 for 4 Bit Gray code sequence Base 2
base=2;
nbit=4;
Seq=grayCodes(base,nbit);
disp('4 bit sequence for Base 2 Gray code')
disp(Seq)
%% demo3 for 5 Bit Gray code sequence Base 2
base=2;
nbit=5;
Seq=grayCodes(base,nbit);
disp('5 bit sequence for Base 2 Gray code')
disp(Seq)
%% demo4 for 4 Bit Gray code sequence Base 3
base=3;
nbit=4;
Seq=grayCodes(base,nbit);
disp('4 bit sequence for Base 3 Gray code')
disp(Seq)
%% demo5 for 3 Bit Gray code sequence Base 10
base=10;
nbit=3;
Seq=grayCodes(base,nbit);
disp('3 bit sequence for Base 10 Gray code')
disp(Seq)