function [currentV, x] = atom_ljw_init(MaxRate)
    global N;
    global M;

    global isOnOff;
    global onOffMap;

    global isOpt;
    global deployRatio;
    %global phaseId;
    global nonParInfo;

    x = zeros(N, M);
    currentV = 0;
    nLte = 0;
    roundTime = zeros(1, M);

  


    assigned = cell(1, M);

    %to be assigned UEs
    assigned{1, 1} = [];

    eMaxRate = MaxRate;
    if isOnOff
        eMaxRate(find(onOffMap(:,1)==0), :) = 0;
    end

    for j = 2:M
        assigned{1, j} = find(eMaxRate(1:N, j) > 0);
    end

    for i = 1:N
        %init with all x(i, 1) = 1;
        if isOnOff
            if onOffMap(i, 1) == 0
                continue;
            end
        end

        if ~any(MaxRate(i, 2:end))
            x(i, 1) = 1;
            nLte = nLte + 1;
            assigned{1, 1}(nLte) = i;
        end
    end

    if isOpt && deployRatio < 1 
        nLteAdd = nLte + nonParInfo{1}(1, 1);
        roundTime = nonParInfo{2};
    end

    if nLte > 0
        if isOpt &&  deployRatio < 1 
            currentV = sum(log(MaxRate(assigned{1, 1}, 1) / nLteAdd));
        else
            currentV = sum(log(MaxRate(assigned{1, 1}, 1) / nLte));
        end
    end

    minXLoop = zeros(N, M);

    %this is the aps that are set
    taken = [];
    kk = 1;
    %then try every wifi ap, need M-1 calls
    %must choose one for each loop, that's why we set the  currentVLoop to large
    %negative
    while length(taken) ~= M - 1
        minJ = 2;
        % every time pick one, may decrease compare with the one before it, but stil
        % need to
        currentVLoop = -1e100;

        for j = 2:M
            %newVArray=[];
            if ~ismember(j, taken) && length(assigned{1, j}) > 0
                %try to offload UEs onto AP j
                %the MaxRate passed in can be eMaxRate, it does not matter too
                %much

                if  isOpt && deployRatio < 1 
                    [newV, newX, newNLte, newRoundTimeJ, assignedToWifi] = calcWiFiAPIncrePartial(j, x, assigned{1, j}, currentV, eMaxRate, N, M, nLte, roundTime(1, j));
                else
                    [newV, newX, newNLte, newRoundTimeJ, assignedToWifi] = calcWiFiAPIncre(j, x, assigned{1, j}, currentV, eMaxRate, N, M, nLte, roundTime(1, j));
                end
                %newVArray(length(newVArray)+1) = newV;
                if newV > currentVLoop
                    currentVLoop = newV;
                    minXLoop = newX;
                    minJ = j;
                    minRT = newRoundTimeJ;
                    minNLte = newNLte;
                    minMoved = assignedToWifi;
                end

            end

        end



if currentVLoop ~= -1e100
            currentV = currentVLoop;
            x = minXLoop;
            roundTime(1, minJ) = minRT;
            nLte = minNLte;

            for tt = 1:length(minMoved)
                assigned = pruneLists(assigned, minMoved(tt));
            end

end

        taken(kk) = minJ;
        kk = kk + 1;

    end

    currentV = -currentV;

    %assert(~isinf(currentV))
    %disp(x);
    % if ~isOnOff
    %     assert(isequal(sum(x, 2), ones(N, 1)));
    % end
end
