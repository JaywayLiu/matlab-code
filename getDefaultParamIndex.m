function index = getDefaultParamIndex(paraName, paraCells)
    if (strcmp(paraName, 'Rw'))
        index = 3;
    else
        index = 1;
    end
end