function points = genNRectPoints(wh, center, n)
    points = zeros(2, n);

    while (n>0)
        p = genOneRectPoint(wh, center);
        if (norm(p) <1)
            points(:, n) = p;
            n = n -1;
        end
    end

end
