function re = pruneLists(listCell, v)
    for i = 2:length(listCell)
        in = find(listCell{1, i}==v);
        listCell{1, i}(in) = [];
    end
    re = listCell;
end