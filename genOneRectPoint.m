function p = genOneRectPoint(wh, center)
    r = rand(2, 1) - [0.5; 0.5];
    sr = r .* wh;
    p = sr + center;

end