function genOneSceneAtom()

datestring = datestr(datetime('now'), 'yyyymmddTHHMMSS');
folderName = strcat('atom_static_mobility_',datestring);
mkdir(folderName);
cd(folderName);

nSteps  = 1;
nUE = 50;

lteAPDist = 10000;
csvwrite('lte_ap.txt', lteAPDist);

wifiRadius = 125;
nAP = 3;
spotRange = 90;
rangeLimit = [450, 450];
alpha = 1.2;

fileID = fopen('scene-config.txt', 'w');
fprintf(fileID, '%d\n', nSteps);
fprintf(fileID, '%d %d\n', nAP, nUE);
fprintf(fileID, '%.4f\n', wifiRadius);
fprintf(fileID, '%.4f\n', spotRange);
fprintf(fileID, '%.4f %.4f\n', rangeLimit(1), rangeLimit(2));
fprintf(fileID, '%.4f\n', alpha);
fclose(fileID);


drawOneSpotFunc(wifiRadius, true, nAP, spotRange, rangeLimit);
generateRandomPlacement(nSteps, nUE, rangeLimit, alpha);
atomParameterTester


cd('..');
end