function intSol = genIntSol(maxIndex, nCol)
   sizeIn = size(maxIndex);
   intSol = zeros(sizeIn(1), nCol);
   for i = 1:sizeIn(1)
       intSol(i, maxIndex(i)) =1;
   end
end

