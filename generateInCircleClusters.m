function pos=generateInCircleClusters(nSteps, nPoints, rangeRadius, randType)
    %nPoints is nUEs
    %check if outside rangeRadius can be optional for this

isVis = 0;
pos = zeros(2, nPoints, nSteps);
nC = 3;
r = 2/3;
phi = pi/4;
circleR = ones(1, nC) .* 0.25;

trans = {
   r, r*cos(phi), r*cos(-phi);
   0, r*sin(phi), r*sin(-phi)
}

%randDist = rangeRadius .* sqrt(randArray) ;
%polar coodination
for i = 1: nSteps
    for j = 1:nPoints
        randAngle = rand();
        randDist = rand();
        randClustId= randi(nC, 1, 1);
        randR = randDist * circleR(1, randClustId);
        pos(1, :, :) =   randR .* cos(randAngle) + trans(1, randClustId);
        pos(2, :, :) = randR .* sin(randAngle) + trans(1, randClustId);
    end
end

if (isVis)
outputFolder = 'image-output';
%outputFolder = strcat(outputFolder, 'mcsDataOutput/new-400ns-16k-new-nobf-partial/');
checkDir(outputFolder);
%ax = gca;
viscircles([0,0],  1, 'Color', 'k', 'LineWidth', 3);
hold on
scatter(pos(1, :, :), pos(2, :, :));
axis equal
end

end

