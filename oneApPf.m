function [x,x0, x0v, x0t, x0j, fval, xt, xj, exitflag, output] = oneApPf(Ninit, ubInit, b)
global ub;
global N;
N = Ninit;

ub= ubInit;
A= ones(1, N);

lb  = zeros(N);


%random capacity
%b= sum(ub) -8

Aeq = zeros(N);
beq = zeros(N, 1);

x0= maxminSol(ub, b);
x0v = myfun(x0);
[x0t, x0j] = sumT(x0);

opts = optimoptions(@fmincon, 'MaxFunctionEvaluations', 5000*N);
[x,fval,exitflag,output] = fmincon(@myfun,x0,A,b,Aeq,beq,lb,ub,null_nonlin, opts);

[xt, xj] = sumT(x);
end

function f = maxminSol(demandV, capacity)
leftN = length(demandV);
f = zeros(1, leftN);

while capacity > 0
    %INDEX is the first one
    minV = min(demandV(demandV~=-1));
    
    capacityTmp = capacity - minV*leftN;
    if capacityTmp >0
        
        for i = 1:length(demandV)
            if(demandV(i)~=-1)
                f(i) = minV;
            end
        end
        capacity = capacityTmp;
        
    else
        share = capacity / leftN;
        for i = 1:length(demandV)
            if(demandV(i)~=-1)
                f(i) = f(i) + share;
            end
        end
        capacity =0;
        
    end
    
    
    
    for i = 1:length(demandV)
        if(demandV(i)==minV)
            demandV(i) = -1;
            leftN = leftN -1;
        end
    end
      
end
end


function f = myfun(x)
global ub;
global N;
th = zeros(1, length(x));
for i=1:length(x)
    if x(i) < ub(i)
        th(i) = x(i);
    else
        th(i) = ub(i);
    end
end
disp(th);
f= sum(-log(th * N * 0.5));
end


function [c,ceq] = null_nonlin(x)
c = [];
ceq = [];
end


function [f, jain] = sumT(x)
global ub;
th = zeros(1, length(x));
for i=1:length(x)
    if x(i) < ub(i)
        th(i) = x(i);
    else
        th(i) = ub(i);
    end
end
%rateV
f = sum(th);
rateVSq = th .^2;
jain = (f^2) / (length(th)*  sum(rateVSq));
end

