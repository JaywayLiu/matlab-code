rng('default');
seed = 1;
rng(seed);
TestN = 50;
nCompare =1;

global N;
global M;
global isBoth;
global underwifiPer;

global isDist;



isDist = 1;
XIN_isboth=[0];
XIN_M=[2];
%XIN_N=[5,10,20];a
XIN_N=[5];
%XIN_wifi=[0.8,0.4,0.6,0.2];
XIN_wifi=[0.8];

errorRates = [0.05, 0.1, 0.2, 0.3, 0.4, 0.5];

%errorRates = [ 0.1, 0.3];
%methodName = {'int', 'atom', 'policy', 'bf', 'frac', 'random'};
methodName = {'bf'};
vDiff = cell(nCompare, length(errorRates));
tDiff = cell(nCompare, length(errorRates));
jDiff = cell(nCompare, length(errorRates));

for i = 1:nCompare
    for j= 1:length(errorRates)
        vDiff{i, j} = zeros(1, TestN);
        tDiff{i, j} = zeros(1, TestN);
        jDiff{i, j} = zeros(1, TestN);
    end
end

for a=1:length(XIN_N)
    for b=1:length(XIN_M)
        for c=1:length(XIN_isboth)
            for d=1:length(XIN_wifi)
                %%%%%%%%%%%%%%%%parameters we want to change
                nn = XIN_N(a);
                N = nn;
                mm = XIN_M(b);
                M = mm;
                isboth = XIN_isboth(c);
                isBoth = isboth;
                underwifi=XIN_wifi(d);
                underwifiPer = underwifi;
                
                %%%%%%%%%%%%%%%%%%%%%%%%%
                
                fileName = sprintf('%d-%d-test%d-%.2f', nn, mm, TestN, underwifi);
                
                fileNametime = sprintf('%d-%d-test%d-%.2f-time', nn, mm, TestN, underwifi);
                if isboth
                    fileName = strcat(fileName,'-both');
                    fileNametime = strcat(fileNametime,'-both');
                else
                    fileName = strcat(fileName,'-pfonly');
                    fileNametime = strcat(fileNametime,'-pfonly');
                end
                
                fileNameOrig = fileName;
                
                fileNameReal = strcat(fileName, '-real');
                fileName = strcat(fileName,'.txt');
                fileNameReal = strcat(fileNameReal,'.txt');
                
                fileNametime = strcat(fileNametime, '-real.txt');
                
                
                
                fileID = fopen(fileName, 'w');
                fileIDReal = fopen(fileNameReal, 'w');
                fileTime = fopen(fileNametime, 'w');
                
                fprintf(fileID, '%s\n',...
                    'bf.v bf.t bf.j');
                
                
                fprintf(fileIDReal, '%s\n',...
                    'bf.v bf.t bf.j');
                
                
                tic
                %store D, x
                dArray = cell(TestN, 1);
                xArray = cell(TestN, nCompare);
                
                dArrayReal = cell(TestN, 1);
                xArrayReal = cell(TestN, nCompare);
                
                
                %this stores the number of times that solutions are
                %different
                nXDiffV = zeros(nCompare, length(errorRates));
                
                for j = 1:length(errorRates)
                    
                    for i=1:TestN
                        %                         vVec = zeros(1, nCompare-1);
                        %                         tVec = zeros(1, nCompare-1);
                        %                         jVec = zeros(1, nCompare-1);
                        
                        vVecReal = zeros(1, nCompare);
                        tVecReal = zeros(1, nCompare);
                        jVecReal = zeros(1, nCompare);
                        

                        [ D, bfx, bfV, bfT, bfJ] =mainljwRateNs3OnlyBf();
                        dArray{i, 1} = D;
                        
                        

                        xArray{i, 1} = bfx;
        

                        xArrayP{ 1} = bfx;

                        
                        
                        %                         vVec(1) = intV;
                        %                         vVec(2) = atomV;
                        %                         vVec(3) = policyV;
                        %                         vVec(4) = bfV;
                        %                         vVec(5) = solV;
                        %
                        %                         tVec(1) = intT;
                        %                         tVec(2) = atomT;
                        %                         tVec(3) = policyT;
                        %                         tVec(4) = bfT;
                        %                         tVec(5) = solT;
                        %
                        %                         jVec(1) = intT;
                        %                         jVec(2) = atomT;
                        %                         jVec(3) = policyT;
                        %                         jVec(4) = bfT;
                        %                         jVec(5) = solT;
                        
                        
                        fprintf(fileID, '%4f %4f %4f\n',...
                            bfV, bfT, bfJ);
                        
                        
                        [ DReal, bfxReal, bfVReal, bfTReal, bfJReal, xArrayR] =mainljwRateNs3AddErrorOnlyBf( errorRates(j), xArrayP, D);
                        
                       
                        dArrayReal{i, 1} = DReal;
                        xArrayReal{i, 1} = bfxReal;
                        
                        %fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);
                        

                        

                        vVecReal(1, 1) = bfVReal;
                        tVecReal(1, 1) = bfTReal;
                        jVecReal(1, 1) = bfJReal;

                        
                        

                        if isequal(bfx, bfxReal) ~=1
                            nXDiffV(1, j) = nXDiffV(1, j) +1;
                        end
                        

                       
                        
                      fprintf(fileIDReal, '%4f %4f %4f\n',...
                            bfVReal, bfTReal, bfJReal);
                        
                        
                        %ii method, j - error rates
                        for ii = 1: nCompare
                            vDiff{ii, j}(i) = -(xArrayR(ii, 1) - vVecReal(1, ii)) / abs(vVecReal(ii)) * 100;
                            tDiff{ii, j}(i) = (xArrayR(ii, 2) - tVecReal(1, ii)) / tVecReal(ii) * 100;
                            jDiff{ii, j}(i) = (xArrayR(ii, 3)  - jVecReal(1, ii)) / jVecReal(ii) * 100;
                        end
                        
                        
                        
                        
                    end
                end
                
                fclose(fileID);
                fclose(fileIDReal);
                
                
                for ii = 1: nCompare
                    outName = sprintf('-diffV-%s.txt', methodName{ii});
                    outName = strcat(fileNameOrig, outName);
                    fileDiff = fopen(outName, 'w');
                    for kk = 1:length(errorRates)
                        for jj = 1: TestN
                            if jj ~=TestN
                                fprintf(fileDiff,  '%.4f ', vDiff{ii, kk}(jj));
                            else
                                fprintf(fileDiff,  '%.4f\n', vDiff{ii, kk}(jj));
                            end
                        end
                    end
                    fclose(fileDiff);
                end
                
                for ii = 1: nCompare
                    outName = sprintf('-diffT-%s.txt', methodName{ii});
                    outName = strcat(fileNameOrig, outName);
                    fileDiff = fopen(outName, 'w');
                    for kk = 1:length(errorRates)
                        for jj = 1: TestN
                            if jj ~=TestN
                                fprintf(fileDiff,  '%.4f ', tDiff{ii, kk}(jj));
                            else
                                fprintf(fileDiff,  '%.4f\n', tDiff{ii, kk}(jj));
                            end
                        end
                        fprintf(fileDiff, '\n');
                    end
                    fclose(fileDiff);
                end
                
                for ii = 1: nCompare
                    outName = sprintf('-diffJ-%s.txt', methodName{ii});
                    outName = strcat(fileNameOrig, outName);
                    fileDiff = fopen(outName, 'w');
                    for kk = 1:length(errorRates)
                        for jj = 1: TestN
                            if jj ~=TestN
                                fprintf(fileDiff,  '%.4f ', jDiff{ii, kk}(jj));
                            else
                                fprintf(fileDiff,  '%.4f\n', jDiff{ii, kk}(jj));
                            end
                        end
                        fprintf(fileDiff, '\n');
                    end
                    fclose(fileDiff);
                end
                
                
                
                outName = sprintf('-nXDiff.txt');
                outName = strcat(fileNameOrig, outName);
                fileDiff = fopen(outName, 'w');
                for ii = 1: nCompare
                    fprintf(fileDiff, '%s\t', methodName{ii});
                    for kk = 1:length(errorRates)
                        fprintf(fileDiff, '%.2f\t', nXDiffV(ii, kk)/TestN);
                    end
                    fprintf(fileDiff, '\n');
                    
                end
                
                outName = sprintf('-nBad.txt');
                outName = strcat(fileNameOrig, outName);
                fileDiff = fopen(outName, 'w');
                for ii = 1: nCompare
                    fprintf(fileDiff, '%s\t', methodName{ii});
                    for kk = 1:length(errorRates)
                        aa= vDiff{ii, kk};
                        fprintf(fileDiff, '%.2f\t', length(aa(aa<0))/TestN);
                    end
                    fprintf(fileDiff, '\n');
                    
                end
                
                
                save(strcat(fileNameReal, '.mat'));
                endTime = toc
                fprintf(fileTime, '%f\n', endTime);
                fclose(fileTime);
                
            end
        end
    end
end