function [minV, x, currentR] = global_greedy(maxRateTable)
global N;
global M;
global isOnOff;
global onOffMap;
x = zeros(N, M);


currentV = 0;

if ~isOnOff
    takenFlag = ones(1, N);
else
    takenFlag = onOffMap';
    x(find(onOffMap==0), 1) = 1;
end
currentR = zeros(N, M);

%kk =1;
%then try every wifi ap, need M-1 calls
%while kk ~= N+1
while (sum(takenFlag) ~=0)
    %then try every UE that is not taken and find the one with
    %maximum PF value increase
    [currentV, x, currentR, takenIndex] = findMaxOfNonTaken(x, currentV, currentR, maxRateTable, takenFlag);

    %taken(kk) = takenIndex;
    takenFlag(1, takenIndex) = 0;
    %kk = kk+1;
    
end
minV = -currentV;
%disp(x);
assert(isequal(sum(x, 2), ones(N, 1)));
end
