function mainPrintAndCallerFixedMCSParam(paM, paraIndex,  inputFolder, outputFolder, timeStamp)

tic

global lteMCSIndex;
global wifiMCSIndex;
global MaxRate;

global isDebug;
global isWiFiPre;
global isDist;
global isMexBf;
global isBF;

global N;
global M;

global isBoth;
isBoth =1;

global isFixMaxRate;


disp('timeStamp=');
disp(timeStamp);



isDist = 0; 
isMexBf = 1;


%///add new global from DisRunner.m
    global isNotConvex;
    global isRunRandom;
%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%
%global that will be controlled by paM
global isOpt;
global deployRatio;
global wifiPos;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%expand all the paM


lenPaM = length(paM);
kk = keys(paM)
%aVs = values(paM);

for i = 1:length(paM)
  pa = paM(kk{i})
  %printf('%s : %d, %s\n', keys{i}, pa.toUseIndex, pa.values(pa.toUseIndex))
  %class(kk{i})
  if strcmp(kk{i},'opt') 
    isOpt =  int8(pa.values{pa.toUseIndex}) - 48
    %isOpt =  str2num(pa.values{pa.toUseIndex}))
  elseif strcmp(kk{i}, 'dRatio')
    %pa.values;
    %pa.toUseIndex;
    deployRatio = str2double(pa.values{pa.toUseIndex})
  elseif strcmp(kk{i}, 'kappa')
    global lteRadius;
    lteRadius= str2double(pa.values{pa.toUseIndex});
  end
end

% disp('deploy Ratio=');
% disp(deployRatio);

nn = N;
mm = M;


nCompare = 8;

%%%%%%%%%%%%%%%files
fileName = sprintf('%d-%d-%06d', nn, mm, timeStamp);

if isBoth
    fileName = strcat(fileName,'-both');
else
    fileName = strcat(fileName,'-pfonly');
   % fileNametime = strcat(fileNametime,'-pfonly');
end

fileNametime = strcat(fileName, '-time');
fileNameBase = fileName;

fileName = strcat(fileName,'.txt');
fileNametime = strcat(fileNametime, '.txt');

fileID = fopen(strcat(outputFolder,fileName), 'w');
fileTime = fopen(strcat(outputFolder, fileNametime), 'w');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xArray = cell(1, nCompare);

%ratediffMethods = {'bf', 'pe', 'atom', 'gr'};
%ratesMethods = {'policy', 'bf', 'int', 'atom', 'sol'};
ratesMethods = {'lgw', 'opt',  'atom', 'gg', 'lge', 'rand'};
%ratediffM = zeros( N, length(ratediffMethods));

fileHandles=[];

for kk=1:length(ratesMethods)
    ratesName = strcat(fileNameBase, '_', char(ratesMethods(kk)), '_rates.txt');
    fileHandles(kk) = fopen(strcat(outputFolder,ratesName), 'w');
end

initUELocations(inputFolder, timeStamp);


prev_pex = zeros(N, M);
prev_policyx = prev_pex; 


[exitcode, outS, D, ...
        policyx, policyV, policyT, policyJ, policyNJ, policyMM, ...
        intx, intV, intT, intJ, intNJ, intMM, ...
        bfx, bfV, bfT, bfJ, bfNJ, bfMM, ...
         startxV, ...
        solx, solV, solT, solJ, solNJ, solMM, ...
        atomx, atomV, atomT, atomJ, atomNJ, atomMM, ...
        ggx, ggV, ggT, ggJ, ggNJ, ggMM, ...
        grx, grV, grT, grJ, grNJ, grMM, ...
        pex, peV, peT, peJ, peNJ, peMM, ...
        rateVs, resFs] = mainljwForDiscrete(1, prev_pex, prev_policyx);
 
    if (isDebug)
        disp(MaxRate);
    end
%     ratediffM(:, 1) = ratediff_bf;
%     ratediffM(:, 2) = ratediff_int;
%     ratediffM(:, 3) = ratediff_atom;
%     ratediffM(:, 4) = ratediff_sol;
   %if (paraIndex == 1)  
    maxRatesName = strcat(fileNameBase,  '_MaxRate.txt');
    csvwrite(strcat(outputFolder, maxRatesName), MaxRate);

    lteMcsName = strcat(fileNameBase,  '_lteMcs.txt');
    csvwrite(strcat(outputFolder, lteMcsName), lteMCSIndex);

    wifiMcsName = strcat(fileNameBase,  '_wifiMcs.txt');
    csvwrite(strcat(outputFolder, wifiMcsName), wifiMCSIndex)
  %end

    %xArray{1, 1} = startxV;
    
%fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);

        xArray{1, 1} = policyx;
        xArray{1, 2} = bfx;
         % xArray{1, 4} = intx;
          %xArray{1, 5} = solx;
          xArray{1, 3} = atomx;
          xArray{1, 4} = ggx;
          xArray{1, 5} = pex;
          xArray{1, 6} = grx;
    
    for kk=1:length(ratesMethods)
        writeVectorToFile(rateVs(:,kk), fileHandles(kk));
        solutionName = strcat(fileNameBase,  '_', char(ratesMethods(kk)), '_x.txt');
        csvwrite(strcat(outputFolder, solutionName), xArray{1, kk});
    end
          
   
    fprintf(fileID, ['%06d %4f %4f %4f %4f %4f %4f '...
         '%4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f '...
	'%4f %4f %4f %4f %4f %4f '...
	'%4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f %4f %4f '...
        '%4f %4f %4f %4f %4f %4f %4f %4f\n'],...
        timeStamp,...
      -0, -0, -0, ...
      0, 0, 0, ...
      -policyV, policyT, policyJ, ...
      -bfV, bfT, bfJ, ...
      -intV, intT, intJ, ...
      -solV, solT, solJ,...
      -atomV, atomT, atomJ, ...
      -ggV, ggT, ggJ, ...
      0, 0, 0,... 
      -peV, peT, peJ,...
      -grV, grT, grJ,...
      policyNJ, bfNJ, intNJ, solNJ, atomNJ, ggNJ, peNJ, grNJ,...
      resFs(1, 1), resFs(1, 2), 0, 0, resFs(1, 3), resFs(1, 4), resFs(1, 5), resFs(1, 6),...  
     policyMM, bfMM, intMM, solMM, atomMM, ggMM,  peMM, grMM);
  
  %3x10 before the NJ part  8 NJs
  
fclose(fileID);


for kk=1:length(ratesMethods)
    fclose(fileHandles(kk));
end


toc
fprintf(fileTime, '%f\n', toc);
fclose(fileTime);

end
