
N = 3;
pq = PriorityQueue();

listLen = 3*N;
eList(listLen) = Event('pre', 0, pq, 0);


%insert some event
for i = 1:N
    eventI = i;
    eList(eventI) = Event('test', i*2, pq, i+1);
% first arg: id/index   second: weights/time
    pq.push(eventI, i*2);
end

sizeq = pq.size()
while (sizeq)
    index = pq.top();
    pq.pop()
    sizeq = pq.size();
    event = eList(index);
    disp('event time=');
    disp(event.time);
    disp('flow id =')
    disp(event.data);

end
