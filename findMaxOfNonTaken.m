function [newV, newx, maxR, takenIndex] = findMaxOfNonTaken(x, currentV, currentR, maxRateTable, takenFlag)
global M;
global isOnOff;
global onOffMap;

    global deployRatio;
    global isOpt;

%find those index that takenFlag  =1, which means not assigned yet

%since this is done at the beginning, no need to do it here
% if isOnOff
%     takenFlag = takenFlag & onOffMap';
% end
ii = find(takenFlag);
newV = currentV;
newx = x;
%this for sure will be updated
takenIndex = -1;
maxDelta = -1e1000;

for k = 1: length(ii)
    i = ii(k);
    jj = find(maxRateTable(i, :) > 0);

    %try all the M APs and see whether it make a larger delta
    for l = 1:length(jj)
       j = jj(l);

    if isOpt && deployRatio < 1 
       [tempV, tempR]= incrementalEvalNoZeroPartial(currentV, currentR, maxRateTable, i, j);
    else
       [tempV, tempR]= incrementalEvalNoZero(currentV, currentR, maxRateTable, i, j);
    end
        delta = tempV - currentV;
        if delta > maxDelta
            tempx = x;
            tempx(i, j) = 1;
            newx = tempx;
            newV = tempV;
            maxR = tempR;
            %no need to update the currentV and currentR
            %currentV = tempV;
            %currentR = tempR;
            takenIndex = i;
            maxDelta = delta;

        end
    end  
    
end
end