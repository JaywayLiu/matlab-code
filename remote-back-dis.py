import glob
import sys
import os

N = int(sys.argv[1])
l = glob.glob("*.txt")

if len(l) < N:
    print("not enought data")
    print("N = "+str(N))
    exit(1)

longFile = "all_long.log"
fFile = "all.log"

header = "gg,atom,lge,lgw,llg\n"

print("cat *.txt > "+longFile)
os.system("cat *.txt > "+longFile)
print("head -%d %s > all.log"%(3*N, longFile))
os.system("head -%d %s > %s"%(3*N, longFile, fFile))

with open("t.log", "w") as f:
    f.write(header)

with open("v.log", "w") as f:
    f.write(header)

with open("TFI.log", "w") as f:
    f.write(header)



print("awk 'NR %% 3 ==1' %s >> t.log"%fFile)
os.system("awk 'NR %% 3 ==1' %s >> t.log"%fFile)

print("awk 'NR %% 3 ==2' %s >> v.log"%fFile)
os.system("awk 'NR %% 3 ==2' %s >> v.log"%fFile)

print("awk 'NR %% 3 ==0' %s >> TFI.log"%fFile)
os.system("awk 'NR %% 3 ==0' %s >> TFI.log"%fFile)