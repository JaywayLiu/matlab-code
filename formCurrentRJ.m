function currentR = formCurrentRJ(rates, sol, j)
    global N;
    global M;
    currentR = zeros(N, M);
    indicies = find(sol(:, j) >0);
    currentR(indicies, j) = rates(indicies);
end