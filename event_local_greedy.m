function [sol, v] = event_local_greedy(flowID, simu)
   %always put this method at the last
    global nMethods;
    global MaxRate;
    apIndexArray = find(MaxRate(flowID, :) > 0);
    sol = simu.sys.cur_solutions(:, : , nMethods);

    v = 0;
         vdelta = -10000000000;
         toSet = 0;
      % I iterate through all the Aps that this UE can reach
        for i = 1:length(apIndexArray)
            kk = apIndexArray(1, i);
            %even though the flow is off, the solution may still connect it to
            %another AP
            %newVDelta = deltaEvalV(simu.sys, flowID, kk);
            currentR = formCurrentRJ(simu.sys.cur_rates(:, nMethods), sol, kk);
            [newVDelta, ~] = incrementalEvalNoZero(-simu.sys.cur_metrics(2, nMethods), currentR, MaxRate, flowID, kk);

            if (newVDelta > vdelta)
                vdelta = newVDelta;
                toSet = kk;
            end

        end


        %no need to add this if? cause it will always be larger than 0?
        %if toSet
         sol(flowID,:) = 0;
         sol(flowID, toSet) = 1;
         v = - vdelta;
        % assert(abs(v) < 100);
        %end


end
