function [x,fval,exitflag,output] =testConceptual()
N = 3;
%A= eye(N, N);
A= [1 ,1,1];

ub = [3, 4, 8];
lb  = zeros(N);

b= 10;

Aeq = zeros(3);
beq = zeros(3, 1);

x0=[2, 3, 8];

opts = optimoptions(@fmincon, 'MaxFunctionEvaluations', 5000*N);
[x,fval,exitflag,output] = fmincon(@myfun,x0,A,b,Aeq,beq,lb,ub,null_nonlin, opts);

end

function f = myfun(x)
f= sum(-log(10+x));
end


function [c,ceq] = null_nonlin(x)
c = [];
ceq = [];
end
