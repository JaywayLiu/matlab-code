rng('default');
seed = 1;
rng(seed);
TestN = 50;
nCompare =6;


%%%%%%%%%%%%%%%%parameters we want to change
nn = 5;
mm = 2;
isboth = 1;
underwifi=0.8;

%%%%%%%%%%%%%%%%%%%%%%%%%

fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);

if isboth
    fileName = strcat(fileName,'-both');
else
    fileName = strcat(fileName,'-pfonly');
end

fileName = strcat(fileName,'.txt');

fileID = fopen(fileName, 'w');
fprintf(fileID, '%s %s %s %s %s %s\n',...
'start.va start.ta start.ja start.vstd start.tstd start.jstd',...
'policy.v policy.t policy.j',...
'bf.v bf.t bf.j',...
'int.v int.t int.j',...
'frac.v frac.t frac.j',...
'atom.v atom.t atom.j');

tic
%store D, x
dArray = cell(TestN, 1);
xArray = cell(TestN, nCompare);
for i=1:TestN 
    [sol, finalV,  exitcode, policyV,...
    outS, intV, intx, xa, D, bfx, bfV, startArray, startxV,...
    policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
    atomx, atomV, atomT, atomJ] =mainljwRateNs3(nn, mm, isboth, underwifi);

    dArray{i, 1} = D;
    
    xArray{i, 1} = startxV;
      xArray{i, 2} = xa;
        xArray{i, 3} = bfx;
          xArray{i, 4} = intx;
          xArray{i, 5} = sol;
          xArray{i, 6} = atomx;
          
    startva = mean(startArray, 1);
    startstd = std(startArray, 1);
    
    fprintf(fileID, '%4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f\n',...
      startva(1), startva(2), startva(3), ...
      startstd(1), startstd(2), startstd(3), ...
      policyV, policyT, policyJ, ...
      bfV, bfT, bfJ, ...
      intV, intT, intJ, ...
      finalV, solT, solJ,...
      atomV, atomT, atomJ);
      
    
end

fclose(fileID);
save(strcat(fileName, '.mat'));
toc