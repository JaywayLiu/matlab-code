global wifiRadius;
global v_edge;
global h_edge;


wifiRadius = 125;
v_edge = 10;
h_edge = 15;

spotRange = 70;
nAP =5;
RangeLimit  =400;

isVis = true;

randAngle = rand(nAP, 1) * 2* pi;
randDist = rand(nAP, 1);

m_centers = zeros(nAP, 2);
m_centers(:,1) = spotRange * randDist .* sin(randAngle);
m_centers(:,2) = spotRange * randDist .* cos(randAngle);
m_centers  = m_centers + ones(nAP, 2) * RangeLimit/2;


if (isVis)
    
clf
axis([0 RangeLimit 0 RangeLimit]);
xt = get(gca, 'XTick');
set(gca, 'FontSize', 14)

ax = gca;

outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

colors = {'b','r','g','y','k'};

for k=1:nAP
viscircles(ax, m_centers(k, :),  wifiRadius, 'Color', colors{k}, 'LineWidth', 4);
viscircles(ax, m_centers(k, :), 2.5, 'Color', colors{k}, 'LineWidth', 4);
%rectangle('Position',[0 0 xlim ylim]);
pause(1);
end

end %end if(isVis)
csvwrite('wifi_centers.txt', m_centers);



print('oneSpot_topo','-dpng')


