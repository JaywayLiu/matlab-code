function [minV, x] = atom_ljw()
global N;
global M;
x = zeros(N, M);


%init with all x(i, 1) = 1;
x(:,1) =1;
minV = myfunInsideS(x);

minVLoop = 1e1000000;
minXLoop = zeros(N, M);

%this is the aps that are set
taken = [];
kk =1;
%then try every wifi ap, need M-1 calls
while length(taken) ~= M-1
    minJ = 2;
    for j = 2:M
        %newVArray=[];
        if ~ismember(j, taken)
            [newV, newX] = calcWiFiAP(j, x);
            %newVArray(length(newVArray)+1) = newV;
            if newV<minVLoop
                minVLoop = newV;
                minXLoop = newX;
                minJ = j;
            end
        end
        
    end
    if minVLoop < minV
        minV = minVLoop;
        x = minXLoop;
    end
    taken(kk) = minJ;
    kk = kk+1;
    
end
%disp(x);
%assert(isequal(sum(x, 2), ones(N, 1)));
end
