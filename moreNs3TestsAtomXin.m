rng('default');
seed = 1;
rng(seed);
TestN = 1000;
%TestN = 1;
nCompare =6;
%XIN_isboth=[0, 1];
XIN_isboth=[0];
XIN_M=[3];
%XIN_N=[5,10,20];
XIN_N=[50];
%XIN_wifi=[0.8];
%XIN_wifi=[0.8,0.4,0.6,0.2];
%XIN_wifi=[0.2];
XIN_wifi=[0.8];

global N;
global M;
global isBoth;
global underwifiPer;
global isBF;


isBF = 0;

for a=1:length(XIN_N)
    for b=1:length(XIN_M)
        for c=1:length(XIN_isboth)
            for d=1:length(XIN_wifi)
%%%%%%%%%%%%%%%%parameters we want to change
                N = XIN_N(a);
                M = XIN_M(b);
                nn = N;
                mm = M;
                isboth = XIN_isboth(c);
                isBoth = isboth;
                underwifi=XIN_wifi(d);
               underwifiPer  = underwifi;

%%%%%%%%%%%%%%%%%%%%%%%%%

fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);
fileNametime = sprintf('%d-%d-test%d-%f-time', nn, mm, TestN, underwifi);
if isboth
    fileName = strcat(fileName,'-both');
    fileNametime = strcat(fileNametime,'-both');
else
    fileName = strcat(fileName,'-pfonly');
    fileNametime = strcat(fileNametime,'-pfonly');
end

fileNameBase = fileName;

fileName = strcat(fileName,'.txt');
fileNametime = strcat(fileNametime, '.txt');

fileID = fopen(fileName, 'w');
fileTime = fopen(fileNametime, 'w');

fprintf(fileID, '%s %s %s %s %s %s %s\n',...
'start.va start.ta start.ja start.vstd start.tstd start.jstd',...
'policy.v policy.t policy.j',...
'bf.v bf.t bf.j',...
'int.v int.t int.j',...
'frac.v frac.t frac.j',...
'atom.v atom.t atom.j', ...
'random.v random.t random.j' );

tic
%store D, x
dArray = cell(TestN, 1);
xArray = cell(TestN, nCompare);

ratediffMethods = {'bf', 'int', 'atom', 'sol'};

ratesMethods = {'policy', 'bf', 'int', 'atom', 'sol'};
ratediffM = zeros(TestN, N, length(ratediffMethods));


fileHandles=[]

for kk=1:length(ratesMethods)
    ratesName = strcat(fileNameBase, '_', char(ratesMethods(kk)), '_rates.txt');
    fileHandles(kk) = fopen(ratesName, 'w');
end

    %atomx, atomV, atomT, atomJ] =mainljwRateNs3();
for i=1:TestN 
    [sol, finalV,  exitcode, policyV,...
    outS, intV, intx, xa, D, bfx, bfV, startArray, startxV,...
    policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
    atomx, atomV, atomT, atomJ, ...
    ratediff_bf, ratediff_int, ratediff_atom, ratediff_sol,...
        rateVs...
    ] =mainljwRateNs3();

    dArray{i, 1} = D;
    
    ratediffM(i, :, 1) = ratediff_bf;
    ratediffM(i, :, 2) = ratediff_int;
    ratediffM(i, :, 3) = ratediff_atom;
    ratediffM(i, :, 4) = ratediff_sol;
    
    for kk=1:length(ratesMethods)
        writeVectorToFile(rateVs(:,kk), fileHandles(kk));
    end
    
    
    xArray{i, 1} = startxV;
    
%fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);

        xArray{i, 2} = xa;
        xArray{i, 3} = bfx;
          xArray{i, 4} = intx;
          xArray{i, 5} = sol;
          xArray{i, 6} = atomx;
          
    startva = mean(startArray, 1);
    startstd = std(startArray, 1);
    [randomV, randomI] = max(startArray(:, 1));
    randomT  = startArray(randomI, 2);
    randomJ  = startArray(randomI, 3);
    
    fprintf(fileID, '%4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f\n',...
      startva(1), startva(2), startva(3), ...
      startstd(1), startstd(2), startstd(3), ...
      policyV, policyT, policyJ, ...
      bfV, bfT, bfJ, ...
      intV, intT, intJ, ...
      finalV, solT, solJ,...
      atomV, atomT, atomJ, ...
      randomV, randomT, randomJ);
      
    
end

for kk=1:length(ratesMethods)
    fclose(fileHandles(kk));
end

for kk = 1:length(ratediffMethods)
    fileCompName = strcat(fileNameBase, '_', char(ratediffMethods(kk)),'_policyComp', '.txt');
    fileCompID = fopen(fileCompName, 'w');
    
    fileCompSumName = strcat(fileNameBase, '_', char(ratediffMethods(kk)),'_sumComp', '.txt');
    fileCompSumID = fopen(fileCompSumName, 'w');
    
    avgCount = 0;
    medianCount = 0;
    for j=1:N
      tempV = [];
      t = 1;
      count10 =0;
      count25 = 0;
      count50 = 0;
      count100=0;
      countneg =0;

      for i=1:TestN 
          if ~isinf(ratediffM(i, j, kk))
              tempV(t) = ratediffM(i, j, kk);
              if tempV(t) >=0.1
                  count10 =count10 + 1;
              end
              if tempV(t) >=0.25
                  count25 =count25 + 1;
              end
              if tempV(t) >=0.5
                  count50 =count50 + 1;
              end              
              if tempV(t) >=1
                  count100 =count100 + 1;
              end
              if tempV(t)<0
                 countneg = countneg +1;
              end              
              fprintf(fileCompID, '%4f ', tempV(t));           
              t = t+1;
          end
      end
      fprintf(fileCompID, '\n');
      maxC = max(tempV);
      minC = min(tempV);
      avgC = mean(tempV);
      medianC = median(tempV);
      stdC = std(tempV);
      
      if avgC>0
          avgCount = avgCount + 1;
      end
    
      if medianC>0
          medianCount = medianCount + 1;
      end
      
      fprintf(fileCompSumID, '%4f %4f %4f %4f %4f %d %d %d %d %d %d\n', maxC, minC, avgC, medianC, stdC, countneg, count10, count25, count50, count100, length(tempV));
      
    end
    
    fprintf(fileCompSumID, '%.4f %.4f\n', avgCount/N, medianCount/N);
    
    fclose(fileCompID);
    fclose(fileCompSumID);
end

fclose(fileID);
save(strcat(fileName, '.mat'));
toc
fprintf(fileTime, '%f\n', toc);
fclose(fileTime);

            end 
        end
    end
end
