rng('default');
%TestN = 50;
TestN = 5000;
nCompare =6;

global N;
global M;
global isBoth;
global underwifiPer;


global isDist;

isDist = 1;

%XIN_isboth=[0];
XIN_M=[3];
%XIN_N=[5,10,20];
XIN_N=[5];
%XIN_wifi=[0.4,0.6,0.2];
%XIN_wifi=[0.8];

errorRates = [0.05, 0.1, 0.2, 0.3, 0.4, 0.5];
%errorRates = [ 0.2];

%errorRates = [ 0.1, 0.3];
methodName = {'int', 'atom', 'policy', 'bf', 'frac', 'random'};

vDiff = cell(nCompare, length(errorRates));
tDiff = cell(nCompare, length(errorRates));
jDiff = cell(nCompare, length(errorRates));

for i = 1:nCompare
    for j= 1:length(errorRates)
        vDiff{i, j} = zeros(1, TestN);
        tDiff{i, j} = zeros(1, TestN);
        jDiff{i, j} = zeros(1, TestN);
    end
end

for a=1:length(XIN_N)
    for b=1:length(XIN_M)
        for c=1:length(XIN_isboth)
            for d=1:length(XIN_wifi)
                %%%%%%%%%%%%%%%%parameters we want to change
                N = XIN_N(a);
                nn = N;
                M = XIN_M(b);
                mm= M;
                isBoth = XIN_isboth(c);
                isboth = isBoth;
                underwifiPer =XIN_wifi(d);
                underwifi = underwifiPer; 
                
                %%%%%%%%%%%%%%%%%%%%%%%%%
                
                fileName = sprintf('%d-%d-test%d-%.2f', nn, mm, TestN, underwifi);
                
                fileNametime = sprintf('%d-%d-test%d-%.2f-time', nn, mm, TestN, underwifi);
                if isboth
                    fileName = strcat(fileName,'-both');
                    fileNametime = strcat(fileNametime,'-both');
                else
                    fileName = strcat(fileName,'-pfonly');
                    fileNametime = strcat(fileNametime,'-pfonly');
                end
                
                fileNameOrig = fileName;
                
                fileNameReal = strcat(fileName, '-real');
                fileNametime = strcat(fileNametime, '-real');

                fileName = strcat('small-', fileName);
                fileNameReal = strcat('small-', fileNameReal);
                fileNametime= strcat('small-', fileNametime);
                
                fileName = strcat(fileName,'.txt');
                fileNameReal = strcat(fileNameReal,'.txt');
                fileNametime = strcat(fileNametime, '.txt');
		

                if testnI==0:

                
                fileID0 = fopen(fileName+{'0:06'}.format(0), 'w');
                fileIDReal0 = fopen(fileNameReal+{'0:06'}.format(0), 'w');
                fileTime0 = fopen(fileNametime+{'0:06'}.format(0), 'w');
                
                fprintf(fileID0, '%s %s %s %s %s %s\n',...
                    'start.va start.ta start.ja start.vstd start.tstd start.jstd',...
                    'policy.v policy.t policy.j',...
                    'bf.v bf.t bf.j',...
                    'int.v int.t int.j',...
                    'frac.v frac.t frac.j',...
                    'atom.v atom.t atom.j');
                
                
                fprintf(fileIDRea0, '%s %s %s %s %s %s\n',...
                    'start.va start.ta start.ja start.vstd start.tstd start.jstd',...
                    'policy.v policy.t policy.j',...
                    'bf.v bf.t bf.j',...
                    'int.v int.t int.j',...
                    'frac.v frac.t frac.j',...
                    'atom.v atom.t atom.j');
                end
                
                
                tic
                %store D, x
                dArray = cell(TestN, 1);
                xArray = cell(TestN, nCompare);
                
                dArrayReal = cell(TestN, 1);
                xArrayReal = cell(TestN, nCompare);
                
                
                %this stores the number of times that solutions are
                %different
                nXDiffV = zeros(nCompare-1, length(errorRates));
                
                for j = 1:length(errorRates)
                    
                    %for i=1:TestN
                      i = testnI;

			rng(i);

                        disp('TestN='+i);
                        %                         vVec = zeros(1, nCompare-1);
                        %                         tVec = zeros(1, nCompare-1);
                        %                         jVec = zeros(1, nCompare-1);
                        
                        vVecReal = zeros(1, nCompare-1);
                        tVecReal = zeros(1, nCompare-1);
                        jVecReal = zeros(1, nCompare-1);
                        
                        [sol, solV,  exitcode, policyV,...
                            outS, intV, intx, policyx, D, bfx, bfV, startV, startx,...
                            policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
                            atomx, atomV, atomT, atomJ] =mainljwRateNs3(1);
                      %1 means adding error, and will not calculate the v,t,j values, how about the random ones?
                        
                        dArray{i, 1} = D;
                        
                        
                        xArray{i, 1} = intx;
                        xArray{i, 2} = atomx;
                        xArray{i, 3} = policyx;
                        xArray{i, 4} = bfx;
                        xArray{i, 5} = sol;
                        xArray{i, 6} = startx;
                        
                        xArrayP{ 1} = intx;
                        xArrayP{ 2} = atomx;
                        xArrayP{ 3} = policyx;
                        xArrayP{ 4} = bfx;
                        xArrayP{ 5} = sol;
                        
                        
                        %                         vVec(1) = intV;
                        %                         vVec(2) = atomV;
                        %                         vVec(3) = policyV;
                        %                         vVec(4) = bfV;
                        %                         vVec(5) = solV;
                        %
                        %                         tVec(1) = intT;
                        %                         tVec(2) = atomT;
                        %                         tVec(3) = policyT;
                        %                         tVec(4) = bfT;
                        %                         tVec(5) = solT;
                        %
                        %                         jVec(1) = intT;
                        %                         jVec(2) = atomT;
                        %                         jVec(3) = policyT;
                        %                         jVec(4) = bfT;
                        %                         jVec(5) = solT;
                       

  
                        [solReal, solVReal,  exitcodeReal, policyVReal,...
                            outSReal, intVReal, intxReal, policyxReal, DReal, bfxReal, bfVReal, startVReal,...
                            policyTReal, policyJReal, bfTReal, bfJReal, solTReal, solJReal, intTReal, intJReal,...
                            atomxReal, atomVReal, atomTReal, atomJReal, xArrayR] =mainljwRateNs3AddError(errorRates(j), xArrayP, D, startx);
                        

                        startva = mean(startV, 1);
                        startstd = std(startV, 1);
                        
                        fprintf(fileID, '%4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f\n',...
                            startva(1), startva(2), startva(3), ...
                            startstd(1), startstd(2), startstd(3), ...
                         xArrayR(3, 1), xArrayR(3,2), xArrayR(3,3), ...
                         xArrayR(4, 1), xArrayR(4,2), xArrayR(4,3), ...
                         xArrayR(1, 1), xArrayR(1,2), xArrayR(1,3), ...
                         xArrayR(5, 1), xArrayR(5,2), xArrayR(5,3), ...
                         xArrayR(2, 1), xArrayR(2,2), xArrayR(2,3)); 
%
%                            policyV, policyT, policyJ, ...
%                            bfV, bfT, bfJ, ...
%                            intV, intT, intJ, ...
%                            solV, solT, solJ,...
%                            atomV, atomT, atomJ);
%                        
                                               dArrayReal{i, 1} = DReal;
                        xArrayReal{i, 1} = startx;
                        
                        %fileName = sprintf('%d-%d-test%d-%f', nn, mm, TestN, underwifi);
                        
                        xArrayReal{i, 2} = policyxReal;
                        xArrayReal{i, 3} = bfxReal;
                        xArrayReal{i, 4} = intxReal;
                        xArrayReal{i, 5} = solReal;
                        xArrayReal{i, 6} = atomxReal;
                        
                        vVecReal(1) = intVReal;
                        vVecReal(2) = atomVReal;
                        vVecReal(3) = policyVReal;
                        vVecReal(4) = bfVReal;
                        vVecReal(5) = solVReal;
                        
                        tVecReal(1) = intTReal;
                        tVecReal(2) = atomTReal;
                        tVecReal(3) = policyTReal;
                        tVecReal(4) = bfTReal;
                        tVecReal(5) = solTReal;
                        
                        jVecReal(1) = intJReal;
                        jVecReal(2) = atomJReal;
                        jVecReal(3) = policyJReal;
                        jVecReal(4) = bfJReal;
                        jVecReal(5) = solJReal;
                        
                        startvaReal = mean(startVReal, 1);
                        startstdReal = std(startVReal, 1);
                        
                        if isequal(intx, intxReal) ~=1
                            nXDiffV(1,j) = nXDiffV(1, j) +1;
                        end
                        
                        if isequal(atomx, atomxReal) ~=1
                            nXDiffV(2,j) = nXDiffV(2, j) +1;
                        end
                        
                        if isequal(policyx, policyxReal) ~=1
                            nXDiffV(3,j) = nXDiffV(3, j) +1;
                        end
                        if isequal(bfx, bfxReal) ~=1
                            nXDiffV(4,j) = nXDiffV(4, j) +1;
                        end
                        
                        if isequal(sol, solReal) ~=1
                            nXDiffV(5,j) = nXDiffV(5, j) +1;
                        end
                        
                        fprintf(fileIDReal, '%4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f\n',...
                            startvaReal(1), startvaReal(2), startvaReal(3), ...
                            startstdReal(1), startstdReal(2), startstdReal(3), ...
                            policyVReal, policyTReal, policyJReal, ...
                            bfVReal, bfTReal, bfJReal, ...
                            intVReal, intTReal, intJReal, ...
                            solVReal, solTReal, solJReal,...
                            atomVReal, atomTReal, atomJReal);
                        
                        
                        %ii method, j - error rates
                        for ii = 1: nCompare-1
                            vDiff{ii, j}(i) = -(xArrayR(ii, 1) - vVecReal(ii)) / abs(vVecReal(ii)) * 100;
                            tDiff{ii, j}(i) = (xArrayR(ii, 2) - tVecReal(ii)) / tVecReal(ii) * 100;
                            jDiff{ii, j}(i) = (xArrayR(ii, 3)  - jVecReal(ii)) / jVecReal(ii) * 100;
                        end
                        
                        
                        
                        
                    end
                end
                
                fclose(fileID);
                fclose(fileIDReal);
                
                
                for ii = 1: nCompare-1
                    outName = sprintf('-diffV-%s.txt', methodName{ii});
                    outName = strcat(fileNameOrig, outName);
                    fileDiff = fopen(outName, 'w');
                    for kk = 1:length(errorRates)
                        for jj = 1: TestN
                            if jj ~=TestN
                                fprintf(fileDiff,  '%.4f ', vDiff{ii, kk}(jj));
                            else
                                fprintf(fileDiff,  '%.4f\n', vDiff{ii, kk}(jj));
                            end
                        end
                    end
                    fclose(fileDiff);
                end
                
                for ii = 1: nCompare-1
                    outName = sprintf('-diffT-%s.txt', methodName{ii});
                    outName = strcat(fileNameOrig, outName);
                    fileDiff = fopen(outName, 'w');
                    for kk = 1:length(errorRates)
                        for jj = 1: TestN
                            if jj ~=TestN
                                fprintf(fileDiff,  '%.4f ', tDiff{ii, kk}(jj));
                            else
                                fprintf(fileDiff,  '%.4f\n', tDiff{ii, kk}(jj));
                            end
                        end
                        fprintf(fileDiff, '\n');
                    end
                    fclose(fileDiff);
                end
                
                for ii = 1: nCompare-1
                    outName = sprintf('-diffJ-%s.txt', methodName{ii});
                    outName = strcat(fileNameOrig, outName);
                    fileDiff = fopen(outName, 'w');
                    for kk = 1:length(errorRates)
                        for jj = 1: TestN
                            if jj ~=TestN
                                fprintf(fileDiff,  '%.4f ', jDiff{ii, kk}(jj));
                            else
                                fprintf(fileDiff,  '%.4f\n', jDiff{ii, kk}(jj));
                            end
                        end
                        fprintf(fileDiff, '\n');
                    end
                    fclose(fileDiff);
                end
                
                
                
                outName = sprintf('-nXDiff.txt');
                outName = strcat(fileNameOrig, outName);
                fileDiff = fopen(outName, 'w');
                for ii = 1: nCompare-1
                    fprintf(fileDiff, '%s\t', methodName{ii});
                    for kk = 1:length(errorRates)
                        fprintf(fileDiff, '%.2f\t', nXDiffV(ii, kk)/TestN);
                    end
                    fprintf(fileDiff, '\n');
                    
                end
                
                outName = sprintf('-nBad.txt');
                outName = strcat(fileNameOrig, outName);
                fileDiff = fopen(outName, 'w');
                for ii = 1: nCompare-1
                    fprintf(fileDiff, '%s\t', methodName{ii});
                    for kk = 1:length(errorRates)
                        aa= vDiff{ii, kk};
                        fprintf(fileDiff, '%.2f\t', length(aa(aa<0))/TestN);
                    end
                    fprintf(fileDiff, '\n');
                    
                end
                
                
                save(strcat(fileNameReal, '.mat'));
                toc
                fprintf(fileTime, '%f\n', toc);
                fclose(fileTime);
                
            end
        end
    end
end
