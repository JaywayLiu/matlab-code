function generateMCSIndex(N, M, nSteps)


rng(110);

 %N = 12;
 %M = 4;
 %nSteps = 1000;



global lteMCSTable;
global wifiMCSTable;
initMCSTables();
idString = sprintf('%d-%d-%d', N, M, nSteps);
datestring = datestr(datetime('now'), 'yyyymmddTHHMMSS');
dataFolder = strcat('mcs_', idString, '_', datestring);

mkdir(dataFolder);
cd(dataFolder);

for i = 1:nSteps
    %lte is always connected for a single lte right now
    %can change it here, just make 0 possible like the wifi
    lteMcsIndex = randi(length(lteMCSTable), N, 1);
    wifiMcsIndex = randi(length(wifiMCSTable), N, M-1);
    csvwrite(sprintf('lte-%04d.txt', i),lteMcsIndex);
    csvwrite(sprintf('wifi-%04d.txt', i),wifiMcsIndex);
end
cd('..');

end
