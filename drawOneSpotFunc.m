function drawOneSpotFunc(wifiRadius, isVis, nAP, spotRange, rangeLimit)
%spotRange is 1 dim, radisu of the range
%this is the limit of the whole scene


randAngle = rand(nAP, 1) * 2* pi;
randDist = rand(nAP, 1);

m_centers = zeros(nAP, 2);
m_centers(:,1) = spotRange * randDist .* sin(randAngle);
m_centers(:,2) = spotRange * randDist .* cos(randAngle);

%recenter the APs to the center of the rangeLimit
m_centers  = m_centers + repmat (rangeLimit/2, nAP, 1);


if (isVis)
    
clf
axis([0 rangeLimit(1) 0 rangeLimit(2)]);
set(gca, 'FontSize', 14)
%xt = get(gca, 'XTick');


ax = gca;

outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

colors = {'b','r','g','y','k'};

for k=1:nAP
viscircles(ax, m_centers(k, :),  wifiRadius, 'Color', colors{k}, 'LineWidth', 4);
viscircles(ax, m_centers(k, :), 2.5, 'Color', colors{k}, 'LineWidth', 4);
%rectangle('Position',[0 0 xlim ylim]);
pause(1);

end
print('oneSpot_topo','-dpng')
end %end if(isVis)

csvwrite('wifi_centers.txt', m_centers);

end


