function nUEArray = carDealer(nCluster, nUE)

    nUEArray = zeros(1, nCluster + 1);

    nUEArray(1) = floor(globalP * nUE);

    rest = nUE - nUEArray(1);

    modr = mod(rest, nCluster);

    div = floor(rest ./ nCluster);

    nUEArray(2:end) = div;

    aa = randi(nCluster, 1, modr);

    for j = 1:length(aa)
        indexToAdd = aa(j) + 1;
        nUEArray(indexToAdd) = nUEArray(indexToAdd) + 1;
    end

    assert(sum(nUEArray) == nUE)

end
