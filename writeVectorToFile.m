function writeVectorToFile(v, fileHandle)
  for i=1:length(v)
    if i~=length(v)
        fprintf(fileHandle, '%4f, ', v(i));
    else
        fprintf(fileHandle, '%4f', v(i));
    end
  end
  fprintf(fileHandle, '\n');
end