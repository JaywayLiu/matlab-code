    classdef AP
        properties
            location;
            isWiFi;
            isPF;
        end
        methods
            function obj = AP(l,t,p)
                obj.location = l;
                obj.isWiFi = t;
                obj.isPF = p;
            end
            function D = dist(x)
                xx = x(1) - obj.location (1);
                yy = x(2) - obj.location (2);
                D = sqrt(xx * xx +  yy * yy);

            end
        end
    end