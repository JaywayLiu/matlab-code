
function [sol, finalV, exitcode, startV, outS, intV, randM] =mainljw()
    global N;
    global M;
    N = 100;
    M =2;
    %N >M assert
    global r
    r = rand(N, M);
    randM = r;

    lb = zeros(N, M);
    ub = ones(N, M);

    Aeq_m = eye(N, N);
    Aeq = Aeq_m;

    for i=1:M-1
        Aeq = [Aeq, Aeq_m];
    end

%     A_m = eye(M, M);
%     A = A_m;
% 
%     for i=1:N-1
%         A = [A, A_m];
%     end
%     A = -A;
%     
%     disp(A)
    

   A = zeros(M, M*N);
   
   for i= 1:M
    A_m = zeros(M, N);
    A_m(i, :) = ones(1, N);
    A(:, N*(i-1)+1:N*(i)) = A_m;
   end
   A = -A;
   
   %disp(A)
   
    b =-ones(M,1);

    
    beq=ones(N, 1);
    
        beq = beq* M*0.8;
    disp(beq);
    
    %try to use multiple staring point
        TRY_DIFF_START_POINT = 1;
    values = zeros(1, TRY_DIFF_START_POINT);
    solutions = cell(1, TRY_DIFF_START_POINT);
    outputs = cell(1, TRY_DIFF_START_POINT);
    exitfs = zeros(1, TRY_DIFF_START_POINT);
    diffs = zeros(1, TRY_DIFF_START_POINT);
    

    
    for k=1:TRY_DIFF_START_POINT
        x0 = zeros(N,M);
    taken = zeros(1, M);
    
    %this gurantee at least one user is connected to every ap
    for i=1:M
        ii = randi(N);
        x0(ii, i) = 1;
        taken(i) = ii;    
    end
    
    for i=1: N
        %j = mod(i, M) +1;
        %randomly select an ap to associate to
        j = randi(M);
        
        %if it is not the assigned node in the last init step
        lia = ismember(i, taken);
        if lia==0
         x0(i, j) = 1;
        end
    end
    
    startV = myfunInside(x0)
    
    %set the iterations we want to run
    opts = optimoptions(@fmincon, 'MaxFunctionEvaluations', 5000*N);
    %opts = optimoptions(@fmincon,'Algorithm','sqp', 'MaxFunctionEvaluations', 5000);
        
    [x,fval,exitflag,output] = fmincon(@myfunInside,x0,A,b,Aeq,beq,lb,ub,null_nonlin, opts);
    
    values(1, k) = fval;
    solutions{1, k} = x;
    diff =  startV - fval;
    exitfs(1, k) = exitflag; 
    outputs{1, k} = output;
    diffs(1, k) = diff;
    
    end
    

    [finalV, b] = max(values);
    sol = solutions{1, b};
    exitcode = exitfs(1, b);
    outS = outputs{1, b};
    
    int_sol = round(sol);
    
    intV = myfunInside(int_sol);
    
    %for i =1: size(solutions)
        %disp('solu'+i)
        %disp(solutions{i});
       %aa = isequal(solutions{i}, solutions{b});
    %end
    %disp(x)
    %disp(fval)
    %disp(values);
    %disp(std(values));
    %disp(diff(b));
    %assignin('base', 'finalSol', finalSol)
   
end

function [c,ceq] = null_nonlin(x)
c = [];
ceq = [];
end


function f = myfunInside(x)
%     N = 5;
%     M = 3;
    global M
    global N
    global r
    %disp('M=')
    %disp(M)
    f=0;

    for i=1:N
        for j=1:M
            bb=sum(x,1);
            
            f = f+ r(i, j) * x(i,j) * (1/ bb(j));
        end
    end
    f =-log(f);
end

