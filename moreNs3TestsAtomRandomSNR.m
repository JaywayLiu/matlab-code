rng('default');
seed = 1;
rng(seed);
TestN = 50;
nCompare =6;
global N;
global M;
global NodeApSNR;
global cof;
global CTLFREQU;
global Band;

global isDist;
isDist  =0;

global isBoth;
global underwifiPer;

CTLFREQU = 7;
cof=0.1;%the gloabl variable for cof we can change.
Band=10;



%%%%%%%%%%%%%%%%parameters we want to change
N = 5;
M = 3;

NodeApSNR=zeros(N,M);
isBoth = 1;
underwifiPer = 0.8;


%%%%%%%%%%%%%%%%%%%%%%%%%

fileName = sprintf('%d-%d-test%d-%f-control%d', N, M, TestN, underwifiPer, CTLFREQU);

if isBoth
    fileName = strcat(fileName,'-both');
else
    fileName = strcat(fileName,'-pfonly');
end

fileName = strcat(fileName,'.txt');

fileID = fopen(fileName, 'w');
fprintf(fileID, '%s %s %s %s %s %s\n',...
    'start.va start.ta start.ja start.vstd start.tstd start.jstd',...
    'policy.v policy.t policy.j',...
    'bf.v bf.t bf.j',...
    'int.v int.t int.j',...
    'frac.v frac.t frac.j',...
    'atom.v atom.t atom.j');

tic
%store D, x

     xA = cell(1, nCompare);
snrArray = cell(TestN,1);
xArray = cell(TestN, nCompare);
GetRandomSNRInitial();

for i=1:TestN
    
    % GetRandomNodeApSNRUpdate() .....
    GetRandomNodeApSNRUpdate();
    if CTLFREQU==1 ||  mod(i, CTLFREQU)==1
       disp(i);
        
        [sol, finalV,  exitcode, policyV,...
            outS, intV, intx, xa,  bfx, bfV, startArray, startxV,...
            policyT, policyJ, bfT, bfJ, solT, solJ, intT, intJ,...
            atomx, atomV, atomT, atomJ] =mainljwRateNs3RandomSNR();
        
        xArray{i, 1} = startxV;
        xArray{i, 2} = xa;
        xArray{i, 3} = bfx;
        xArray{i, 4} = intx;
        xArray{i, 5} = sol;
        xArray{i, 6} = atomx;
        
        xA{1, 1} = startxV;
        xA{1, 2} = xa;    
        xA{1, 3} = bfx;
        xA{1, 4} = intx;
        xA{1, 5} = sol;
        xA{1, 6} = atomx;
        
        
    else
        [startArray, policyV, policyT, policyJ, ...
            bfV, bfT, bfJ, ...
            intV, intT, intJ, ...
            finalV, solT, solJ,...
            atomV, atomT, atomJ] = updatedResultsFromOldAssociation(xA);
    end
    
    snrArray{i, 1} = NodeApSNR;
    
    
    startva = mean(startArray, 1);
    startstd = std(startArray, 1);
    
    fprintf(fileID, '%4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f %4f\n',...
        startva(1), startva(2), startva(3), ...
        startstd(1), startstd(2), startstd(3), ...
        policyV, policyT, policyJ, ...
        bfV, bfT, bfJ, ...
        intV, intT, intJ, ...
        finalV, solT, solJ,...
        atomV, atomT, atomJ);
    
    
end

fclose(fileID);
save(strcat(fileName, '.mat'));
toc