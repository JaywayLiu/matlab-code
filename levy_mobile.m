%z = levy(3000, 2, 1.2);



%plot(z(:,1), z(:,2), '-o')
wifiRadius = 125;

gridD = wifiRadius *1.25;
a = gridD:gridD:1000-gridD;
[X, Y] = meshgrid(a, a);
wSize = size(X);
nwifi = wSize(1) * wSize(2);

nUE = nwifi * 40;

m_centers = zeros(nwifi, 2);

m_centers(:,1) = reshape(X, [1, nwifi]);
m_centers(:,2) = reshape(Y, [1, nwifi]);

%viscircles(m_centers, ones(nwifi, 1) * wifiRadius, 'LineWidth', 2);
point = randi(1000, nUE, 2);


[Idx, D] = knnsearch(m_centers, point, 'K', 1, 'NSMethod', 'kdtree');
