%this is the scaled version with S
%scaled the throughput with N*0.5
function f = myfunInsideS(x)
%     N = 5;
%     M = 3;
global M;
global N;
global MaxRate;
%global isBoth;


%global isAddError;
%global errorRate;
%global errorDirection;

%global isThroughputScaling;

global isOnOff;
global onOffMap;
%global timeStamp;

%expand the vector in columns, M columns
if isOnOff
    onOffM = repmat(onOffMap, 1, M);
    x = x & onOffM;
end

rateV = calcRawRates(x, MaxRate);

%    if (isAddError)
%        rateV = rateV .* (ones(N, M) + errorDirection * errorRate);
%    end

%disp(rateV);

f=0;
%This code works for both integral and fractional association

%TODO: we may be able to remove the for loops here, and may remove the assert by
%a flag?
for i=1:N
  if isOnOff 
   if onOffMap(i, 1) ==0
     continue;
   end
  end
    ii = find(x(i,:)>0);
    if (isempty(ii))
        f = 1e100;
        return;
    end
    for j=1:length(ii)
        %if rateV ==0, will get a utility of 0, just make sure all the
        %rates are >>0 if connected
        %if (x(i, j)>0 && rateV(i, j) >0) 
        if (rateV(i, ii(j)) >0)  
            %it is safer to write it this way instead of putting the x(i,j)
            %inside the log 
                %f = f - log(rateV(i, ii(j)));
                f = f - log(rateV(i, ii(j)) * x(i, ii(j)));
%                 disp('rateV=')
%                 disp(rateV(i, ii(j)));
%                 disp('x')
%                 disp(x(i, ii(j)))
%                 j
        else
                f = 1e100;
                return;
        end
            %realf = realf - log(realV(i, j));
    end
end

%     assert(~isinf(f));
% f =-log(f);
end

