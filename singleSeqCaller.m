function singleSeqCaller(dR, isOpt, steps, isBF)
    %isOpt = 0;
    %dR = 0.8; deployment ratio
    %steps = 256;
    mostOut = tic
    global isOnOff;
    isOnOff = 0

    global wifiPos
    global isBF;
    wifiPos = 2/3
    %isBF = 1
  global isRunRandom;
  isRunRandom = 1;
  global N;
  global M;
 global lteRadius;
lteRadius = 1;

 global isWiFiPF;
isWiFiPF = 1

    bfString = int2str(isBF)

    initWThroughNew();
outputFolderName = "";
 inputFolderName = "";

    %parpool(4)
    %parfor i = 1:steps
    for i = 1:steps
        [outputFolderName, inputFolderName] = mainPrintAndCallerFixedMCS(i, dR, isOpt);
    end

    toc(mostOut)

    othertime = tic
    fileID = fopen(strcat(outputFolderName, 'outputFolderName.txt'), 'w');
    fprintf(fileID, '%s', outputFolderName);
    fclose(fileID);

    fileID = fopen(strcat(outputFolderName, 'inputFolderName.txt'), 'w');
    fprintf(fileID, '%s', inputFolderName);
    fclose(fileID);
    % disp(outputFolderName);
    system(['python', ' ', 'remote-back.py', ' ', outputFolderName], '-echo');
    system(['python', ' ', 'drawTopoForApo.py', ' ', outputFolderName], '-echo')
    system(['Rscript', ' ', '--vanilla', ' ', 'concept-line-one-case.R', ' ', outputFolderName, ' ', bfString ], '-echo')

    system(['Rscript', ' ', '--vanilla', ' ', 'flowLevelStat.R', ' ', outputFolderName,...
        ' ', int2str(N),...
        ' ', int2str(M),...
        ' ', int2str(steps),...
        ' ', int2str(isBF)...
        ], '-echo')

    if dR == 1 && isOpt == 0
        baseFolder = outputFolderName
        system(['cat', ' ', strcat(baseFolder, '*_wifiMcs.txt'), ' ', '>', ' ', strcat(baseFolder, 'Mcs_wifi.txt')], '-echo')
        system(['cat', ' ', strcat(baseFolder, '*_lteMcs.txt'), ' ', '>', ' ', strcat(baseFolder, 'Mcs_lte.txt')], '-echo')
        system(['Rscript', ' ', '--vanilla', ' ', 'drawMcs_once.R', ' ', outputFolderName], '-echo')

    end
    aa = strsplit(outputFolderName, "/")
    pf = strcat("/"+aa(2) + "/" +aa(3) + "/" + aa(4))
    cd(pf)
    pathS = strcat(aa(5), '.tar.bz2')
    system(['tar', ' ', '-jcf', ' ', char(pathS), ' ', char(aa(5))], '-echo')
    toc(othertime)
    %cd(outputFolderName)
    %how to split the path and get 5-3-...
    %cd


    exit
end
