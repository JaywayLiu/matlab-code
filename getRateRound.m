function f = getRateRound(x, i, j)
global N;
if j==1
    bb=sum(round(x) ,1);
    if bb(j) >0
        f =  getRateFromDist(i, j) * round(x(i,j)) * (1/ bb(j));
    else
        %if no one connects, rate is 0?  may have inf?
        f=0;
    end
    
else
    sumR = 0;
    for i = 1:N
        rateI = getRateFromDist(i, j);
        if rateI>0.0000000000001
            sumR = sumR +1/rateI * round(x(i, j));
        end
    end
    if sumR<0.00000000000000001
        f = 0;
    else
        f = 1/sumR * round(x(i, j));
    end
end
end
