function initAPLocations(dataFolder)
global locationInfo;

lteFileName = strcat(dataFolder, '/lte_centers.txt');
locationInfo.lteLocs = load(lteFileName)';

wifiFileName = strcat(dataFolder, '/wifi_centers.txt');
locationInfo.wifiLocs = load(wifiFileName)';

end