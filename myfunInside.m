function f = myfunInside(x)
%     N = 5;
%     M = 3;
global M
global N
% global rateV
% global r
%disp('M=')
%disp(M)
f=0;
%rateTmp = 0;


for i=1:N
    for j=1:M
        %1 means the first dimention here, vertical
        rateTmp = getRate(x, i, j);
        if rateTmp >0
            f = f - log(rateTmp)* x(i, j);
        end
    end
end
end