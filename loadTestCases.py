import csv
import numpy
import os
import shlex
import subprocess
import socket

h = socket.gethostname()

if h[0: 6] == "apollo" or h[0:5] == "ullab":
    isPa = 2
else:
    isPa = 1
print("isPa =" + str(isPa))

# toTest = "Kappa"
# toTest = "cCircle-Pb"
toTest = "tCircle-Pb"

toTest = "Deploy-Ratio"

print(toTest)
#mo_source = "mcs_6-4-256-667_20191013T111109"
#mo_source = "mcs_8-4-16384-667_20191026T201106"
#mo_source = "mcs_32-4-16384-667_20191010T120845"
#mo_source = "mcs_32-4-16384_cCluster-Pb_45";
#mo_source = "mcs_32-4-16384_tCluster-Pb"

#005 should be the one with uniform
mo_source = "mcs_32-4-16384-667_20191017T222005"
#mo_source = 'mcs_32-4-16384-667_20191102T172358_cluster-circ'

print(mo_source)

if isPa == 2:
    dest_folder = "/users/jianwel/mcsDataOutput/"
else:
    dest_folder = "/home/jianwel/mcsDataOutput/"

print(dest_folder)

toTexFolder = "toTex"


def createFolderForOnePara(prefix, paraName, paraValues):
    l = []
    for vString in paraValues:
        fString = paraName+"="+vString
        if prefix != None:
            fString = prefix + "/" + fString
        l.append(fString)
        checkFolder(fString)
    return l


def checkFolder(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


fileName = "input_data/testCases.txt"
testCaseDict = {}
with open(fileName, "r") as tc:
    for line in tc:  # iterate over each line
        testCaseName, paraList = line.split(' : ')
        print(testCaseName)
        print(paraList.split())
        testCaseDict[testCaseName] = paraList.split()

# for key, v in testCaseDict.items():
#     print(key)
#     print(v)
paraFileName = "input_data/params.txt"

paraDict = {}

with open(paraFileName, "r") as pf:
    for line in pf:
        paraName = line.rstrip()
        print(paraName)
        vFile = "input_data/paraValues/"+paraName+".txt"
        # a = pandas.read_csv(vFile, sep=",", )
        # print(a)
        # l = .values.tolist()
        # print(l)
        with open(vFile, "r") as vf:
            a = csv.reader(vf)
            c = list(a)
            b = [i.strip() for i in c[0]]
            paraDict[paraName] = b


seqMax = [len(paraDict[i]) for i in testCaseDict[toTest]]

N = numpy.prod(seqMax)
print(N)


#################################################
# create the folders

outputFolder = dest_folder + mo_source
checkFolder(outputFolder)

outputFolder += "/" + toTest
checkFolder(outputFolder)

print("outputfolder = "+outputFolder)
os.chdir(outputFolder)


paraName = testCaseDict[toTest][0]
paraValues = paraDict[paraName]
l1Folders = createFolderForOnePara(None, paraName, paraValues)
baseFolders = l1Folders

if len(testCaseDict[toTest]) == 2:
    paraNameL2 = testCaseDict[toTest][1]
    paraValuesL2 = paraDict[paraNameL2]
    l2List = []
    for ff in l1Folders:
        # os.chdir(ff)
        oneL2 = createFolderForOnePara(ff, paraNameL2, paraValuesL2)
        l2List.extend(oneL2)
        # os.chdir("..")
    baseFolders = l2List

tarFolders = [toTest + "/" + ii + "/" + toTexFolder for ii in baseFolders]
tarString = " ".join(tarFolders)

moreFolders = [toTest + "/" + ii + "/" + toTexFolder for ii in baseFolders] + \
                [toTest + "/" + ii + "/" + "stat" for ii in baseFolders] +\
                [toTest + "/" + ii + "/" + "minList" for ii in baseFolders] +\
                [toTest + "/" + ii + "/" + "maxList" for ii in baseFolders] +\
                [toTest + "/" + ii + "/" + "all.txt" for ii in baseFolders]

firstOne = toTest + "/" + baseFolders[0] + "/"

mcsS = ["Mcs_lte.txt", "Mcs_wifi.txt"]
mcsFiles= [firstOne + ii for ii in mcsS]
moreFolders += mcsFiles

moreFolderString = " ".join(moreFolders)
#################################################
if isPa == 2:
    start = 3
else:
    start = 1
end = start + N
#runRange = range(start, end)

if isPa == 2:
    baseName = "apollo%02d"
else:
    baseName = "babbage%d"
#compName = [baseName%(i) for i in runRange]
plist = []
for ii in range(1, N+1):
    # for ii in range(1, 1+1):
    compName = baseName % (start+ii-1)
    #command = """ssh -t %s 'cd ~/matlab-code && matlab -nojvm -nodisplay -nosplash -r "singleSeqCaller(%f, %d, %d)" | tee logs/seq-%d-%.3f-%d.log' """%(compName, jj, ii, nSteps, ii, jj, nSteps)
    print("""ssh -t %s 'cd ~/matlab-code && matlab -nojvm -nodisplay -nosplash -r "wrapperCaller('\\''%s'\\'', '\\''%s'\\'', %d,  '\\''%s'\\''); exit;" 2>&1 > logs/%s-%s-%d.log' """ %
          (compName, toTest, mo_source, ii, toTexFolder, toTest, mo_source, ii))
    command = """ssh -t %s 'cd ~/matlab-code && matlab -nojvm -nodisplay -nosplash -r "wrapperCaller('\\''%s'\\'', '\\''%s'\\'', %d,  '\\''%s'\\''); exit;" 2>&1 > logs/%s-%s-%d.log' """ % (
        compName, toTest, mo_source, ii, toTexFolder, toTest, mo_source, ii)
    args = shlex.split(command)
#        print(command)
    print(args)
    # os.system(command)
    p = subprocess.Popen(args)
    plist.append(p)

exit_codes = [t.wait() for t in plist]
print(exit_codes)

with open('exit_code.txt', 'w') as filehandle:
    filehandle.writelines("%d\n" % p for p in exit_codes)

os.chdir("..")


print("Rscript --vanilla ~/matlab-code/summaryAcrossSplit.R `pwd` "+toTest)
os.system("Rscript --vanilla ~/matlab-code/summaryAcrossSplit.R `pwd` "+toTest)


tarString = tarString+" "+toTest+"/figs"
moreFolderString= moreFolderString+" "+toTest+"/figs"
#os.system("tar -jxf %s.tar.bz2 %s"%(toTest, tarString))
print("tar -jcf %s.tar.bz2 %s" % (toTest, tarString))
os.system("tar -jcf %s.tar.bz2 %s" % (toTest, tarString))

print("tar -jcf %s.tar.bz2 %s" % (toTest+"-more", moreFolderString))
os.system("tar -jcf %s.tar.bz2 %s" % (toTest+"-more", moreFolderString))


os.chdir("..")
# print("tar -jcf %s.tar.bz2 %s" % (mo_source, mo_source))
# os.system("tar -jcf %s.tar.bz2 %s" % (mo_source, mo_source))
