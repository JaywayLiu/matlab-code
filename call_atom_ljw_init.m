%function [currentV, x] = call_atom_ljw_init()
    global M;
    global N;
    global MaxRate;
    global lteRadius;
    lteRadius = 1;

     M = 4;
     N = 12;

%     M = 3;
%     N = 5;

    %fixMaxRate();

    

    initWThroughNew();
    %fullDataFolder = "mcs_test_atom_diff"
    fullDataFolder = "mcs_test_lg_12-4_diff"
    initAPLocations(fullDataFolder);
    initUELocations(fullDataFolder, 1);
    
    mcsIndexFromDist();
    %mcsIndexFromDistFiles(dataFolder, timeStampPassIn);         
    MaxRate = calcMaxRateTable()


    global isOnOff;

    isOnOff = 0;


   % [currentV, atomx] = atom_ljw_init(MaxRate)
 atomx = policyGenMCS_equal(MaxRate)
    newV = myfunInsideS(atomx)

    [res_atom_raw, rate_atom_raw] = calcRates(atomx);

    rate_atom_raw
rateV_atom = sum(rate_atom_raw, 2);
resV_atom = sum(res_atom_raw, 2);

[~, resF_atom] = sumTFromTArray(resV_atom);
%[atomT, atomJ] = sumT(atomx);
[atomT, atomJ] = sumTFromTArray(rateV_atom)

%end
