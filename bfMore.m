function [minV, minx] = bfMore()
global N;
global M;
policy = zeros(1, N);
%the value range in policy [0, M-1], will add 1 whenever indexing
minV = 1e10000;

%simulate plus one operation with bits reversed

%[0 - M^N-1], but for the last iteration, will not do the addOneMore
for k=1: M^N
    x = zeros(N, M);
    
    %translate the policy to x
    for i= 1:N
            x(i, policy(1, i) +1) =1;
    end
    
    v= myfunInsideS(x);
    if v< minV
        minV= v;
        minx = x;
    end
    
    if (k==M^N)
        break;
    end

    %0 means lte  1 means wifi
    policy = addOneMore(policy, 1);
end
end


