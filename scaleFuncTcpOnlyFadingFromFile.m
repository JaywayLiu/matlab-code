function f = scaleFuncTcpOnlyFadingFromFile(x)
  %global ShannonCapacity;
  %global TCPGoodput;
  M = csvread('allResults-more-fading.txt');
  ShannonCapacity = M(:, 11);
  TCPGoodput = M(:, 9);
  f = norm((x.*ShannonCapacity) - TCPGoodput);
end