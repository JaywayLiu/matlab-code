#import glob
import os
import time


N = 32
M = 4

maxStep = 256

outputName = "backup/all-%04d.txt"%maxStep
outputFile = open("backup/all-%04d.txt"%maxStep,"w")

#outputFile.write('start.va start.ta start.ja start.vstd start.tstd start.jstd '+
#'policy.v policy.t policy.j '+
#'bf.v bf.t bf.j '+
#'int.v int.t int.j '+
#'frac.v frac.t frac.j '+
#'atom.v atom.t atom.j'+'\n');

outputFile.write('start.va start.ta start.ja start.vstd start.tstd start.jstd '+
'policy.v policy.t policy.j '+
'bf.v bf.t bf.j '+
'int.v int.t int.j '+
'frac.v frac.t frac.j '+
'atom.v atom.t atom.j '+
'gg.v gg.t gg.j ' +
      'random.v random.t random.j '+
      'pe.v pe.t pe.j '+
      'policy.nj bf.nj int.nj sol.nj atom.nj gg.nj random.nj pe.nj'+
'\n');

#time.sleep(0.05);
outputFile.close();

for i in range(1, maxStep+1):
  fileName = '%d-%d-test%d-both.txt'%(N, M, i) 
  print('cat %s >> %s'%(fileName, outputName))
  os.system('cat %s >> %s'%(fileName, outputName))
  time.sleep(0.005);

