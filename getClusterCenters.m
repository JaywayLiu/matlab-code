function m_centers = getClusterCenters(wifiPos, angleIn)

m_centers(1, 1) = wifiPos;
m_centers(1, 2) = 0;


anglePi = angleIn / 180 *  pi;
m_centers(2, 1) = wifiPos .* cos(anglePi);
m_centers(2, 2) = wifiPos .* sin(anglePi);

m_centers(3, 1) = wifiPos .* cos(-anglePi);
m_centers(3, 2) = wifiPos .* sin(-anglePi);



end


