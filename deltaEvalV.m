function newVDelta = deltaEvalV(sys, flowID, kk)
    global MaxRate;
    global onOffMap;
    global nMethods;

    preOnOffMap = onOffMap;
    preOnOffMap(flowID, 1) = 0;

    old_sol = sys.cur_solutions(:, kk, nMethods) & preOnOffMap;
    nOld = length(find(old_sol == 1))

    if (kk == 1)

        if (nOld ~=0)
        log(MaxRate(flowID, kk) / (nOld + 1))
            newVDelta = log(MaxRate(flowID, kk) / (nOld + 1)) + nOld * log(nOld / (nOld + 1));
        else
            newVDelta = log(MaxRate(flowID, kk));
        end

    else

        if (nOld ~= 0)
            wifiRates = sys.cur_rates(:, kk);
            posRates = wifiRates(wifiRates > 0)
            oldR = 1 / posRates(1, 1)
            %assert(oldR > 0);
            newR = oldR + 1 / MaxRate(flowID, kk);
            newVDelta = (nOld + 1) * log(1/newR) - nOld * log(1/oldR); 
        else
            newVDelta = log(MaxRate(flowID, kk));
        end
    end
    % assert(~isinf(newVDelta));
    % assert(~isnan(newVDelta));

end