function f= getRateFromDistOld(i, j)
global disToBs;
global tMap;
if j==1
    f = 3.999521;
else
    dist = disToBs{1, j}(i);
    if dist <= 70
        f= 10;
    elseif dist >70 && dist <125
        lower = floor(dist /5) *5;
        upper = ceil(dist/5) *5;
        w = (dist - lower) /5;
        %disp(lower);
        %disp(upper);
        f = w* tMap(upper) + (1-w)*tMap(lower);
    elseif dist >= 125
        f = 0;
    end
    
end
end