%function genMultiScenes(testName)
testName = 'cCluster-Pb'
testName = 'tCluster-Pb'

testCases = containers.Map('KeyType', 'char', 'ValueType', 'any');
testCases('cCluster-Pb') = (0:0.1:1);
testCases('tCluster-Pb') = (0:0.1:1);
%testCases('tCluster-Pb') = [0];

strCells = strsplit(testName, '-')
paraName = strCells{1, 2}

genType = "normal"
genType = "cluster-circ"
genType = "cluster-tshape"

%rotateD = 60
rotateD = 45

nSteps = 16384;

nUE = 32;
nAP = 3;
globalP = 0;

testNameOut = testName

% if ~strcmp(genType, 'normal')
%     folderName = strcat(testNameOut, '_', genType)
% end

if strcmp(testName(1:8), "cCluster")
    testNameOut = strcat(testNameOut, '_', int2str(rotateD))
end

testNameOut = strcat('mcs_', sprintf('%d-%d-%d_', nUE, nAP+1, nSteps), testNameOut)

mkdir(testNameOut)
addpath .
cd(testNameOut)

list = testCases(testName);

for i = 1:length(list)
    subName = strcat(paraName, "=", num2str(list(i)));
    globalP = list(i)
    genOneSceneForMulti(genType, nSteps, nUE, nAP, globalP, subName, rotateD);

end

cd("..")

%end
