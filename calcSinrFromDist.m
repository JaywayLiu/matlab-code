function [sinr, dist] = calcSinrFromDist(p1, p2, alpha, fadingExp)
 dist = norm(p1 - p2);
 sinr = alpha / (dist ^ fadingExp);  
end