function [pos, clusterCenters] = genCircClustersRandom(nCluster, nUE, globalP, nSteps, clusterPos, rangeRadius, angleIn, baseClusterCenter)


    pos = zeros(2, nUE, nSteps);

    randType = 'uniform';

    % outputName = strcat("multi_cluster", "circ", int2str(angleIn))
    % %checkDir(outputName)

    %nCluster does not count the global one
    p = (1 - globalP) / nCluster;
    pV = ones(1, nCluster + 1) .* p;
    pV(1, 1) = globalP;

    sump = 0;

    for i = 1:length(pV)
        sump = sump + pV(i);
        pV(i) = sump;
    end

    %ueCell = {}
    clusterCenters = getClusterCenters(clusterPos, angleIn);

    for j = 1:nSteps

        nUEArray = simuPlacement(pV, nUE);

        posOneRun = generateRandomPlacementInCircleRandom(nUEArray(nCluster + 1), clusterCenters(nCluster, :), rangeRadius, randType);

        for i = nCluster:-1:1

            if i ~= 1
                cCenter = clusterCenters(i - 1, :);
                rr = rangeRadius;
            else
                cCenter = baseClusterCenter;
                rr = 1;
            end

            subpos = generateRandomPlacementInCircleRandom(nUEArray(i), cCenter, rr, randType);
            posOneRun = cat(2, posOneRun, subpos);

        end

        pos(:, :, j) = posOneRun;

    end

end
