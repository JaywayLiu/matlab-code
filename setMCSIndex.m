function setMCSIndex()
global N;
global M;

global lteMCSIndex;
global wifiMCSIndex;
%initMCSTables();

%N = 20;

%nSteps = 500;

lteMCSIndexR = [0 2 3 0 2 0 0 3 1 2 0 0]';
%lteMCSIndexR = [0 2 3 0 2]';

lteMCSIndex = lteMCSIndexR + ones(size(lteMCSIndexR));


%  wifiIndexR = [4 8 1 0 6 1 0 0 5 7 3 8;
%  2 8 3 4 7 8 0 8 5 7 0 2]';

wifiIndexR = [4 8 1 0 6 1 0 0 5 7 3 8;
 2 8 3 4 7 8 0 8 5 7 0 2;
 0 0 0 4 6 1 5 2 0 7 2 6]';


% wifiIndexR= [ 1 0 7 4 6;
%  0 1 4 8 1]';


wifiMCSIndex = wifiIndexR + ones(size(wifiIndexR));
sizeLte = size(lteMCSIndex);
sizeWiFi = size(wifiMCSIndex);


N = sizeWiFi(1);
M = sizeWiFi(2) + sizeLte(2);

% lteMCSIndex = [1; 3; 4;1;3];
% wifiMCSIndex= [2, 1,8,5,7;
%                1, 2, 5,9,2]';
end