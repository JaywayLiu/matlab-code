function drawOneSpotSetAngle(wifiRadius, wifiPos, isVis, angleIn, RangeLimit)

global v_edge;
global h_edge;

v_edge = 15;
h_edge = 15;
%wifiRadius = 0.625;

m_centers(1, 1) = wifiPos;
m_centers(1, 2) = 0;


anglePi = angleIn / 180 *  pi;
m_centers(2, 1) = wifiPos .* cos(anglePi);
m_centers(2, 2) = wifiPos .* sin(anglePi);

m_centers(3, 1) = wifiPos .* cos(-anglePi);
m_centers(3, 2) = wifiPos .* sin(-anglePi);



if (isVis)
    
clf
%saveas(gcf,'wifi-pos.pdf')
axis equal
axis([-RangeLimit RangeLimit -RangeLimit RangeLimit]);
xt = get(gca, 'XTick');
set(gca, 'FontSize', 14)


ax = gca;

% outerpos = ax.OuterPosition;
% ti = ax.TightInset; 
% left = outerpos(1) + ti(1);
% bottom = outerpos(2) + ti(2);
% ax_width = outerpos(3) - ti(1) - ti(3);
% ax_height = outerpos(4) - ti(2) - ti(4);
% ax.Position = [left bottom ax_width ax_height];

colors = {'b','r','g','y','k'};

viscircles(ax, [0,0], RangeLimit , 'Color', 'k' , 'LineWidth', 2, 'LineStyle', ':');
viscircles(ax, [0,0], 0.01 , 'Color', 'k', 'LineWidth', 3);

for k=1:nWiFiAP
viscircles(ax, m_centers(k, :),  wifiRadius, 'Color', colors{k}, 'LineWidth', 4);
viscircles(ax, m_centers(k, :), 0.01, 'Color', colors{k}, 'LineWidth', 3);

%rectangle('Position',[0 0 xlim ylim]);
pause(1);
end


end %end if(isVis)
csvwrite('wifi_centers.txt', m_centers);
%print('oneSpot_topo_norm','-dpng')

end


