function xa = policyGenMCS_stick(pre_x)
%will stick to the old wifi ap if not broken
global M;
global N;
global lteMCSIndex;
global wifiMCSIndex;

%only wifi's distance, so -1
xa = zeros(N, M);
for i=1: N
    pre_index = find(pre_x(i,:));
    %assert(size(pre_index)==1);
    if wifiMCSIndex(i, pre_index-1) >0
        xa(i, pre_index)  =1;
    else
        %[maxT, index] = max(wifiMCSIndex(i, setdiff(2:M, pre_index)));
        [maxT, index] = max(wifiMCSIndex(i, 1:M-1));
        %if max is still pre_index, then the mcs is 0, will go to else
        %below
        if maxT >0
         xa(i, index+1)  =1;
        else
          xa(i, 1)  =1;  
        end
    end
    

end