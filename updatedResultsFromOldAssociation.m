function [startArray, policyV, policyT, policyJ, ...
    bfV, bfT, bfJ, ...
    intV, intT, intJ, ...
    finalV, solT, solJ,...
    atomV, atomT, atomJ] = updatedResultsFromOldAssociation(xArray)

startxList = xArray{1, 1};
sizeRandom = size(startxList);

startArray  =zeros(sizeRandom(2), 3);

for i=1:sizeRandom(2)
    startArray(i, 1) = myfunInsideS(startxList{1, i});
    [startArray(i, 2),  startArray(i, 3)] = sumT(startxList{1, i});
    
end
policyV = myfunInsideS(xArray{1, 2});

[policyT, policyJ] = sumT(xArray{1, 2});

bfV = myfunInsideS(xArray{1, 3});
[bfT, bfJ] = sumT(xArray{1, 3});

intV =  myfunInsideS(xArray{1, 4});
[intT, intJ] = sumT(xArray{1, 4});

finalV =  myfunInsideS(xArray{1, 5});
[solT, solJ] = sumT(xArray{1, 5});

atomV =  myfunInsideS(xArray{1, 6});
[atomT, atomJ] = sumT(xArray{1, 6});

end



