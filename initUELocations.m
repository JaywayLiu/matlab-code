function initUELocations(dataFolder, runIndex)
global locationInfo;

filename = strcat(dataFolder, sprintf('/mobility_%06d.txt', runIndex));
locationInfo.ueLocs = load(filename);


end