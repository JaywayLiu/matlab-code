%function for M>1
function f = addOneMore(array, pos)
global M;
if array(1, pos) <M-1
    array(1, pos) = array(1, pos) + 1;
else
    array(1, pos) = 0;
    array = addOneMore(array, pos+1);
end
f = array;
end