function paM = loadParamsTestCases()
    paM = containers.Map('KeyType','char','ValueType', 'any')
    paraFile = 'input_data/params.txt';
    valueFolder = 'input_data/paraValues/';
    fid = fopen(paraFile, 'r');
    %[nr, sCell, ~] = xlsread(paraFile);
    txt = textscan(fid, '%s', 'delimiter', '\n');
    %values = cell(1, length(txt{1}))
    fclose(fid)

    for i = 1:length(txt{1})
        paraName =  strtrim(txt{1}{i});
        valueFileName = strcat(valueFolder, paraName, '.txt');
        disp(valueFileName)
        fd = fopen(valueFileName);
        paraVCell = textscan(fd, '%s', 'delimiter', ',');
        index = getDefaultParamIndex(paraName, paraVCell{1});
        paM(paraName) = Param(paraVCell{1}, index);

    end

    % k = keys(paM) ;
    % val = values(paM) ;
    % for i = 1:length(paM)
    %     [k{i} val{i}.values val{i}.toUseIndex]
    % end
end
