function [resV, rateV] = calcRates(x)
    % resV - returns the amount of resource (normalized, add to 1)
    % rateV - is the estimated throughput
    global M;
    global N;
    global isOnOff;
    global onOffMap;
    global MaxRate;
    %global isBoth;
    %global isAddError;
    %global errorRate;
    %global errorDirection;
    %

    resV = zeros(N, M);

    bb = sum(x, 1);
    %disp('sizeof x=');
    %disp(size(x));
    %disp(N);
    %disp(M);

    if isOnOff
        onOffM = repmat(onOffMap, 1, M);
        x = x & onOffM;
    end

    rateV = calcRawRates(x, MaxRate);
%
%    for j = 1:M
%
%        if bb(1, j) == 0
%		
%            continue;
%        end
%
%            if j == 1
%                resV(:, j) = x(:, j) .* (1 / bb(1, j));
%            else
%                newSumR = calcSumRInverse(MaxRate(:, j) .* x(:, j));
%
%                for i = 1:N
%
%                    if x(i, j) > 0 && MaxRate(i, j) > 0
%                        resV(i, j) = (newSumR / MaxRate(i, j));
%                    end
%
%                end
%
%            end
%
%        end

        % for i=1:N
        %     for j=1:M
        %         if isBoth
        %             if j==1
        %                 if x(i, j) >0
        %                    resV(i,j) =1/bb(j) *x(i,j);
        %                 end
        %             else
        %                 if x(i,j)>0 && rateV(i, j)>0
        %                    resV(i,j) = 1/rateV(i, j) * x(i,j);
        %                 end
        %             end
        %         else
        %                 if x(i, j) >0
        %                    resV(i,j) =1/bb(j) *x(i,j);
        %                 end
        %         end
        %     end
        % end

        % if (isAddError)
        %     rateV = (rateV ) .* (ones(N, M) + errorDirection * errorRate);
        % end

    end
