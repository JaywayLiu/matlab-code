classdef Param < handle

    properties
        %this is a list of values
        values;
        toUseIndex;
        %descript;

    end

    methods

        function obj = Param(values, toUseIndex)
            obj.values = values;
            obj.toUseIndex = toUseIndex;
        end
    end


end
