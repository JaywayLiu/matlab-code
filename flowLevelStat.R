library(psych)
library(xtable)
split_path <- function(x) if (dirname(x)==x) x else c(basename(x),split_path(dirname(x)))
whichmedian <- function(x) which.min(abs(x - median(x)))

args = commandArgs(trailingOnly=TRUE)

insideDir = args[1]  
nUE = as.integer(args[2])
nAP = as.integer(args[3])
nSteps = as.integer(args[4])
isBF = as.integer(args[5])

# isBF = 1

# ###############
# insideDir = '/home/jianwel/mcsDataOutput/mcs_6-4-256-667_20191013T111109/opt=0/1.0000'
# nUE = 6
# nAP = 4
# nSteps = 256
# ###############


# ###############
# insideDir = '/home/jianwel/mcsDataOutput/mcs_12-4-16384-667_20191010T100942-bf/opt=0/1.0000'
# nUE = 12
# nAP = 4
# nSteps = 16384
# ###############

subDir = "flowStat"
print(file.path(insideDir, subDir))
dir.create(file.path(insideDir, subDir), showWarnings = FALSE)

outputFolder = paste(insideDir, subDir, sep="/")
print(paste(insideDir, subDir, sep="/"))

#setwd(outputFolder)




methods = c('policy', 'pe', 'atom', 'gg', 'gr')
output_methods = c('lgw', 'lge', 'atom', 'gg', 'rand')
#output_methods = methods
methods = output_methods

if (isBF) {
 
  methods = c(methods, 'opt')
  output_methods = c(output_methods, 'opt')
}
nMethod = length(methods)
#i, j, k
# k is the method index
# i is the UE index
# j is the step index

da = array(dim = c(nUE, nSteps, nMethod))

k = 1
for (methodS in methods)
{
  for(j in c(1:nSteps)) {
    filename = sprintf('%d-%d-%06d-both_%s_rates.txt', nUE, nAP, j, methodS)
    filename = paste(insideDir, filename, sep='/')
    #print(filename)
    aa = read.csv(filename, sep=",", header = FALSE)
    #print(aa)
    da[, j, k] = as.matrix(aa)
  }
  k = k+1
}

thresh = 0
baseMs = c('lgw', 'lge')

threshMetrics = c('$P_{greater}$',  '$P_{equal}$', '$P_{less}$');
#threshMetrics = c('$P_{greater}$',  '$P_{equal}$', '$P_{less}$');
threshOutName= c('greater',  'equal', 'smaller');
#threshMetrics = c('$P_{greater}$',  '$P_{le}$');

dfMatrix = array(list(), dim=c(length(baseMs)))
compArray =  array(dim=c(nUE, nSteps, nMethod, length(baseMs)))

pvG= vector()
pvE= vector()
pvS= vector()




#df = data.frame(nrow=nMethod, ncol=length(statArray))
#colnames(df) = c('mean', 'std', 'median', 'max', 'min')
#rownames(df) = methods

testCaseName = "bf"
testCaseName = "cCluster-Baseline"

for (l in c(1:length(baseMs)))
{
  for (k in c(1:length(methods)))
  {

    compArray[, , k, l] = (da[, , k] - da[, , l]) / da[, , l]
    for (i in c(1:nUE))
    {
      pGreater = length(which(compArray[i, , k, l] > thresh)) / nSteps * 100
      #print(pGreater)
      pEqual = length(which(compArray[i, , k, l] == thresh)) / nSteps * 100
      pSmaller = length(which(compArray[i, , k, l] < thresh)) / nSteps * 100
      pvG[i] = pGreater
      pvE[i] = pEqual
      pvS[i] = pSmaller
    }
    
    meanG = mean(pvG)
    meanE = mean(pvE)
    meanS = mean(pvS)
    
    oneRow = c(meanG, meanE, meanS)


    dfMatrix[[l]] = rbind(dfMatrix[[l]], oneRow)
    #df[methods[k],] = basicSumG
   # dfE = data.frame()
   # dfE$methods[k] = basicSumE
    
  }
  rownames(dfMatrix[[l]]) = output_methods
  colnames(dfMatrix[[l]]) = threshMetrics
  
  baseName = baseMs[l]
  outfileName = paste('compare', 'with', baseName,  sep="_" )
  #dfMatrix[[l, 1]] = as.data.frame(gList)
      print(xtable(dfMatrix[[l]], type = "latex",
        caption = paste0("Flow level comparison over ", baseName)),
         label=paste0("tab::", testCaseName, "-flow-level-", baseName), 
        file = paste(outputFolder, '/', outfileName, ".tex", sep=""), caption.placement = 'top', 
        sanitize.text.function=function(x){x}
        )


}

# i=1
# for (baseName in baseMs) 
# {
# j=1
#   for (gle in threshOutName)
#   {
#     outfileName = paste('compare', 'with', baseName, 'percentage', gle, sep="_" )
#     outfileName = paste(outputFolder, '/', outfileName, '.txt', sep="")
#     write.csv(dfMatrix[[i, j]], outfileName)
#     print(xtable(dfMatrix[[i, j]], type = "latex",
#         caption = paste("Flow level comparison over", baseName,  ")", sep="")),
#         file = paste(outfileName, ".tex", sep=""), caption.placement = 'top')
#     j = j +1
#   }
#   i = i +1
# }



#compArray
#l is the base method, 1 pe 2 policy
#k method 
#j step index
#i UE index 

# for every i, an k, l, count for the j, and then save it to 
# a vector of size N
#then draw CDF for that vector (need for every k, l)
 
#describe the vector and save it to a dataframe

#nameFigs = sprintf("%s-%s-%.2d.pdf", methodS, baseMethod, thresh

# if base is 0, it will be inf, will remove all the inf 
