function [nv, xtemp, roundTimeJTemp] = moveEvalPartial(i, j, MaxRate, newV, newX, nLte, nLteAdd, roundTimeJ)
    


    xtemp = newX;
    xtemp(i, 1) = 0;
    xtemp(i, j) = 1;

    if nLteAdd > 1
        lteDelta = (nLte - 1) * log((nLteAdd) / (nLteAdd - 1)) - log(MaxRate(i, 1) / nLteAdd);
        %it is sured i is on LTE, so at least one on LTE, if nLTEAdd ==1, 
        %it means nLte ==0
    elseif nLteAdd == 1
        lteDelta = -log(MaxRate(i, 1));
    end

    roundTimeJTemp = (roundTimeJ + 1 / MaxRate(i, j));
    ins = find(newX(:, j) > 0);
    K = length(ins);


    if roundTimeJ ~= 0
        wifiDelta = (K + 1) * log(1 / roundTimeJTemp) - K * log(1 / roundTimeJ);
    else
        wifiDelta = log(MaxRate(i, j));
    end

    nv = newV + lteDelta + wifiDelta;

    % debugNv =  -myfunInsideS(xtemp)
    % if abs(debugNv) < 100
    %     xtemp
    %   assert(abs(nv - debugNv)<0.001);
    % end

end
