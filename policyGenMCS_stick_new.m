function xa = policyGenMCS_stick_new(pre_x, MaxRate)
%will stick to the old wifi ap if not broken
global M;
global N;

%only wifi's distance, so -1
xa = pre_x;
for i=1: N
    pre_index = find(pre_x(i,:));
    %assert(size(pre_index)==1);
    if pre_index == 1 || MaxRate(i, pre_index) == 0
        %[maxT, index] = max(wifiMCSIndex(i, setdiff(2:M, pre_index)));
        [maxT, index] = max(MaxRate(i, 2:M));
        %if max is still pre_index, then the mcs is 0, will go to else
        %below
        if maxT >0
         xa(i, index+1)  =1;
        else
          xa(i, 1)  =1;  
        end
    end

end