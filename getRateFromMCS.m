function f = getRateFromMCS(i, j)
global lteMCSIndex;
global wifiMCSIndex;
global lteMCSTable;
global wifiMCSTable;

if j==1
    f = lteMCSTable(lteMCSIndex(i, 1));
else
    f = wifiMCSTable(wifiMCSIndex(i, j-1));
end
end
