import csv
import pandas as pd
import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import logging
import matplotlib.lines as mlines
from matplotlib.cm import rainbow

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)



class TopoDrawer:


    def loadLteAPInfo(self):
        with open(self.inputFolder + "/lte_centers.txt") as f:
            l = list(csv.reader(f, delimiter=","))
            logger.debug(l)
            self.lte_centers = [float(i) for i in l[0]]



    def loadWifiAPInfo(self):
        with open(self.inputFolder + "/wifi_centers.txt") as f:
            l = list(csv.reader(f, delimiter=","))


            self.wifi_centers = [[float(l[i][j]) for j in range(len(l[i]))]
                                 for i in range(len(l))]

    def loadOtherInfo(self):
        with open(self.inputFolder + "/scene-config.txt") as f:
            self.nRuns = int(f.readline())
            self.nWiFiAPs, self.nUE = [int(x) for x in f.readline().split()]
            self.wifiRadius = float(f.readline())
            self.wifiPos = float(f.readline())
            self.lteRadius = float(f.readline())

            logger.debug("nRuns = {}".format(self.nRuns))

    def drawAll(self):
        for ii in self.algoList:
            for jj in self.metricList:
                self.drawOneMetric(ii, jj)




    def drawOneMetric(self, algoString, metricString):

        #ax.axis("equal")  # make aspect ratio square

        gg_json = self.findMaxMin(algoString, metricString)

        self.drawList("maxList", gg_json["maxList"], algoString, metricString)
        self.drawList("minList", gg_json["minList"], algoString, metricString)

        #plt.show()


    def drawOne(self, listString, index, ki, algoString, metricString, fig, ax):
        m_string = self.inputFolder + "/mobility_{0:06}.txt".format(index)
        data = pd.read_csv(m_string, header=None)
        dim = data.shape
        fullString = algoString +"_"+metricString

        for i in range(0, dim[1]):
            ax.add_artist(plt.Circle(data.iloc[:, i].tolist(), 0.02, color='k'))





        if (self.isDrawSol):
            solFormat = "{}-{}-{:06}-both_{}_x.txt".format(self.nUE,
                                                                 self.nWiFiAPs+1, index, algoString)
            solFile = self.outputFolder +solFormat
            solFrame = pd.read_csv(solFile, header=None)
            sol = solFrame.values.tolist()
            logger.debug(sol)

            for ii in range(len(sol)):
                for jj in range(len(sol[0])):
                    if sol[ii][jj] == 1:
                        uePos = data.iloc[:, ii].tolist()
                        if (jj==0):
                            apPos = self.lte_centers
                        else:
                            apPos = self.wifi_centers[jj-1]
#                        line = mlines.Line2D(uePos, apPos)
                        #ax.add_line(plt.plot(uePos, apPos, "k."))
                        ax.add_line(mlines.Line2D([uePos[0], apPos[0]],[uePos[1],
                                                                   apPos[1]],
                                                  color=self.colors[jj]))
                        #ax.add_artist(plt.plot(uePos[0], uePos[1], apPos[0],
#                                             apPos[1],
                        #ax.plot(uePos[0], uePos[1], apPos[0],
#                                             apPos[1],
#                                             "k.")
#                        ax.add_line(line)

        fig.savefig(self.outputFolder +listString+'/{}_{:03}_{:06}.{'
                                                  '}'.format(fullString,
                                                             ki, index,
                                                             self.IMAGE_TYPE))
        if (self.isPng):
            pngFolder=self.outputFolder +listString+"/png"

            if not os.path.exists(pngFolder):
                os.makedirs(pngFolder)
            fig.savefig(pngFolder+'/{}_{:03}_{:06}.{'
                                                      '}'.format(algoString,
                                                                 ki, index,
                                                                 "png"))
        plt.close(fig)


    def drawAps(self,  ax):
        lteCircle = plt.Circle(self.lte_centers, self.lteRadius, color='k',
                               fill=False, linestyle="dashed")
        ax.add_artist(lteCircle)
        ax.plot(self.lte_centers[0], self.lte_centers[1], 'bv', markersize=8)

        for pos in self.wifi_centers:
            ax.add_artist(plt.Circle(pos, self.wifiRadius, color='k',
                               fill=False, linestyle="dotted"))
            ax.plot(pos[0], pos[1], 'g^')

    def drawList(self, listString, list, algoString, metricString):
        listFolder= self.outputFolder+listString


        if not os.path.exists(listFolder):
            os.makedirs(listFolder)

        for ki, index in enumerate(list):
            fig, ax = plt.subplots()
            ax.set_aspect(1.0)  # make aspect ratio square
            ax.axis([-1, 1, -1, 1])
            self.drawAps(ax)
            self.drawOne(listString, index, ki, algoString, metricString,
                         fig, ax)


    def findMaxMin(self, algoName, metricName):
        data = pd.read_csv(self.outputFolder + "/all.txt", delimiter=' ' )
        #print(data)
        sorted_data = data.sort_values(by=[algoName+"."+metricName], ascending=False)
        #print(sorted_data)
        topK = sorted_data.head(self.K)["frameN"].tolist()
        print(topK)
        tailK = sorted_data.tail(self.K)["frameN"].tolist()
        print(tailK)
        return {"maxList": topK, "minList": tailK}


    def __init__(self, outputFolder, algoList, metricList, K=1,
                                                            inputFolder=None):

        self.outputFolder = outputFolder
        self.algoList = algoList
        self.metricList = metricList

        if not inputFolder:
            with open(self.outputFolder + "/inputFolderName.txt") as f:
                self.inputFolder = f.read().strip() + "/"
                logger.info("read input name: {intputFolder}")
        else:
            self.inputFolder = inputFolder

        self.loadLteAPInfo()
        self.loadWifiAPInfo()
        self.loadOtherInfo()

        print(self.lte_centers)
        print(self.wifi_centers)

        self.colors = rainbow(np.linspace(0, 1, self.nWiFiAPs+1))

        self.K = K
        self.IMAGE_TYPE = "eps"
        self.isPng = True
        self.isDrawSol = True


if __name__ == "__main__":
    outputFolder = sys.argv[1]
#    outputFolder = "/home/jianwel/mcsDataOutput/dist-equal-alpha-5-3-256-667" \
#                   "-bf/opt=0/1.0000/"

    algoList = ["gg", "atom", "pe", "policy", "bf"]
    metricList = ["v", "t"]

    topo_drawer = TopoDrawer(outputFolder, algoList, metricList, K=3)
    topo_drawer.drawAll()



