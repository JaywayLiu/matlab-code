function testCases =loadTestCases()

testCases = containers.Map('KeyType','char','ValueType', 'any')
fid = fopen('input_data/testCases.txt', 'r');
tline = fgetl(fid);
%testName = ""
%paraListS = ""

i = 1;
while ischar(tline)
   disp(tline);
   %A = sscanf(tline, '%s : %s')
   A = strsplit(tline, ' : ')
   testCases(A{1, 1}) = strsplit(A{1, 2}, ' ') 
   tline = fgetl(fid);
   i = i +1;
end
fclose(fid);

end
