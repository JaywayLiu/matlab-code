function singleSeqCallerParam(paM, testCaseName, paraIndex, inputFolderName, outputFolderName, steps)
    %isOpt = 0;
    %dR = 0.8; deployment ratio
    %steps = 256;
    global N;
    global M;

    global isOnOff;
    isOnOff = 0

    global wifiPos
    wifiPos = 2/3

    global isBF;
    isBF = 0

    global isNotConvex;
    isNotConvex = 1

    %wifi pos should be a variable, wifi pos calc in m
  global isRunRandom;
  isRunRandom = 1;

global isFixMaxRate;
isFixMaxRate = 0;

  
global isDebug;
isDebug = 0;
global isWiFiPre;
isWiFiPre = 0;


 global isWiFiPF;
isWiFiPF = 0;


%output global config here
  outputFolderName
  configFile = strcat(outputFolderName, '/', 'globalConfig.txt')
  gConfig = fopen(configFile, 'w');
  fprintf(gConfig, "isBF : %d\n", isBF);
  fprintf(gConfig, "isRunRandom : %d\n", isRunRandom);
  fprintf(gConfig, "isWiFiPre : %d\n", isWiFiPre);
  fprintf(gConfig, "isNotConvex : %d\n", isNotConvex);
  %fprintf(gConfig, "isBaseWifiPre : %d\n", isBaseWiFiPre);
  fclose(gConfig);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %output the paM here to a filer
  paFile = strcat(outputFolderName, '/', 'paConfig.txt');
  paConfig = fopen(paFile, 'w');
  lenPaM = length(paM);
  kk = keys(paM);
  %paVs = values(paM);

  for i = 1:length(paM)
    pa = paM(kk{i});
    fprintf(paConfig, '%s : %d, %s\n', kk{i}, pa.toUseIndex, pa.values{pa.toUseIndex});
  end
  fclose(paConfig);
  %%%%%%%%%%%%%%%%%%%%%
  %init the N or M 

  if strcmp(testCaseName, "Number-of-UEs")
    pa = paM(kk{1})
    N = str2num(pa.values{pa.toUseIndex});

    inputFolderName = strcat(inputFolderName, '/', 'N=', pa.values{pa.toUseIndex})
  end
  if strcmp(testCaseName, "Number-of-APs")
    pa = paM(kk{1})
    M = str2num(pa.values{pa.toUseIndex});

    inputFolderName = strcat(inputFolderName, '/', 'M=', pa.values{pa.toUseIndex})
  end

  %also need to patch the inputFodler here

   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    initWThroughNew();

    %TODO can change Pw here
    initAPLocations(inputFolderName);
    %parfor i = 1:steps
    for i = 1:steps
        mainPrintAndCallerFixedMCSParam(paM, paraIndex, inputFolderName, outputFolderName, i);
    end

    fileID = fopen(strcat(outputFolderName, 'outputFolderName.txt'), 'w');
    fprintf(fileID, '%s', outputFolderName);
    fclose(fileID);

    fileID = fopen(strcat(outputFolderName, 'inputFolderName.txt'), 'w');
    fprintf(fileID, '%s', inputFolderName);
    fclose(fileID);
    % disp(outputFolderName);
    %bfString = sprintf("%d", isBF);
    bfString = int2str(isBF)
    system(['python', ' ', 'remote-back.py', ' ', outputFolderName], '-echo');
    %system(['python', ' ', 'drawTopoForApo.py', ' ', outputFolderName, ' ', bfString], '-echo')
    system(['python', ' ', 'drawTopoForApo.py', ' ', outputFolderName], '-echo')

    %we can pass in the testcaseName and praIndex here` for label if needed
    system(['Rscript', ' ', '--vanilla', ' ', 'concept-line-one-case.R', ' ', outputFolderName, ' ', bfString], '-echo')
    
    system(['Rscript', ' ', '--vanilla', ' ', 'flowLevelStat.R', ' ', outputFolderName,...
        ' ', int2str(N),...
        ' ', int2str(M),...
        ' ', int2str(steps),...
        ' ', int2str(isBF)...
        ], '-echo')

end
