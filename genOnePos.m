function newPos = genOnePos(genType)
    if strcmp(genType, 'normal')
        newPos =  generateRandomPlacementInCircleRandom(1, [0, 0], 1, 'uniform');
    else
        newPos = zeros(2, 1);
        nCluster = 3;
        nUE =1;
        globalP = 0;
        nSteps = 1;
        clusterPos = 2/3;
        rangeRadius = 0.25;
        angleIn = 45;
        baseClusterCenter = [0, 0];

        pos =  genCircClustersRandom(nCluster, nUE, globalP, nSteps, clusterPos, rangeRadius, angleIn, baseClusterCenter);
        newPos = pos(:, :, 1);
    end 
end