%all use this to calc rates now, both the func and utility part
function rateV = calcRawRates(x, MaxRate)
    %the x map passed in has already considered onOffMap
    global N;
    global M;
    global isWiFiPF;

    % N, M = dim(MaxRate) ??
    %global isThroughputScaling;

    rateV = zeros(N, M);
    nCon = sum(x, 1);
    %no need for this for loop, getRate can return matrxi
    for j = 1:M
        %1 means the first dimention here, vertical
        if nCon(1, j) == 0
            continue;
        end

        eMaxRate = MaxRate(:, j) .* x(:, j);

        if j == 1
            rateV(:, j) = eMaxRate ./ nCon(1, 1);
        else
            if ~isWiFiPF
                newSumR = calcSumRInverse(eMaxRate);
                rateV(:, j) = x(:, j) .* (1 / newSumR);
            else
                rateV(:, j) = eMaxRate ./ nCon(1, j);
            end

        end

    end

    %exit;
    %rateV = rateV / minRate * exp(1);
    %scale the rateV to use the right part of the log curve
    % if (isThroughputScaling)
    %    rateV = (rateV * N * 0.5);
    % end

end
