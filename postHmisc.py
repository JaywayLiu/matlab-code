import sys
import os
import glob
""" 
This script will be put at the root of the local mcsDataOutput
only works for one level
"""
#scriptLoc = "~/docs/matlab-code/"
scriptLoc = "~/mcsDataOutput/"
scriptName = "concept-line-one-case.R"
destRoot = "~/docs/dissert/mcsDataOutput/"
toTex = "toTex"
sName = sys.argv[1]
testCaseName = sys.argv[2]

os.chdir(sName +"/"+ testCaseName)

destRoot += sName +"/"+ testCaseName +"/"

for ii in glob.glob("./*"):
    if os.path.isdir(ii):
        os.chdir(ii)
        os.system("echo `pwd`")
        print("Rscript --vanilla " + scriptLoc + scriptName+ " `pwd` 0")
        os.system("Rscript --vanilla " + scriptLoc + scriptName+ " `pwd` 0")
        print("cp -ra figs "+ destRoot +ii +"/"+toTex)
        os.system("cp -ra figs "+ destRoot +ii +"/"+toTex)
        os.chdir("..")

os.chdir("../..")
