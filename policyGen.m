function xa = policyGen()
global M;
global N;
global disToBs;

%only wifi's distance, so -1
xa = zeros(N, M);
for i=1: N
    dis = zeros(1, M-1);
    for j=2:M
        dis(j-1) = disToBs{1, j}(i);
    end
    
    %index is the first occurence
    [minD, index] = min(dis);
    
    %connect to the cloest wifi
    if minD <125
        xa(i, index+1)  =1;
    else
        %connect to lte
        xa(i, 1) = 1;
    end
end