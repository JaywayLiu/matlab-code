function f = calcSumRInversePartial(rateArray, j)
%  ir = ones(size(rateArray))./rateArray;
global nonParInfo;
ir = 1 ./ rateArray;
%some  of them may be 0
zero_indices = isinf(ir);
ir(zero_indices) = 0;
  f = sum(ir);
  f= f+ nonParInfo{2}(1, j);
end