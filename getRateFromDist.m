function f= getRateFromDist(i, j)
global disToBs;
global wifiT;
global lteT;

 dist = disToBs{1, j}(i);
if j==1
    %f = 3.999521;
    f = interp1(lteT(:,1), lteT(:,4), dist);
else
    f = interp1(wifiT(:,2), wifiT(:,3), dist);
    
end
end