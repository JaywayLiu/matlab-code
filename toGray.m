%inputs: base, digits, value
% output: Gray
%Convert a value to a Gray code with the given base and digits.
% Iterating through a sequence of values would result in a sequence
% of Gray codes in which only one digit changes at a time.
function gray = toGray(base, digits, value)

	baseN = zeros(digits);	%  Stores the ordinary base-N number, one digit per entry
	
 
	%  Put the normal baseN number into the baseN array. For base 10, 109 
	%  would be stored as [9,0,1]
	for i = 1:digits 
		baseN(i) = mod(value, base);
		value    = value / base;
    end
 
	%  Convert the normal baseN number into the Gray code equivalent. Note that
	%  the loop starts at the most significant digit and goes down.
	shift = 0;
	for i = digits:-1:1 
        %i = i-1;
		%  The Gray digit gets shifted down by the sum of the higher
		%  digits.
		gray(i) = mod((baseN(i) + shift),  base);
		shift = shift + base - gray(i);	%  Subtract from base so shift is positive
    end
    
end
%  EXAMPLES
%  input: value = 1899, base = 10, digits = 4
%  output: baseN[] = [9,9,8,1], gray[] = [0,1,7,1]
%  input: value = 1900, base = 10, digits = 4
%  output: baseN[] = [0,0,9,1], gray[] = [0,1,8,1]