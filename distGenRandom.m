function  distGenRandom()
global N;
global M;
global disToBs;
global underwifiPer;
disToBs = cell(1, M);

wifiRange = 120/underwifiPer;
lteStart = 10000;


for j=1:M
    if j==1
        %disp(lteStart+wifiRange*1.414);
        disToBs{1, j} = randi([lteStart, round(lteStart+wifiRange*1.414)], 1, N);
    else
        disToBs{1, j} = randi([0, wifiRange], 1, N);
    end
end

end