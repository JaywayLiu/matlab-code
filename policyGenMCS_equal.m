function xa = policyGenMCS_equal(MaxRate)
global M;
global N;

%only wifi's distance, so -1
xa = zeros(N, M);
for i=1: N
    maxRateI = MaxRate(i, 1:M);
    [maxV, ~] = max(maxRateI);
    idx = find(maxRateI == maxV);
    lenR = length(idx);
    randIndex = randi(lenR, 1, 1);
%    randIndex = 1; 
    xa(i, idx(randIndex))  =1;
% if(i==3)
%  disp(maxRateI)
%  disp(maxV)
%  disp(idx)
%  disp(randIndex)
%  disp(idx(randIndex))
%     
%end   %xa(i, index)  =1;

end
