#include "mex.h"
#include "stdint.h"
#include "matrix.h"
#include "stdio.h"
#include "math.h"
#include "assert.h"

typedef mwSize intType;
typedef uint32_t outputType;

void copyArray(intType from[], intType to[], mwSize N)
{
  mwSize i;
  for (i=0; i<N; i++) {
    to[i] = from[i];
  }
}

double brute_force(mwSize ncol, mwSize nrow,  double *maxRate, outputType *x);

inline double getItemDouble(double* pointer, intType rowIndex, intType colIndex,  intType nrow)
{
    return (pointer[rowIndex + colIndex*nrow]);
}

inline intType* getItemInt(intType* pointer, intType rowIndex, intType colIndex,  intType nrow)
{
    return &(pointer[rowIndex + colIndex*nrow]);
}

inline intType getIndex(intType rowIndex, intType colIndex,  intType nrow)
{
    return rowIndex + colIndex * nrow;
}

//x should have been set to all 0 
void ToBooleanMatrix(mwSize* sol, outputType* x, mwSize nrow, mwSize ncol)
{
  intType i;
  for (i = 0; i< nrow; i++)
  {
     x[getIndex(i, sol[i], nrow)] = 1;
  }

}

void assertAllZero(outputType* output, intType ncol, intType nrow)
{
  intType i, j;
  for (i =0; i<ncol; i++)
  {
    for(j=0; j< nrow; j++)
    {
      assert(output[getIndex(j, i, nrow)] == 0);
    }
  }
}

void printCharArray(char* output,  intType nrow, intType ncol)
{
  intType i, j;
  for (i =0; i<nrow; i++)
  {
    for(j=0; j< ncol; j++)
    {
       printf("%d ", output[getIndex(i, j, nrow)]);
    }
	printf("\n");
  }
}

void printDoubleArray(double* output,  intType nrow, intType ncol)
{
  intType i, j;
  for (i =0; i<nrow; i++)
  {
    for(j=0; j< ncol; j++)
    {
       printf("%.3f ", output[getIndex(i, j, nrow)]);
    }
	printf("\n");
  }
}

void printIntArray(int* output,  intType nrow, intType ncol)
{
  intType i, j;
  for (i =0; i<nrow; i++)
  {
    for(j=0; j< ncol; j++)
    {
       printf("%d ", output[getIndex(i, j, nrow)]);
    }
	printf("\n");
  }
}
//#define maxRate(r, c) (maxRate[(r)*WIDTH + (c)])
/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{

//intType ncol = mxGetScalar(prhs[0]); 
//intType nrow = mxGetScalar(prhs[1]);
/* variable declarations here */
#if MX_HAS_INTERLEAVED_COMPLEX
double* maxRate1D = mxGetDoubles(prhs[0]);
#else
double* maxRate1D = mxGetPr(prhs[0]);
#endif

intType nrow = mxGetM(prhs[0]);
intType ncol = mxGetN(prhs[0]);

printf("ncol %d\n", ncol);
printf("nrow %d\n", nrow);

printDoubleArray(maxRate1D, nrow, ncol );
//can assert here
//assert(sizeM = nrow);
//assert(sizeN = ncol);


//maybe using 32 or 64bits will be faster here?
plhs[0] = mxCreateNumericMatrix( nrow, ncol, mxINT32_CLASS, mxREAL);
outputType* output = (outputType*) mxGetData(plhs[0]);

//printIntArray(output,  nrow, ncol);

double returnValue;
//assertAllZero(output, ncol, nrow);

//plhs[1] = mxCreateScalar();
//double* maxVout = mxGetDoubles(plhs[1]);
returnValue = brute_force(ncol, nrow, maxRate1D, output);

plhs[1] =mxCreateDoubleScalar(returnValue);
//brute_force(ncol, nrow, maxRate1D, output);
 
  /*  
    // Read in the data
for (int col=0; col < ncol; col++) {
    for (int row=0; row < nrow; row++) {
        outputMatrix[row + col*nrow] = outputBuff[col][row];
    }
}
*/
}
/* code here */
//maxRate is 2d array flatten
//x is the output
double brute_force(mwSize ncol, mwSize nrow, double *maxRate, outputType *x)
{
  mwSize nUE = nrow;
  mwSize nAP = ncol;

	bool isSkipHungry = true;

//solution 1
  mwSize maxN = pow(nAP, nUE);
printf("maxN = %d \n", maxN);
  mwSize j;
  mwSize baseN[nUE];
  mwSize sol[nUE];

 for(j =0; j< nUE; j++)
    sol[j] =0;

  double wifiSum[nAP];
  mwSize nWiFi[nAP];



  double maxV = -1000000;
  bool skipCurrentCase = false;

for(j = 0; j < maxN; j++) { 
    mwSize i;
    mwSize value = j;
    double sumV = 0;
 
  for (i =0; i<nAP; i++) {
    wifiSum[i] = 0;
  }

  for (i =0; i<nAP; i++) {
    nWiFi[i] =0;
} 	
   mwSize nlte = 0;
//	printf("j = %d\n", j);
  	for (i = 0; i < nUE; i++) {
      mwSize apIndex = value % nAP;
		baseN[i] = apIndex;
		value    = value / nAP;

    if (apIndex ==0)
    {
      ++nlte;
    }
    else
    {
      double maxRateOne =  maxRate[getIndex(i, apIndex, nrow)];
      if (maxRateOne >0) {
      wifiSum[apIndex] += 1 / maxRateOne ;
      nWiFi[apIndex]++;
	}
      else if (isSkipHungry) {
	skipCurrentCase = true;
	 continue;
       }

    }
   
}

if (skipCurrentCase)
{
  skipCurrentCase = false;
  continue;
}
//finish one case here
if (nlte>0) {
    for (i =0; i<nUE; i++)
{
   if (baseN[i] ==0)
    	sumV += log(maxRate[getIndex(i, 0, nrow)] / nlte);
}
}

    intType k;
    for (k =1; k<nAP; k++)
    {
     if (wifiSum[k] >0) {
      sumV += log(1/wifiSum[k]) * nWiFi[k];
	}
    }

    if (sumV > maxV)
    {
      maxV = sumV;
      copyArray(baseN, sol, nUE);
    }
//  printf("maxV = %f\n", maxV);
 

}
printf("sol= \n");
for (j =0; j<nUE; j++)
	printf("%d ", sol[j]);
printf("\n");



ToBooleanMatrix(sol, x, nrow, ncol);
/*
  double sum = 0;
  for (j=0; j<ncol; j++) {
  for (i=0; i<nrow; i++){
	printf("%d ", j);
//      printf("%.2f ", *(getItemDouble(maxRate, i, j, nrow)));
	sum += *(getItemDouble(maxRate, i, j, nrow));
  }
  printf("\n");
  }
  printf("%.3f", sum);
  */

  printf("maxV = %f\n", maxV);
  return maxV;
}


