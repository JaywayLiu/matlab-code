function initWThroughNew()
global wifiT;
global lteT;
global lteMCSTable;
global wifiMCSTable;
global lteSinrTable;
global wifiSinrTable;
wifiT = load('input_data/wifi.txt');
lteT = load('input_data/lte.txt');
lteMCSTable = lteT(:,3);
wifiMCSTable = wifiT(:,3);


lteSinrTable = lteT(:, 2);
wifiSinrTable = wifiT(:, 2);

%boost the sinr table as needed here
%wifiSinrTable = wifiSinrTable .* 1.1;
%lteSinrTable = lteSinrTable .* 1.1;


%[r, ~] = size(wifiT);
% kType = 'int32';
% vType = 'double';
% tMap = containers.Map('KeyType',kType,'ValueType',vType);

end
