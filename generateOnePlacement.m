function pos =  generateOnePlacement(origin, radius, N)

%rangeLimit = [400, 400];

rangeRadius  = radius;
nUE = N;
ori = origin; %x, y

    randAngle = rand(nUE, 1) * 2* pi;
    pos = zeros(nUE, 2);
    %polar coodination
    pos(:,1) = rangeRadius * rand(nUE, 1) .* cos(randAngle) + ori(1);
    pos(:,2) = rangeRadius * rand(nUE, 1) .* sin(randAngle) + ori(2);
    
for i = 1:nSteps
    radomDirection = rand(nUE, 2);
    pos = pos + radomDirection * ti;
    csvwrite(sprintf('%04d', i), pos);
end

end

