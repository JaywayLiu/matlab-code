#!/usr/bin/python3
"""
Should put this into the root folder of the dissert, above the mcsDataOutput
"""
import sys
import os

sName = sys.argv[1]
testCaseName = sys.argv[2]

user = "jianwel"
hostname = "access2.ces.clemson.edu"

if testCaseName == "all":
    print(f"rsync --relative {user}@{hostname}:mcsDataOutput/{sName}/*.tar.bz2 .")
    os.system(f"rsync --relative {user}@{hostname}:mcsDataOutput/{sName}/*.tar.bz2 .")
    os.chdir(f"mcsDataOutput/{sName}")
    os.system("tar -jxf *.tar.bz2")
    os.chdir("../..")
else:
    print(f"rsync --relative {user}@{hostname}:mcsDataOutput/{sName}/{testCaseName}.tar.bz2 .")
    
    os.system(f"rsync --relative {user}@{hostname}:mcsDataOutput/{sName}/{testCaseName}.tar.bz2 .")
    os.chdir(f"mcsDataOutput/{sName}")
    os.system(f"tar -jxf {testCaseName}.tar.bz2")
    os.chdir("../..")
#print(f"rsync --relative {user}@{hostname}:mcsDataOutput/{sName}/*.tar.bz2 {sName}/")
#os.system(f"rsync {user}@{hostname}:mcsDataoutput/{sName}/*.tar.bz2 .")