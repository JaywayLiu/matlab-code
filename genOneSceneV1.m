function genOneSceneV1()

    nSteps = 16384;
    nSteps = 1300;
    %nUE = 32;
    %nAP = 3;

    %nSteps  = 256;
    %nSteps  = 1024;
    %nUE = 64;
    %nUE = 64;
    nUE = 32;
    nUE = 64;
    %nUE = 8;
    %nUE = 6;
    nAP = 3;

        %globalP = 0;

    %nSteps  = 256;
    %nUE = 5;
    %nAP = 2;
    genType = "normal"
    genType = "cluster-circ"
    %genType = "cluster-tshape"

    global N;
    N = nUE;
    global M;
globalP =0;
    %wifiPos = 0.375;
    %wifiPos = 0.5;
    wifiPos = 2/3;

    M = nAP + 1;
    idString = sprintf('%d-%d-%d-%d', N, M, nSteps, round(wifiPos * 1000));
    datestring = datestr(datetime('now'), 'yyyymmddTHHMMSS');
    folderName = strcat('mcs_', idString, '_', datestring);
    if ~strcmp(genType, 'normal')
        folderName = strcat(folderName, '_', genType)
    end
    mkdir(char(folderName));
    addpath .
    cd(char(folderName));

    lteAPCenter = [0, 0];
    csvwrite('lte_centers.txt', lteAPCenter);

    %wifiRadius = 0.625;
    wifiRadius = 0.775;

    rangeLimit = 1;

    fileID = fopen('scene-config.txt', 'w');
    fprintf(fileID, '%d\n', nSteps);
    fprintf(fileID, '%d %d\n', nAP, nUE);
    fprintf(fileID, '%.4f\n', wifiRadius);
    fprintf(fileID, '%.4f\n', wifiPos);
    fprintf(fileID, '%.4f\n', rangeLimit);
    fprintf(fileID, '%.4f %.4f\n', lteAPCenter(1), lteAPCenter(2));
    fclose(fileID);

    %this is for calc of Wifi locations

    if strcmp(genType, 'normal')
        pos = generateRandomPlacementInCircleV1(nSteps, nUE, lteAPCenter, rangeLimit, 'uniform');
    elseif strcmp(genType, "cluster-circ")

        clusterPos = 2/3;
        rangeRadius = 0.25;
        angleIn = 45;

        fileID = fopen('circ-cluster.txt', 'w');

        fprintf(fileID, 'globalP: %.3f\n', globalP);
        fprintf(fileID, 'clusterPos: %.3f\n', clusterPos);
        fprintf(fileID, 'rangeRadius: %.3f\n', rangeRadius);
        fprintf(fileID, 'angleIn: %.3f\n', angleIn);
        fclose(fileID);

        %[pos, clusterCenters] = genCircClusters(3, nUE, globalP, nSteps, clusterPos, rangeRadius, angleIn, lteAPCenter);

        [pos, clusterCenters] = genCircClustersRandom(3, nUE, globalP, nSteps, clusterPos, rangeRadius, angleIn, lteAPCenter);
    elseif strcmp(genType, "cluster-tshape")

    else
        disp("type not suported, exit")
        exit
    end

    for i = 1:nSteps
        csvwrite(sprintf('mobility_%06d.txt', i), pos(:, :, i));
    end


    drawOneSpotNorm(wifiRadius, wifiPos, false, nAP, rangeLimit);

    cd('..');
end
