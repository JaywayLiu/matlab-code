function f= getRateFromRandomSNR(i,j)
%this global is generated in GetRandomSNRInitial and Update...
global NodeApSNR;
global Band;
f=Band*log2(1+NodeApSNR(i,j));
%disp(f);
%assert(~isnan(f) && ~isinf(f));
end
