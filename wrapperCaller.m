function wrapperCaller(testCaseName, mo_source, testIndex, toTexFolder)
    %will pass paM to the mainPrintCaller
    %testIndex = 2
    paM = loadParamsTestCases();
    testCasesMap = loadTestCases();
    global N;
    global M;
    global isOnOff;
    isOnOff = 0;


    %isPa = getCompType();
    %isPa = 2;
%[re, compName] = getenv('computername');
[re, compName] = system('hostname'); 
disp('compName=');
disp(compName);
%dataFolder = 'mcs_20181113T111754';
if isequal( compName , 'usignite')
    isPa = 0;
elseif isequal( compName(1:6), 'apollo')
	isPa = 2;
else
    isPa =1;
end



    disp('isPa=');
    disp(isPa);
%this is the one for the figures

%no / allowd in datafolder, need to concat later!!!
if isPa ==0
    outputBase = '/scratch2/jianwel/'
elseif isPa == 2
    outputBase = '/users/jianwel/'
else
    outputBase = '/home/jianwel/'
end

pwdString = pwd;
inputFolderName = strcat(pwdString,  '/' ,  mo_source);


%#############################################
    %toTest = 'WiFi_Coverage_Radius';

    paraList = testCasesMap(testCaseName);

    lenParas = length(paraList);

    sz = [];
    paraPath = '';

    if lenParas ==1 && length(paraList{1}) ~=0
        paraName = paraList{1};
        pa = paM(paraName)
        pa.toUseIndex = testIndex;

        paraPath = strcat(paraName,'=', pa.values{testIndex}, '/');
    
    elseif lenParas ==2
        for i = 1:lenParas
            sz(i) = length(paM(paraList{i}).values) 
        end

        [i1, i2] = ind2sub(sz, testIndex);

        pa1 = paM(paraList{1}); 
        pa2 = paM(paraList{2});


        
        pa1.toUseIndex = i1;
        pa2.toUseIndex = i2;

        paraPath = strcat(paraList{1}, '=', pa1.values{i1}, '/');
        secondPath = strcat(paraList{2}, '=', pa2.values{i2}, '/');
        paraPath = strcat(paraPath, secondPath);
        %may extend it to multiple i return values here
    end
%#########################################


    multiFolderTestCases = {'Number-of-UEs', 'Number-of-APs', 'cCircle-Pb',  'tCircle-Pb'};

    if ismember(testCaseName, multiFolderTestCases) 
        inputFolderName = strcat(inputFolderName, '/',  paraPath);
    end
disp(inputFolderName);

%splitCell = strsplit(mo_source, '/');

% if strcmp(testCaseName, "Number-of-UEs") ||  strcmp(testCaseName, "Number-of-APs" || strcmp(testCaseName, "cCircle-Pb"))
%     splitCell = strsplit(mo_source, '/');
%     sizes = sscanf(splitCell{2}, 'mcs_%d-%d-%d-%d_%dT%d');
% else
%     sizes = sscanf(mo_source, 'mcs_%d-%d-%d-%d_%dT%d');
% end

re = loadSceneConfig(inputFolderName);
N = re(3, 1);
M = re(2, 1) + 1
nSteps = re(1, 1);

% N = sizes(1);
% M = sizes(2);




    outputBase = strcat(outputBase, 'mcsDataOutput/', mo_source, '/');

    outputFolderName = strcat(outputBase, testCaseName, '/')

    if (length(paraPath) ~=0)
        outputFolderName = strcat(outputFolderName, paraPath)
    end

    singleRunTimer = tic
    singleSeqCallerParam(paM, testCaseName, testIndex, inputFolderName, outputFolderName, nSteps);

    timeFileS = sprintf("logs/time_%d.txt", testIndex);
    ftime = fopen(timeFileS, "w");
    fprintf(ftime, "%f\n", toc(singleRunTimer));
    fclose(ftime);

    if testIndex ==1
            system(['cat', ' ', strcat(outputFolderName, '*_wifiMcs.txt'), ' ', '>', ' ', strcat(outputFolderName, 'Mcs_wifi.txt')], '-echo')
            system(['cat', ' ', strcat(outputFolderName, '*_lteMcs.txt'), ' ', '>', ' ', strcat(outputFolderName, 'Mcs_lte.txt')], '-echo')
            system(['Rscript', ' ', '--vanilla', ' ', 'drawMcs_once.R', ' ', outputFolderName], '-echo')
    end

    toMoveList = {'figs', 'flowStat'}
    destFolder = strcat(outputFolderName, toTexFolder, '/')
    rmpath(destFolder)
    mkdir(destFolder)
    %exit
    for k = 1:length(toMoveList)
        toMoveList{k}
        system(['mv', ' ', strcat(outputFolderName, toMoveList{k}), ' ', destFolder], '-echo')
    end

end
