import os
import subprocess 
import shlex
dR =[0.0, 0.25, 0.5, 0.75, 1]
#dR =[1]
isOpt = [0, 1]
start = 5
nTask = len(dR) * len(isOpt)
end = start + nTask
runRange = range(start, end)

baseName = "apollo%02d"
#nSteps = 256 
nSteps = 16384
#compName = [baseName%(i) for i in runRange]
plist = []
k = start
for ii in isOpt:
    for jj in dR:
        compName = baseName%k
        k = k +1
        #command = """ssh -t %s 'cd ~/matlab-code && matlab -nojvm -nodisplay -nosplash -r "singleSeqCaller(%f, %d, %d)" | tee logs/seq-%d-%.3f-%d.log' """%(compName, jj, ii, nSteps, ii, jj, nSteps)
        command = """ssh -t %s 'cd ~/matlab-code && matlab -nojvm -nodisplay -nosplash -r "singleSeqCaller(%f, %d, %d)" 2>&1 > logs/seq-%d-%.3f-%d.log' """%(compName, jj, ii, nSteps, ii, jj, nSteps)
        args = shlex.split(command)
#        print(command)
        print(args)
        #os.system(command)
        p = subprocess.Popen(args)
        plist.append(p)
        
exit_codes = [t.wait() for t in plist]
print(exit_codes)



