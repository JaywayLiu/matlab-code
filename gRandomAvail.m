function xa = gRandomAvail()
global M;
global N;
%global lteMCSIndex;
%global wifiMCSIndex;
global MaxRate;
xa = zeros(N, M);

%only wifi distance, so -1
for i=1: N
    maxRateI = MaxRate(i, 1:M);
    %[maxV, ~] = max(maxRateI);
    idx = find(maxRateI > 0);
    %if LTE rate can goes to 0, this place will throw error
    randIndex = randi(length(idx), 1, 1);
    xa(i, idx(randIndex))  =1;
    %xa(i, index)  =2;

end

end
