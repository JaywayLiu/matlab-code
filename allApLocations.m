function allApLocations(folderName, topoConfig)

nUniform = topoConfig.nUniform;
nPPP = topoConfig.nPPP;

apFolderName = 'ap_locations';

fullPath = strcat('/', folderName, '/', apFolderName, '/');
for i = 1:nUniform
    createUniformAp(fullPath, i, topoConfig);
end

for i = nUniform + 1: nUniform + nPPP
    createPPPAp(fullPath, i, topoConfig);
end
end