 function nUEArray = simuPlacement(pVector, N)
%the length of pVector should be M
%assert(length(pVector) == M)
nUEArray = zeros(length(pVector), 1);
for i= 1:N
    a = rand();
    %use > here, so that if pVector(0) ==0, it won't give it any UE
    jj = find(pVector>a, 1, 'first');
    nUEArray(jj, 1) = nUEArray(jj, 1) +1;
end
%assert(sum(nUEArray) == N)
end
