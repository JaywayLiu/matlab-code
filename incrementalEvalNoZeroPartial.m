function [tempV, currentR] = incrementalEvalNoZeroPartial(currentV, currentR, maxRateTable, i, j)

    global isOpt;
    global deployRatio;
    %global phaseId;
    global nonParInfo;

    %need to make sure the input maxRateTable(i, j) >0
    %global onOffMap;
    %use of logical index is more efficient than find
    % ii = currentR(:,j) >0;
    bi = find(currentR(:, j) > 0);
    nUEOld = length(bi);
    nActiveUE = nUEOld + 1;

    nUEOldAdd = nUEOld;
    nActiveUEAdd = nActiveUE;

    if deployRatio < 1 && isOpt
        nUEOldAdd = nUEOld + nonParInfo{1}(1, j);
        nActiveUEAdd = nUEOldAdd + 1;
    end

    %newValue is always >0, this is sured by the caller
    newValue = maxRateTable(i, j);
    %otherDelta = 0;

    %either nUEOld >0 or nonPar >0 or both
    if (nUEOldAdd > 0)

        if (j == 1)
            newT = maxRateTable(i, j) / nActiveUEAdd;

            if nUEOld > 0
                newValues = maxRateTable(bi, j) / nActiveUEAdd;
                currentR(bi, j) = newValues;
                otherDelta = nUEOld * log(nUEOldAdd / nActiveUEAdd);
            else
                %only partial can enter this else
                otherDelta = 0;
            end

            %when old values are all 0s
        else

            if nUEOld > 0
                oldValue = currentR(bi(1), j);
                newInverse = 1 / oldValue + 1 / newValue;
                newT = 1 / newInverse;
                %newArray is row vector, return should be a scalar
                otherDelta =  nUEOld * log(newT / oldValue);
                currentR(bi, j) = newT;
            else
                %only partial can enter this else
                %if deployRatio < 1 && isOpt
                newInverse = 1 / newValue + nonParInfo{2}(1, j);
                newT = 1/newInverse;
                %end
                otherDelta = 0;
            end

  
            % else
            %     currentR(:, j) = 0;
            %     sumDelta = -nUEOld * log(oldValue);
            % end
        end

        currentR(i, j) = newT;
        tempV = currentV + otherDelta + log(newT);

    else
        %assert(nUEOldAdd==0)
        tempV = currentV + log(newValue);
        currentR(i, j) = newValue;
    end

end
