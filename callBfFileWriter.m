function callBfFileWriter()

global N;
global M;

N = 10;
M=2;

policy = zeros(1,N);


%simulate plus one operation with bits reversed
for k=1: M^N
    x = zeros(N, M);
    
    %translate the policy to x
    for i= 1:N
            x(i, policy(1, i) +1) =1;
    end
    
        if (k==0)
            dlmwrite('bfPolicy.csv',policy,'delimiter',',');
        else
            dlmwrite('bfPolicy.csv',policy,'delimiter',',','-append');
        end

    if (k==M^N)
        break;
    end

    %0 means lte  1 means wifi
    policy = addOneMore(policy, 1);
    %if(k==1) 
        disp(policy);
    %end
end

end


