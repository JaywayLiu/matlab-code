function generateRandomPlacement(nSteps, nUE, rangeLimit, alpha)

 pos = zeros(nUE, 2); 
 ori = rangeLimit /2;
 
 rangeRadius = alpha* min(rangeLimit(1), rangeLimit(2)); 
 for i = 1:nSteps
    randAngle = rand(nUE, 1) * 2* pi;
    randDist = rand(nUE, 1);
    %polar coodination
    pos(:,1) = rangeRadius * randDist  .* cos(randAngle) + ori(1);
    pos(:,2) = rangeRadius * randDist .* sin(randAngle) + ori(2);
    csvwrite(sprintf('mobility_%05d.txt', i), pos);
 end
end

