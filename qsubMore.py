import os

stepSize = 100
end = 1300

scriptName = "py_matlab_dis18b.pbs"

for i in range(0, 13):
    print("qsub -J %d-%d %s"%(1+i*stepSize, (i+1)*stepSize, scriptName))
    os.system("qsub -J %d-%d %s"%(1+i*stepSize, (i+1)*stepSize, scriptName))