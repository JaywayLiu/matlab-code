function mcsIndexFromDist()
global lteMCSIndex;
global wifiMCSIndex;

global lteSinrTable;
global wifiSinrTable;
global locationInfo;

global N;
global M;
global lteRadius;
%load the file
%N = 12;
%M =4;
isSetWiFiRadius = 0;

%these number should come from sinr reading from files
%lteRadius = 1;

fadingExp = 2.6;
epsilon = 0.001;
alphaLte= lteSinrTable(2) *((lteRadius+epsilon) ^ fadingExp);
%disp('alphaLte')

if isSetWiFiRadius
	wifiRadius = 0.625;
	alphaWifi = wifiSinrTable(2)* (wifiRadius ^ fadingExp);
else
	alphaWifi = alphaLte; 
	%wifiRadius = 0.774;
	wifiRadius = (alphaWifi / wifiSinrTable(2) )^ (1/fadingExp);
end

%alphaLte = 0.146 
%alphaWifi= 0.146 

%calc the sp/np
% global ueLocs;
% global wifiLoc; 
% global lteLoc;


lteMCSIndex = zeros(N, 1);
wifiMCSIndex = zeros(N, M-1);

sinrMa= zeros(N, M);
distMa = zeros(N, M); 

for i =1:N
%may use verctor to do this instead of for loop
% disp('i=')
% disp(i)
[sinr, dist] = calcSinrFromDist(locationInfo.ueLocs(:, i), locationInfo.lteLocs, alphaLte, fadingExp);
%disp(sinr)
sinrMa(i, 1) = sinr;
distMa(i, 1) = dist;
lteMCSIndex(i) = searchCloestLower(lteSinrTable, sinr);

for j=1:M-1
     %inputs are row vectors
     [sinr, dist] = calcSinrFromDist(locationInfo.ueLocs(:, i), locationInfo.wifiLocs(:, j), alphaWifi, fadingExp);
     sinrMa(i, j+1) = sinr;
     distMa(i, j+1) = dist;
        wifiMCSIndex(i, j) = searchCloestLower(wifiSinrTable, sinr);  
end
end

%disp(distMa);

%disp(sinrMa);


%disp(lteMCSIndex);
%disp(wifiMCSIndex);

end
