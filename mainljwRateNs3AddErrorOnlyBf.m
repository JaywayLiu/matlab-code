
function [ D, bfx, bfV, bfT,bfJ, xArrayR] =mainljwRateNs3AddErrorOnlyBf( errorRateInit, xArray, disToBsInit)
global N;
global M;
global disToBs;
global isBoth;
global underwifiPer;
global errorRate;

global isAddError;

isAddError = 1;


global isDist;

isDist = 1;

disToBs = disToBsInit;

errorRate = errorRateInit;

%rng(seed)


generateErrorDirection();
%N >M assert

%    r used in myfunInside, that's why we make it global
%global r
%r = rand(N, M);
%randM = r;




%distGenRandom();
initWThrough();

xArrayR = zeros(length(xArray), 3);
for i = 1:length(xArray)
    xArrayR(i, 1) = myfunInsideS(xArray{i});
    [t, j] = sumT(xArray{i});
    xArrayR(i, 2) = t;
     xArrayR(i, 3) = j;
end


[bfV, bfx] = bfMore();
[bfT, bfJ] = sumT(bfx);



D = disToBs;

end

