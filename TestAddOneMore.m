
function [minV, minx] = TestAddOneMore()
global N;
global M;

N = 2;
M = 3;
policy = zeros(1, N+2);
minV = 1e10000;
%simulate plus one operation with bits reversed
for k=1: M^N
    x = zeros(N, M);
    
    %translate the policy to x
    for i= 1:N
            x(i, policy(i) +1) =1;
    end
    minx = x;
    disp(policy);
    
%     v= myfunInsideS(x);
%     if v< minV
%         minV= v;
%         minx = x;
%     end
    %0 means lte  1 means wifi
    policy = addOneMore(policy, 1);
end
end


%function for M>1
function f = addOneMore(array, pos)
global M;
if array(pos) <M-1
    array(pos) = array(pos) + 1;
else
    array(pos) = 0;
    array = addOneMore(array, pos+1);
end
f = array;
end
