function  f =distGenFromFiles(folderName, topoIndex)
global N;
global M;
global disToBs;
%global underwifiPer;
%N = 50;
%M = 4;
disToBs = cell(1, M);

lteP = csvread(strcat(folderName , '/lte_ap.txt'))
wifiP = csvread(strcat(folderName , '/wifi_centers.txt'))
mnP = csvread(strcat(folderName , sprintf('/mobility_%05d.txt', topoIndex)));


for j=1:M
    if j==1
        %disp(lteStart+wifiRange*1.414);
        for i=1:N
           %disToBs{1, j}(i) = norm(mnP(i), lteP)
           disToBs{1, j}(i) =  lteP;
        end
    else
        for i=1:N
            disToBs{1, j}(i) = norm(mnP(i) - wifiP(j-1));
        end
    end
end

 f= disToBs;
end
