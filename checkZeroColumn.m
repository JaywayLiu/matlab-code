function f=checkZeroColumn(x, name, timeStamp)
	[~, M] = size(x);
	i = 1;
	fileName = strcat(name, '_zero_connection_cases.log');
	ff = fopen(fileName, 'a+');
	for j = 1:M
		if (~any(x(:, j)))
			fprintf(ff, '%d %d\n', timeStamp, j);
		end
	end
	fclose(ff); 	
end
