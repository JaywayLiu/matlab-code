import csv
import pandas as pd
import os, sys

import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)



class TopoDrawer:


    def loadLteAPInfo(self):
        with open(self.inputFolder + "/lte_centers.txt") as f:
            l = list(csv.reader(f, delimiter=","))
            logger.debug(l)
            self.lte_centers = [float(i) for i in l[0]]



    def loadWifiAPInfo(self):
        with open(self.inputFolder + "/wifi_centers.txt") as f:
            l = list(csv.reader(f, delimiter=","))


            self.wifi_centers = [[float(l[i][j]) for j in range(len(l[i]))]
                                 for i in range(len(l))]

    def loadOtherInfo(self):
        with open(self.inputFolder + "/scene-config.txt") as f:
            self.nRuns = int(f.readline())
            self.nWiFiAPs, self.nUE = [int(x) for x in f.readline().split()]
            self.wifiRadius = float(f.readline())
            self.wifiPos = float(f.readline())
            self.lteRadius = float(f.readline())

            logger.debug(f"nRuns = {self.nRuns}")
            logger.debug(f"lteRadius = {self.lteRadius}")

    def drawAll(self):
        for ii in self.algoList:
            for jj in self.metricList:
                self.drawOneMetric(ii+"."+jj)




    def drawOneMetric(self, algoString):

        #ax.axis("equal")  # make aspect ratio square

        gg_json = self.findMaxMin(algoString)

        self.drawList("maxList", gg_json["maxList"], algoString)
        self.drawList("minList", gg_json["minList"], algoString)

        #plt.show()


    def drawOne(self, listString, index, ki, algoString, fig, ax):
        m_string = self.inputFolder + "/mobility_{0:05}.txt".format(index)
        data = pd.read_csv(m_string, header=None)
        dim = data.shape

        for i in range(0, dim[1]):
            ax.add_artist(plt.Circle(data.iloc[:, i].tolist(), 0.02, color='k'))



        fig.savefig(self.outputFolder +listString+'/{}_{:03}_{:06}.{'
                                                  '}'.format(algoString,
                                                             ki, index,
                                                             self.IMAGE_TYPE))

        if (self.isPng):
            pngFolder=self.outputFolder +listString+"/png"

            if not os.path.exists(pngFolder):
                os.makedirs(pngFolder)
            fig.savefig(pngFolder+'/{}_{:03}_{:06}.{'
                                                      '}'.format(algoString,
                                                                 ki, index,
                                                                 "png"))
        plt.close(fig)


    def drawAps(self,  ax):
        lteCircle = plt.Circle(self.lte_centers, self.lteRadius, color='k',
                               fill=False, linestyle="dashed")
        ax.add_artist(lteCircle)
        ax.plot(self.lte_centers[0], self.lte_centers[1], 'bv', markersize=8)

        for pos in self.wifi_centers:
            ax.add_artist(plt.Circle(pos, self.wifiRadius, color='k',
                               fill=False, linestyle="dotted"))
            ax.plot(pos[0], pos[1], 'g^')

    def drawList(self, listString, list, algoString):
        listFolder= self.outputFolder+listString


        if not os.path.exists(listFolder):
            os.makedirs(listFolder)

        for ki, index in enumerate(list):
            fig, ax = plt.subplots()
            ax.set_aspect(1.0)  # make aspect ratio square
            ax.axis([-1, 1, -1, 1])
            self.drawAps(ax)
            self.drawOne(listString, index, ki, algoString,  fig, ax)


    def findMaxMin(self, algoName):
        data = pd.read_csv(self.outputFolder + "/all.txt", delimiter=' ' )
        #print(data)
        sorted_data = data.sort_values(by=[algoName], ascending=False)
        #print(sorted_data)
        topKFrame = sorted_data.head(self.K)
        print(topKFrame)
        topK = list(topKFrame.index.values)
        print(topKFrame.index.values)
        #print(list(topKFrame.index.values))
        tailKFrame = sorted_data.tail(self.K)
        tailK = list(tailKFrame.index.values)
        print(tailK)
        return {"maxList": topK, "minList": tailK}


    def __init__(self, outputFolder, algoList, metricList, K=1,
                                                            inputFolder=None):

        self.outputFolder = outputFolder
        self.algoList = algoList
        self.metricList = metricList

        if not inputFolder:
            with open(self.outputFolder + "/inputFolderName.txt") as f:
                self.inputFolder = f.read().strip() + "/"
                logger.info("read input name: {intputFolder}")
        else:
            self.inputFolder = inputFolder

        self.loadLteAPInfo()
        self.loadWifiAPInfo()
        self.loadOtherInfo()

        print(self.lte_centers)
        print(self.wifi_centers)

        self.K = K
        self.IMAGE_TYPE = "eps"
        self.isPng = True


if __name__ == "__main__":
    outputFolder = sys.argv[1]
#    outputFolder = "/home/jianwel/mcsDataOutput/dist-equal-alpha-5-3-256-667" \
#                   "-bf/opt=0/1.0000/"

    algoList = ["gg", "atom", "pe", "policy", "bf"]
    metricList = ["v", "t"]

    topo_drawer = TopoDrawer(outputFolder, algoList, metricList, K=5)
    topo_drawer.drawAll()



