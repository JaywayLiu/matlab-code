function initOnOffMap(dataFolder)
  global onOffMap;
  onOffMap = csvread(strcat(dataFolder, '/activeMatrix.csv'));
end