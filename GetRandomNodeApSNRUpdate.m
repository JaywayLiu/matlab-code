function []=GetRandomNodeApSNRUpdate()
global cof;
global NodeApSNR;
global N;
global M;

for i=1:N
    for j=1:M
        J=rand;
        if J<0.5
            snr=(1-cof)*NodeApSNR(i,j);
            NodeApSNR(i,j)=snr;     
        else 
            snr=(1+cof)*NodeApSNR(i,j);
            NodeApSNR(i,j)=snr;
        end
    end
 end
end